	.text
	.file	"llvm-link"
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_annealer_thread.cpp
	.type	_GLOBAL__sub_I_annealer_thread.cpp,@function
_GLOBAL__sub_I_annealer_thread.cpp:     # @_GLOBAL__sub_I_annealer_thread.cpp
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rax
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end0:
	.size	_GLOBAL__sub_I_annealer_thread.cpp, .Lfunc_end0-_GLOBAL__sub_I_annealer_thread.cpp
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function _ZN15annealer_thread3RunEv
.LCPI1_0:
	.quad	4609434218613702656     # double 1.5
.LCPI1_2:
	.quad	0                       # double 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_1:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	_ZN15annealer_thread3RunEv
	.p2align	4, 0x90
	.type	_ZN15annealer_thread3RunEv,@function
_ZN15annealer_thread3RunEv:             # @_ZN15annealer_thread3RunEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	16(%rdi), %ebx
	movl	$_ZN3Rng9seed_lockE, %edi
	callq	pthread_mutex_lock
	movl	$5008, %edi             # imm = 0x1390
	callq	_Znwm
	movq	%rax, %rcx
	movl	_ZN3Rng4seedE(%rip), %edx
	leal	1(%rdx), %esi
	movl	%esi, _ZN3Rng4seedE(%rip)
	movq	%rdx, (%rax)
	movq	%rdx, %rsi
	shrq	$30, %rsi
	xorl	%edx, %esi
	imull	$1812433253, %esi, %esi # imm = 0x6C078965
	addl	$1, %esi
	movq	%rsi, 8(%rax)
	movl	$3, %edx
	.p2align	4, 0x90
.LBB1_1:                                # %for.body.for.body_crit_edge.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdi
	shrq	$30, %rdi
	xorq	%rsi, %rdi
	imulq	$1812433253, %rdi, %rsi # imm = 0x6C078965
	addq	%rdx, %rsi
	addq	$-1, %rsi
	movl	%esi, %edi
	movq	%rdi, -8(%rcx,%rdx,8)
	shrq	$30, %rdi
	xorl	%esi, %edi
	imull	$1812433253, %edi, %esi # imm = 0x6C078965
	addl	%edx, %esi
	movq	%rsi, (%rcx,%rdx,8)
	addq	$2, %rdx
	cmpq	$625, %rdx              # imm = 0x271
	jne	.LBB1_1
# %bb.2:                                # %_ZN6MTRand10initializeEm.exit.i.i
	cvtsi2sd	%ebx, %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movq	(%rcx), %rbx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_3:                                # %for.body.i4.i.i
                                        # =>This Inner Loop Header: Depth=1
	andl	$-2147483648, %ebx      # imm = 0x80000000
	movq	8(%rcx,%rsi), %rbp
	movl	%ebp, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	orq	%rbx, %rdx
	movl	%ebp, %edi
	andl	$1, %edi
	negl	%edi
	andl	$-1727483681, %edi      # imm = 0x9908B0DF
	xorq	3176(%rcx,%rsi), %rdi
	shrq	%rdx
	xorq	%rdx, %rdi
	movq	%rdi, (%rcx,%rsi)
	cmpl	$1808, %esi             # imm = 0x710
	je	.LBB1_4
# %bb.44:                               # %for.body.i4.i.i.1
                                        #   in Loop: Header=BB1_3 Depth=1
	andl	$-2147483648, %ebp      # imm = 0x80000000
	movq	16(%rcx,%rsi), %rbx
	movl	%ebx, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	orq	%rbp, %rdx
	movl	%ebx, %edi
	andl	$1, %edi
	negl	%edi
	andl	$-1727483681, %edi      # imm = 0x9908B0DF
	xorq	3184(%rcx,%rsi), %rdi
	shrq	%rdx
	xorq	%rdx, %rdi
	movq	%rdi, 8(%rcx,%rsi)
	addq	$16, %rsi
	jmp	.LBB1_3
.LBB1_4:                                # %for.cond4.preheader.i.i.i
	addq	%rsi, %rax
	movl	$3, %edi
	.p2align	4, 0x90
.LBB1_5:                                # %for.body7.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rsi
                                        # kill: def $ebp killed $ebp killed $rbp def $rbp
	andl	$-2147483648, %ebp      # imm = 0x80000000
	movq	-8(%rax,%rdi,8), %rbx
	movl	%ebx, %edi
	andl	$2147483646, %edi       # imm = 0x7FFFFFFE
	orq	%rbp, %rdi
	movq	(%rax,%rsi,8), %rbp
	movl	%ebx, %edx
	andl	$1, %edx
	negl	%edx
	andl	$-1727483681, %edx      # imm = 0x9908B0DF
	xorq	-1832(%rax,%rsi,8), %rdx
	shrq	%rdi
	xorq	%rdi, %rdx
	movq	%rdx, -16(%rax,%rsi,8)
	andl	$-2147483648, %ebx      # imm = 0x80000000
	movl	%ebp, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	orq	%rbx, %rdx
	shrq	%rdx
	movl	%ebp, %edi
	andl	$1, %edi
	negl	%edi
	andl	$-1727483681, %edi      # imm = 0x9908B0DF
	xorq	-1824(%rax,%rsi,8), %rdi
	xorq	%rdx, %rdi
	movq	%rdi, -8(%rax,%rsi,8)
	leaq	2(%rsi), %rdi
	cmpl	$399, %edi              # imm = 0x18F
	jne	.LBB1_5
# %bb.6:                                # %_ZN3RngC2Ev.exit
	andl	$-2147483648, %ebp      # imm = 0x80000000
	movq	(%rcx), %rdx
	movl	%edx, %edi
	andl	$2147483646, %edi       # imm = 0x7FFFFFFE
	orq	%rbp, %rdi
	shrq	%rdi
	andl	$1, %edx
	negl	%edx
	andl	$-1727483681, %edx      # imm = 0x9908B0DF
	xorq	-1816(%rax,%rsi,8), %rdx
	xorq	%rdi, %rdx
	movq	%rdx, (%rax,%rsi,8)
	movl	$624, 5000(%rcx)        # imm = 0x270
	movq	%rcx, 4992(%rcx)
	movq	%rcx, 16(%rsp)
	movl	$_ZN3Rng9seed_lockE, %edi
	callq	pthread_mutex_unlock
	movq	(%r13), %rdi
.Ltmp0:
	leaq	80(%rsp), %rsi
	leaq	16(%rsp), %rcx
	movq	$-1, %rdx
	callq	_ZN7netlist18get_random_elementEPllP3Rng
.Ltmp1:
# %bb.7:                                # %invoke.cont
	movq	(%r13), %rdi
.Ltmp3:
	leaq	40(%rsp), %rsi
	leaq	16(%rsp), %rcx
	movq	$-1, %rdx
	callq	_ZN7netlist18get_random_elementEPllP3Rng
.Ltmp4:
# %bb.8:                                # %while.cond.preheader
	movq	%rax, %rbx
	leaq	24(%r13), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$-1, 8(%rsp)            # 4-byte Folded Spill
	xorl	%ebp, %ebp
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_9 Depth=1
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
.LBB1_19:                               # %for.cond.cleanup
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	36(%rsp), %ebp          # 4-byte Reload
	addl	$1, %ebp
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	pthread_barrier_wait
.LBB1_9:                                # %while.cond
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_22 Depth 2
                                        #       Child Loop BB1_24 Depth 3
                                        #       Child Loop BB1_26 Depth 3
	movl	20(%r13), %eax
	cmpl	$-1, %eax
	je	.LBB1_10
# %bb.16:                               # %_ZN15annealer_thread10keep_goingEiii.exit
                                        #   in Loop: Header=BB1_9 Depth=1
	cmpl	%ebp, %eax
	jg	.LBB1_17
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_10:                               # %if.then.i
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	cmpl	8(%rsp), %eax           # 4-byte Folded Reload
	jle	.LBB1_12
# %bb.11:                               # %if.then.i
                                        #   in Loop: Header=BB1_9 Depth=1
	cmpb	$0, 8(%r13)
	je	.LBB1_12
.LBB1_17:                               # %while.body
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	.LCPI1_0(%rip), %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	cmpl	$0, 12(%r13)
	jle	.LBB1_18
# %bb.21:                               # %for.body.preheader
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, 8(%rsp)             # 4-byte Folded Spill
	xorl	%r12d, %r12d
	jmp	.LBB1_22
	.p2align	4, 0x90
.LBB1_37:                               # %if.then23
                                        #   in Loop: Header=BB1_22 Depth=2
	movq	(%r13), %rdi
.Ltmp18:
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	_ZN7netlist14swap_locationsEP12netlist_elemS1_
.Ltmp19:
# %bb.38:                               #   in Loop: Header=BB1_22 Depth=2
	addl	$1, 12(%rsp)            # 4-byte Folded Spill
.LBB1_39:                               # %if.end31
                                        #   in Loop: Header=BB1_22 Depth=2
	addl	$1, %r12d
	cmpl	12(%r13), %r12d
	jge	.LBB1_19
.LBB1_22:                               # %for.body
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_24 Depth 3
                                        #       Child Loop BB1_26 Depth 3
	movq	%rbx, %rbp
	movq	40(%rsp), %rdx
	movq	%rdx, 80(%rsp)
	movq	(%r13), %rdi
.Ltmp6:
	leaq	40(%rsp), %rsi
	leaq	16(%rsp), %rcx
	callq	_ZN7netlist18get_random_elementEPllP3Rng
.Ltmp7:
# %bb.23:                               # %invoke.cont11
                                        #   in Loop: Header=BB1_22 Depth=2
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB1_24:                               # %do.body.i.i
                                        #   Parent Loop BB1_9 Depth=1
                                        #     Parent Loop BB1_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	#APP
	lock
	cmpxchgq	%rax, 80(%rbp)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB1_24
# %bb.25:                               # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit.i
                                        #   in Loop: Header=BB1_22 Depth=2
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB1_26:                               # %do.body.i15.i
                                        #   Parent Loop BB1_9 Depth=1
                                        #     Parent Loop BB1_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	#APP
	lock
	cmpxchgq	%rax, 80(%rbx)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB1_26
# %bb.27:                               # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit16.i
                                        #   in Loop: Header=BB1_22 Depth=2
	movq	%rax, %r15
.Ltmp9:
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	_ZN12netlist_elem9swap_costEP10location_tS1_
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
.Ltmp10:
# %bb.28:                               # %call4.i.noexc
                                        #   in Loop: Header=BB1_22 Depth=2
.Ltmp11:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	_ZN12netlist_elem9swap_costEP10location_tS1_
.Ltmp12:
# %bb.29:                               # %invoke.cont14
                                        #   in Loop: Header=BB1_22 Depth=2
	movapd	48(%rsp), %xmm1         # 16-byte Reload
	addsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	movapd	%xmm1, 48(%rsp)         # 16-byte Spill
	ucomisd	%xmm1, %xmm0
	ja	.LBB1_37
# %bb.30:                               # %if.else.i82
                                        #   in Loop: Header=BB1_22 Depth=2
.Ltmp14:
	leaq	16(%rsp), %rdi
	callq	_ZN3Rng5drandEv
.Ltmp15:
# %bb.31:                               # %call.i80.noexc
                                        #   in Loop: Header=BB1_22 Depth=2
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	movapd	48(%rsp), %xmm0         # 16-byte Reload
	xorpd	.LCPI1_1(%rip), %xmm0
	divsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	callq	exp
	ucomisd	72(%rsp), %xmm0         # 8-byte Folded Reload
	jbe	.LBB1_39
# %bb.32:                               # %if.then
                                        #   in Loop: Header=BB1_22 Depth=2
	movq	(%r13), %rdi
.Ltmp16:
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	_ZN7netlist14swap_locationsEP12netlist_elemS1_
.Ltmp17:
# %bb.33:                               #   in Loop: Header=BB1_22 Depth=2
	addl	$1, 8(%rsp)             # 4-byte Folded Spill
	jmp	.LBB1_39
.LBB1_12:                               # %_ZN15annealer_thread10keep_goingEiii.exit.thread91
	movb	$0, 8(%r13)
.LBB1_13:                               # %while.end
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_15
# %bb.14:                               # %delete.notnull.i
	callq	_ZdlPv
.LBB1_15:                               # %_ZN3RngD2Ev.exit
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.LBB1_20:                               # %lpad3
	.cfi_def_cfa_offset 144
.Ltmp5:
	jmp	.LBB1_41
.LBB1_40:                               # %lpad
.Ltmp2:
	jmp	.LBB1_41
.LBB1_34:                               # %lpad10
.Ltmp8:
	jmp	.LBB1_41
.LBB1_36:                               # %lpad16
.Ltmp20:
	jmp	.LBB1_41
.LBB1_35:                               # %lpad13
.Ltmp13:
.LBB1_41:                               # %ehcleanup38
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_43
# %bb.42:                               # %delete.notnull.i86
	callq	_ZdlPv
.LBB1_43:                               # %_ZN3RngD2Ev.exit87
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN15annealer_thread3RunEv, .Lfunc_end1-_ZN15annealer_thread3RunEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	255                     # @TType Encoding = omit
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end0-.Lcst_begin0
.Lcst_begin0:
	.uleb128 .Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.uleb128 .Ltmp0-.Lfunc_begin0   #   Call between .Lfunc_begin0 and .Ltmp0
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp0-.Lfunc_begin0   # >> Call Site 2 <<
	.uleb128 .Ltmp1-.Ltmp0          #   Call between .Ltmp0 and .Ltmp1
	.uleb128 .Ltmp2-.Lfunc_begin0   #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp3-.Lfunc_begin0   # >> Call Site 3 <<
	.uleb128 .Ltmp4-.Ltmp3          #   Call between .Ltmp3 and .Ltmp4
	.uleb128 .Ltmp5-.Lfunc_begin0   #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp18-.Lfunc_begin0  # >> Call Site 4 <<
	.uleb128 .Ltmp19-.Ltmp18        #   Call between .Ltmp18 and .Ltmp19
	.uleb128 .Ltmp20-.Lfunc_begin0  #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp6-.Lfunc_begin0   # >> Call Site 5 <<
	.uleb128 .Ltmp7-.Ltmp6          #   Call between .Ltmp6 and .Ltmp7
	.uleb128 .Ltmp8-.Lfunc_begin0   #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp9-.Lfunc_begin0   # >> Call Site 6 <<
	.uleb128 .Ltmp12-.Ltmp9         #   Call between .Ltmp9 and .Ltmp12
	.uleb128 .Ltmp13-.Lfunc_begin0  #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp14-.Lfunc_begin0  # >> Call Site 7 <<
	.uleb128 .Ltmp17-.Ltmp14        #   Call between .Ltmp14 and .Ltmp17
	.uleb128 .Ltmp20-.Lfunc_begin0  #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp17-.Lfunc_begin0  # >> Call Site 8 <<
	.uleb128 .Lfunc_end1-.Ltmp17    #   Call between .Ltmp17 and .Lfunc_end1
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end0:
	.p2align	2
                                        # -- End function
	.text
	.globl	_ZN15annealer_thread10keep_goingEiii # -- Begin function _ZN15annealer_thread10keep_goingEiii
	.p2align	4, 0x90
	.type	_ZN15annealer_thread10keep_goingEiii,@function
_ZN15annealer_thread10keep_goingEiii:   # @_ZN15annealer_thread10keep_goingEiii
	.cfi_startproc
# %bb.0:                                # %entry
	movl	20(%rdi), %eax
	cmpl	$-1, %eax
	je	.LBB2_1
# %bb.4:                                # %if.else
	cmpl	%esi, %eax
	setg	%al
                                        # kill: def $al killed $al killed $eax
	retq
.LBB2_1:                                # %if.then
	cmpl	%ecx, %edx
	jle	.LBB2_3
# %bb.2:                                # %if.then
	movb	$1, %al
	cmpb	$0, 8(%rdi)
	je	.LBB2_3
# %bb.5:                                # %if.end9
                                        # kill: def $al killed $al killed $eax
	retq
.LBB2_3:                                # %if.then4
	movb	$0, 8(%rdi)
	xorl	%eax, %eax
                                        # kill: def $al killed $al killed $eax
	retq
.Lfunc_end2:
	.size	_ZN15annealer_thread10keep_goingEiii, .Lfunc_end2-_ZN15annealer_thread10keep_goingEiii
	.cfi_endproc
                                        # -- End function
	.globl	_ZN15annealer_thread28calculate_delta_routing_costEP12netlist_elemS1_ # -- Begin function _ZN15annealer_thread28calculate_delta_routing_costEP12netlist_elemS1_
	.p2align	4, 0x90
	.type	_ZN15annealer_thread28calculate_delta_routing_costEP12netlist_elemS1_,@function
_ZN15annealer_thread28calculate_delta_routing_costEP12netlist_elemS1_: # @_ZN15annealer_thread28calculate_delta_routing_costEP12netlist_elemS1_
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
	.cfi_def_cfa_offset 48
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB3_1:                                # %do.body.i
                                        # =>This Inner Loop Header: Depth=1
	#APP
	lock
	cmpxchgq	%rax, 80(%rdi)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB3_1
# %bb.2:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB3_3:                                # %do.body.i15
                                        # =>This Inner Loop Header: Depth=1
	#APP
	lock
	cmpxchgq	%rax, 80(%r14)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB3_3
# %bb.4:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit16
	movq	%rax, %rbx
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	_ZN12netlist_elem9swap_costEP10location_tS1_
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	_ZN12netlist_elem9swap_costEP10location_tS1_
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	addq	$16, %rsp
	.cfi_def_cfa_offset 32
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end3:
	.size	_ZN15annealer_thread28calculate_delta_routing_costEP12netlist_elemS1_, .Lfunc_end3-_ZN15annealer_thread28calculate_delta_routing_costEP12netlist_elemS1_
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4               # -- Begin function _ZN15annealer_thread11accept_moveEddP3Rng
.LCPI4_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	_ZN15annealer_thread11accept_moveEddP3Rng
	.p2align	4, 0x90
	.type	_ZN15annealer_thread11accept_moveEddP3Rng,@function
_ZN15annealer_thread11accept_moveEddP3Rng: # @_ZN15annealer_thread11accept_moveEddP3Rng
	.cfi_startproc
# %bb.0:                                # %entry
	subq	$40, %rsp
	.cfi_def_cfa_offset 48
	xorl	%eax, %eax
	xorps	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	ja	.LBB4_2
# %bb.1:                                # %if.else
	movq	%rsi, %rdi
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	callq	_ZN3Rng5drandEv
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	xorpd	.LCPI4_0(%rip), %xmm0
	divsd	(%rsp), %xmm0           # 8-byte Folded Reload
	callq	exp
	xorl	%ecx, %ecx
	ucomisd	8(%rsp), %xmm0          # 8-byte Folded Reload
	seta	%cl
	movl	$2, %eax
	subl	%ecx, %eax
.LBB4_2:                                # %return
	addq	$40, %rsp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end4:
	.size	_ZN15annealer_thread11accept_moveEddP3Rng, .Lfunc_end4-_ZN15annealer_thread11accept_moveEddP3Rng
	.cfi_endproc
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_main.cpp
	.type	_GLOBAL__sub_I_main.cpp,@function
_GLOBAL__sub_I_main.cpp:                # @_GLOBAL__sub_I_main.cpp
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rax
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit.2, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit.2, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end5:
	.size	_GLOBAL__sub_I_main.cpp, .Lfunc_end5-_GLOBAL__sub_I_main.cpp
	.cfi_endproc
                                        # -- End function
	.text
	.globl	main                    # -- Begin function main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
	.cfi_def_cfa_offset 272
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r13d
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$22, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB6_94
# %bb.1:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit340
	cmpb	$0, 56(%rbx)
	je	.LBB6_3
# %bb.2:                                # %if.then.i260
	movb	67(%rbx), %al
	jmp	.LBB6_4
.LBB6_3:                                # %if.end.i264
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.LBB6_4:                                # %_ZNKSt5ctypeIcE5widenEc.exit266
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movl	$3, %edi
	callq	srandom
	leal	-5(%r13), %eax
	cmpl	$2, %eax
	jae	.LBB6_95
# %bb.5:                                # %if.end
	movq	8(%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r15
	movl	$_ZSt4cout, %edi
	movl	$.L.str.3, %esi
	movl	$13, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movl	%r15d, %esi
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB6_94
# %bb.6:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit344
	cmpb	$0, 56(%rbp)
	je	.LBB6_8
# %bb.7:                                # %if.then.i272
	movb	67(%rbp), %al
	jmp	.LBB6_9
.LBB6_8:                                # %if.end.i276
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.LBB6_9:                                # %_ZNKSt5ctypeIcE5widenEc.exit278
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	16(%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	$_ZSt4cout, %edi
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%eax, %esi
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movl	$.L.str.4, %esi
	movl	$27, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB6_94
# %bb.10:                               # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit348
	cmpb	$0, 56(%rbp)
	je	.LBB6_12
# %bb.11:                               # %if.then.i284
	movb	67(%rbp), %al
	jmp	.LBB6_13
.LBB6_12:                               # %if.end.i288
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.LBB6_13:                               # %_ZNKSt5ctypeIcE5widenEc.exit290
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	24(%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
	movl	$_ZSt4cout, %edi
	movl	$.L.str.5, %esi
	movl	$19, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$_ZSt4cout, %edi
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movl	%ebx, %esi
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB6_94
# %bb.14:                               # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit352
	cmpb	$0, 56(%rbp)
	je	.LBB6_16
# %bb.15:                               # %if.then.i296
	movb	67(%rbp), %al
	jmp	.LBB6_17
.LBB6_16:                               # %if.end.i300
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.LBB6_17:                               # %_ZNKSt5ctypeIcE5widenEc.exit302
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	movq	32(%r14), %rbx
	leaq	16(%rsp), %r12
	movq	%r12, (%rsp)
	testq	%rbx, %rbx
	je	.LBB6_24
# %bb.18:                               # %cond.end.thread.i
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rax, 104(%rsp)
	movq	%r12, %rax
	cmpq	$15, %rbp
	jbe	.LBB6_21
# %bb.19:                               # %if.then4.i.i.i.i
.Ltmp21:
	movq	%rsp, %rdi
	leaq	104(%rsp), %rsi
	xorl	%edx, %edx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
.Ltmp22:
# %bb.20:                               # %call5.i.i.i9.i.noexc
	movq	%rax, (%rsp)
	movq	104(%rsp), %rcx
	movq	%rcx, 16(%rsp)
.LBB6_21:                               # %if.end6.i.i.i.i
	testq	%rbp, %rbp
	je	.LBB6_27
# %bb.22:                               # %if.end6.i.i.i.i
	cmpq	$1, %rbp
	jne	.LBB6_26
# %bb.23:                               # %if.then.i.i.i.i.i.i
	movb	(%rbx), %cl
	movb	%cl, (%rax)
	jmp	.LBB6_27
.LBB6_26:                               # %if.end.i.i.i.i.i.i.i
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	callq	memcpy
.LBB6_27:                               # %invoke.cont
	movq	104(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	(%rsp), %rcx
	movb	$0, (%rcx,%rax)
.Ltmp23:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.6, %esi
	movl	$18, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp24:
# %bb.28:                               # %invoke.cont25
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdx
.Ltmp25:
	movl	$_ZSt4cout, %edi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp26:
# %bb.29:                               # %invoke.cont27
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB6_30
# %bb.32:                               # %call.i304.noexc
	cmpb	$0, 56(%rbp)
	je	.LBB6_34
# %bb.33:                               # %if.then.i311
	movb	67(%rbp), %al
	jmp	.LBB6_36
.LBB6_34:                               # %if.end.i315
.Ltmp27:
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp28:
# %bb.35:                               # %.noexc317
	movq	(%rbp), %rax
.Ltmp29:
	movq	%rbp, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.Ltmp30:
.LBB6_36:                               # %call.i172.noexc
.Ltmp31:
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZNSo3putEc
.Ltmp32:
# %bb.37:                               # %call1.i173.noexc
.Ltmp33:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp34:
# %bb.38:                               # %invoke.cont29
	movl	$-1, %r12d
	cmpl	$6, %r13d
	jne	.LBB6_50
# %bb.39:                               # %if.then32
	movq	40(%r14), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r12
.Ltmp35:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.7, %esi
	movl	$29, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp36:
# %bb.40:                               # %invoke.cont36
.Ltmp37:
	movl	$_ZSt4cout, %edi
	movl	%r12d, %esi
	callq	_ZNSolsEi
.Ltmp38:
# %bb.41:                               # %invoke.cont38
	movq	%rax, %rbp
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%rbp,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB6_42
# %bb.44:                               # %call.i202.noexc
	cmpb	$0, 56(%rbx)
	je	.LBB6_46
# %bb.45:                               # %if.then.i
	movb	67(%rbx), %al
	jmp	.LBB6_48
.LBB6_46:                               # %if.end.i
.Ltmp39:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp40:
# %bb.47:                               # %.noexc208
	movq	(%rbx), %rax
.Ltmp41:
	movq	%rbx, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.Ltmp42:
.LBB6_48:                               # %call.i192.noexc
.Ltmp43:
	movsbl	%al, %esi
	movq	%rbp, %rdi
	callq	_ZNSo3putEc
.Ltmp44:
# %bb.49:                               # %call1.i193.noexc
.Ltmp45:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp46:
.LBB6_50:                               # %if.end42
.Ltmp47:
	leaq	104(%rsp), %rbx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	_ZN7netlistC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Ltmp48:
# %bb.51:                               # %invoke.cont44
	movq	%rbx, 48(%rsp)
	movb	$1, 56(%rsp)
	movq	40(%rsp), %rax          # 8-byte Reload
                                        # kill: def $eax killed $eax killed $rax
	cltd
	idivl	%r15d
	movl	%eax, 60(%rsp)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, 64(%rsp)
	movl	%r12d, 68(%rsp)
	leaq	72(%rsp), %r14
	movq	%r14, %rdi
	xorl	%esi, %esi
	movl	%r15d, %edx
	callq	pthread_barrier_init
	movslq	%r15d, %rax
	shrq	$60, %rax
	jne	.LBB6_52
# %bb.57:                               # %_ZNSt6vectorImSaImEE17_S_check_init_lenEmRKS0_.exit.i
	movq	%r15, %rbp
	shlq	$32, %rbp
	je	.LBB6_58
# %bb.59:                               # %_ZNSt16allocator_traitsISaImEE8allocateERS0_m.exit.i.i.i.i
	sarq	$29, %rbp
.Ltmp50:
	movq	%rbp, %rdi
	callq	_Znwm
.Ltmp51:
# %bb.60:                               # %for.body.i.i.preheader.i.i.i.i.i
	movq	%rax, %r12
	movq	%rax, %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	testl	%r15d, %r15d
	jg	.LBB6_62
	jmp	.LBB6_68
.LBB6_58:
	xorl	%r12d, %r12d
	testl	%r15d, %r15d
	jle	.LBB6_68
.LBB6_62:                               # %for.body.preheader
	movl	%r15d, %ebp
	leaq	48(%rsp), %r13
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB6_63:                               # %for.body
                                        # =>This Inner Loop Header: Depth=1
	movl	$_Z8entry_ptPv, %edx
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%r13, %rcx
	callq	pthread_create
	addq	$8, %rbx
	addq	$-1, %rbp
	jne	.LBB6_63
# %bb.64:                               # %for.cond55.preheader
	testl	%r15d, %r15d
	jle	.LBB6_68
# %bb.65:                               # %for.body58.preheader
	movl	%r15d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_66:                               # %for.body58
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
.Ltmp52:
	xorl	%esi, %esi
	callq	pthread_join
.Ltmp53:
# %bb.67:                               # %for.inc64
                                        #   in Loop: Header=BB6_66 Depth=1
	addq	$1, %rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_66
.LBB6_68:                               # %for.cond.cleanup57
.Ltmp55:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.8, %esi
	movl	$18, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp56:
# %bb.69:                               # %invoke.cont68
.Ltmp57:
	leaq	104(%rsp), %rdi
	callq	_ZN7netlist18total_routing_costEv
.Ltmp58:
# %bb.70:                               # %invoke.cont70
.Ltmp59:
	movl	$_ZSt4cout, %edi
	callq	_ZNSo9_M_insertIdEERSoT_
.Ltmp60:
# %bb.71:                               # %invoke.cont72
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r15,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB6_72
# %bb.77:                               # %call.i321.noexc
	cmpb	$0, 56(%rbp)
	je	.LBB6_79
# %bb.78:                               # %if.then.i328
	movb	67(%rbp), %al
	jmp	.LBB6_81
.LBB6_79:                               # %if.end.i332
.Ltmp61:
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp62:
# %bb.80:                               # %.noexc334
	movq	(%rbp), %rax
.Ltmp63:
	movq	%rbp, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.Ltmp64:
.LBB6_81:                               # %call.i233.noexc
.Ltmp65:
	movsbl	%al, %esi
	movq	%r15, %rdi
	callq	_ZNSo3putEc
.Ltmp66:
# %bb.82:                               # %call1.i234.noexc
.Ltmp67:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp68:
# %bb.83:                               # %invoke.cont74
	testq	%r12, %r12
	je	.LBB6_85
# %bb.84:                               # %if.then.i.i.i
	movq	%r12, %rdi
	callq	_ZdlPv
.LBB6_85:                               # %_ZNSt6vectorImSaImEED2Ev.exit
	movq	%r14, %rdi
	callq	pthread_barrier_destroy
	leaq	104(%rsp), %rdi
	callq	_ZN7netlistD2Ev
	movq	(%rsp), %rdi
	leaq	16(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB6_87
# %bb.86:                               # %if.then.i.i243
	callq	_ZdlPv
.LBB6_87:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	xorl	%eax, %eax
	addq	$216, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.LBB6_94:                               # %if.then.i338
	.cfi_def_cfa_offset 272
	callq	_ZSt16__throw_bad_castv
.LBB6_95:                               # %if.then
	movl	$_ZSt4cout, %edi
	movl	$.L.str.1, %esi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	(%r14), %rsi
	movq	%rax, %rdi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$.L.str.2, %esi
	movq	%rax, %rdi
	callq	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movq	%rax, %rdi
	callq	_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	movl	$1, %edi
	callq	exit
.LBB6_24:                               # %if.then.i.i.i.i
.Ltmp81:
	movl	$.L.str.9, %edi
	callq	_ZSt19__throw_logic_errorPKc
.Ltmp82:
# %bb.25:                               # %.noexc
.LBB6_30:                               # %if.then.i354
.Ltmp78:
	callq	_ZSt16__throw_bad_castv
.Ltmp79:
# %bb.31:                               # %.noexc356
.LBB6_52:                               # %if.then.i.i
.Ltmp72:
	movl	$.L.str.10, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp73:
# %bb.56:                               # %.noexc210
.LBB6_72:                               # %if.then.i359
.Ltmp69:
	callq	_ZSt16__throw_bad_castv
.Ltmp70:
# %bb.76:                               # %.noexc361
.LBB6_42:                               # %if.then.i214
.Ltmp75:
	callq	_ZSt16__throw_bad_castv
.Ltmp76:
# %bb.43:                               # %.noexc216
.LBB6_53:                               # %lpad
.Ltmp83:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.LBB6_74:                               # %lpad48
.Ltmp74:
	movq	%rax, %rbp
	jmp	.LBB6_90
.LBB6_73:                               # %lpad43
.Ltmp49:
	movq	%rax, %rbp
	jmp	.LBB6_91
.LBB6_55:                               # %lpad35
.Ltmp77:
	movq	%rax, %rbp
	jmp	.LBB6_91
.LBB6_54:                               # %lpad24
.Ltmp80:
	movq	%rax, %rbp
	jmp	.LBB6_91
.LBB6_88:                               # %ehcleanup
.Ltmp71:
	movq	%rax, %rbp
	testq	%r12, %r12
	jne	.LBB6_89
	jmp	.LBB6_90
.LBB6_75:                               # %ehcleanup.thread
.Ltmp54:
	movq	%rax, %rbp
.LBB6_89:                               # %if.then.i.i.i246
	movq	%r12, %rdi
	callq	_ZdlPv
.LBB6_90:                               # %ehcleanup77
	movq	%r14, %rdi
	callq	pthread_barrier_destroy
	leaq	104(%rsp), %rdi
	callq	_ZN7netlistD2Ev
.LBB6_91:                               # %ehcleanup83
	movq	(%rsp), %rdi
	leaq	16(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB6_93
# %bb.92:                               # %if.then.i.i253
	callq	_ZdlPv
.LBB6_93:                               # %ehcleanup84
	movq	%rbp, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	255                     # @TType Encoding = omit
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end1-.Lcst_begin1
.Lcst_begin1:
	.uleb128 .Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.uleb128 .Ltmp21-.Lfunc_begin1  #   Call between .Lfunc_begin1 and .Ltmp21
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp21-.Lfunc_begin1  # >> Call Site 2 <<
	.uleb128 .Ltmp22-.Ltmp21        #   Call between .Ltmp21 and .Ltmp22
	.uleb128 .Ltmp83-.Lfunc_begin1  #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp22-.Lfunc_begin1  # >> Call Site 3 <<
	.uleb128 .Ltmp23-.Ltmp22        #   Call between .Ltmp22 and .Ltmp23
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp23-.Lfunc_begin1  # >> Call Site 4 <<
	.uleb128 .Ltmp34-.Ltmp23        #   Call between .Ltmp23 and .Ltmp34
	.uleb128 .Ltmp80-.Lfunc_begin1  #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp35-.Lfunc_begin1  # >> Call Site 5 <<
	.uleb128 .Ltmp46-.Ltmp35        #   Call between .Ltmp35 and .Ltmp46
	.uleb128 .Ltmp77-.Lfunc_begin1  #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp47-.Lfunc_begin1  # >> Call Site 6 <<
	.uleb128 .Ltmp48-.Ltmp47        #   Call between .Ltmp47 and .Ltmp48
	.uleb128 .Ltmp49-.Lfunc_begin1  #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp50-.Lfunc_begin1  # >> Call Site 7 <<
	.uleb128 .Ltmp51-.Ltmp50        #   Call between .Ltmp50 and .Ltmp51
	.uleb128 .Ltmp74-.Lfunc_begin1  #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp51-.Lfunc_begin1  # >> Call Site 8 <<
	.uleb128 .Ltmp52-.Ltmp51        #   Call between .Ltmp51 and .Ltmp52
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp52-.Lfunc_begin1  # >> Call Site 9 <<
	.uleb128 .Ltmp53-.Ltmp52        #   Call between .Ltmp52 and .Ltmp53
	.uleb128 .Ltmp54-.Lfunc_begin1  #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp55-.Lfunc_begin1  # >> Call Site 10 <<
	.uleb128 .Ltmp68-.Ltmp55        #   Call between .Ltmp55 and .Ltmp68
	.uleb128 .Ltmp71-.Lfunc_begin1  #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp68-.Lfunc_begin1  # >> Call Site 11 <<
	.uleb128 .Ltmp81-.Ltmp68        #   Call between .Ltmp68 and .Ltmp81
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp81-.Lfunc_begin1  # >> Call Site 12 <<
	.uleb128 .Ltmp82-.Ltmp81        #   Call between .Ltmp81 and .Ltmp82
	.uleb128 .Ltmp83-.Lfunc_begin1  #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp78-.Lfunc_begin1  # >> Call Site 13 <<
	.uleb128 .Ltmp79-.Ltmp78        #   Call between .Ltmp78 and .Ltmp79
	.uleb128 .Ltmp80-.Lfunc_begin1  #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp72-.Lfunc_begin1  # >> Call Site 14 <<
	.uleb128 .Ltmp73-.Ltmp72        #   Call between .Ltmp72 and .Ltmp73
	.uleb128 .Ltmp74-.Lfunc_begin1  #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp69-.Lfunc_begin1  # >> Call Site 15 <<
	.uleb128 .Ltmp70-.Ltmp69        #   Call between .Ltmp69 and .Ltmp70
	.uleb128 .Ltmp71-.Lfunc_begin1  #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp75-.Lfunc_begin1  # >> Call Site 16 <<
	.uleb128 .Ltmp76-.Ltmp75        #   Call between .Ltmp75 and .Ltmp76
	.uleb128 .Ltmp77-.Lfunc_begin1  #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp76-.Lfunc_begin1  # >> Call Site 17 <<
	.uleb128 .Lfunc_end6-.Ltmp76    #   Call between .Ltmp76 and .Lfunc_end6
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end1:
	.p2align	2
                                        # -- End function
	.text
	.globl	_Z8entry_ptPv           # -- Begin function _Z8entry_ptPv
	.p2align	4, 0x90
	.type	_Z8entry_ptPv,@function
_Z8entry_ptPv:                          # @_Z8entry_ptPv
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rax
	.cfi_def_cfa_offset 16
	callq	_ZN15annealer_thread3RunEv
.Lfunc_end7:
	.size	_Z8entry_ptPv, .Lfunc_end7-_Z8entry_ptPv
	.cfi_endproc
                                        # -- End function
	.section	.text._ZN7netlistD2Ev,"axG",@progbits,_ZN7netlistD2Ev,comdat
	.weak	_ZN7netlistD2Ev         # -- Begin function _ZN7netlistD2Ev
	.p2align	4, 0x90
	.type	_ZN7netlistD2Ev,@function
_ZN7netlistD2Ev:                        # @_ZN7netlistD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	addq	$64, %rdi
	movq	80(%r14), %rsi
.Ltmp84:
	callq	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
.Ltmp85:
# %bb.1:                                # %_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEED2Ev.exit
	movq	40(%r14), %rbx
	movq	48(%r14), %r15
	cmpq	%r15, %rbx
	jne	.LBB8_2
# %bb.6:                                # %invoke.cont.i
	testq	%rbx, %rbx
	je	.LBB8_8
.LBB8_7:                                # %if.then.i.i.i
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB8_8:                                # %_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EED2Ev.exit
	movq	16(%r14), %rbx
	movq	24(%r14), %r15
	cmpq	%r15, %rbx
	jne	.LBB8_9
# %bb.17:                               # %invoke.cont.i13
	testq	%rbx, %rbx
	je	.LBB8_18
.LBB8_20:                               # %if.then.i.i.i14
	movq	%rbx, %rdi
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	jmp	_ZdlPv                  # TAILCALL
	.p2align	4, 0x90
.LBB8_4:                                # %_ZSt8_DestroyISt6vectorI10location_tSaIS1_EEEvPT_.exit.i.i.i.i
                                        #   in Loop: Header=BB8_2 Depth=1
	.cfi_def_cfa_offset 32
	addq	$24, %rbx
	cmpq	%rbx, %r15
	je	.LBB8_5
.LBB8_2:                                # %for.body.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_4
# %bb.3:                                # %if.then.i.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB8_2 Depth=1
	callq	_ZdlPv
	jmp	.LBB8_4
	.p2align	4, 0x90
.LBB8_15:                               # %_ZSt8_DestroyI12netlist_elemEvPT_.exit.i.i.i.i
                                        #   in Loop: Header=BB8_9 Depth=1
	addq	$88, %rbx
	cmpq	%rbx, %r15
	je	.LBB8_16
.LBB8_9:                                # %for.body.i.i.i.i6
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_11
# %bb.10:                               # %if.then.i.i.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB8_9 Depth=1
	callq	_ZdlPv
.LBB8_11:                               # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit.i.i.i.i.i.i
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_13
# %bb.12:                               # %if.then.i.i.i4.i.i.i.i.i.i
                                        #   in Loop: Header=BB8_9 Depth=1
	callq	_ZdlPv
.LBB8_13:                               # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit5.i.i.i.i.i.i
                                        #   in Loop: Header=BB8_9 Depth=1
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rdi, %rax
	je	.LBB8_15
# %bb.14:                               # %if.then.i.i.i.i.i.i.i.i7
                                        #   in Loop: Header=BB8_9 Depth=1
	callq	_ZdlPv
	jmp	.LBB8_15
.LBB8_5:                                # %invoke.cont.loopexit.i
	movq	40(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_7
	jmp	.LBB8_8
.LBB8_16:                               # %invoke.cont.loopexit.i11
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_20
.LBB8_18:                               # %_ZNSt6vectorI12netlist_elemSaIS0_EED2Ev.exit
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.LBB8_19:                               # %lpad.i.i
	.cfi_def_cfa_offset 32
.Ltmp86:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN7netlistD2Ev, .Lfunc_end8-_ZN7netlistD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase0-.Lttbaseref0
.Lttbaseref0:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end2-.Lcst_begin2
.Lcst_begin2:
	.uleb128 .Ltmp84-.Lfunc_begin2  # >> Call Site 1 <<
	.uleb128 .Ltmp85-.Ltmp84        #   Call between .Ltmp84 and .Ltmp85
	.uleb128 .Ltmp86-.Lfunc_begin2  #     jumps to .Ltmp86
	.byte	1                       #   On action: 1
.Lcst_end2:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase0:
	.p2align	2
                                        # -- End function
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,comdat
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E # -- Begin function _ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E,@function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E: # @_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	testq	%rsi, %rsi
	je	.LBB9_5
# %bb.1:                                # %while.body.preheader
	movq	%rsi, %rbx
	movq	%rdi, %r14
	jmp	.LBB9_2
	.p2align	4, 0x90
.LBB9_4:                                # %_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE12_M_drop_nodeEPSt13_Rb_tree_nodeISA_E.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r15, %rbx
	testq	%r15, %r15
	je	.LBB9_5
.LBB9_2:                                # %while.body
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	movq	16(%rbx), %r15
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rax
	cmpq	%rax, %rdi
	je	.LBB9_4
# %bb.3:                                # %if.then.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB9_2 Depth=1
	callq	_ZdlPv
	jmp	.LBB9_4
.LBB9_5:                                # %while.end
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end9:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E, .Lfunc_end9-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
	.cfi_endproc
                                        # -- End function
	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate  # -- Begin function __clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# %bb.0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end10:
	.size	__clang_call_terminate, .Lfunc_end10-__clang_call_terminate
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_netlist.cpp
	.type	_GLOBAL__sub_I_netlist.cpp,@function
_GLOBAL__sub_I_netlist.cpp:             # @_GLOBAL__sub_I_netlist.cpp
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rax
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit.4, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit.4, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end11:
	.size	_GLOBAL__sub_I_netlist.cpp, .Lfunc_end11-_GLOBAL__sub_I_netlist.cpp
	.cfi_endproc
                                        # -- End function
	.text
	.globl	_ZN7netlist7releaseEP12netlist_elem # -- Begin function _ZN7netlist7releaseEP12netlist_elem
	.p2align	4, 0x90
	.type	_ZN7netlist7releaseEP12netlist_elem,@function
_ZN7netlist7releaseEP12netlist_elem:    # @_ZN7netlist7releaseEP12netlist_elem
	.cfi_startproc
# %bb.0:                                # %entry
	retq
.Lfunc_end12:
	.size	_ZN7netlist7releaseEP12netlist_elem, .Lfunc_end12-_ZN7netlist7releaseEP12netlist_elem
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function _ZN7netlist18total_routing_costEv
.LCPI13_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	_ZN7netlist18total_routing_costEv
	.p2align	4, 0x90
	.type	_ZN7netlist18total_routing_costEv,@function
_ZN7netlist18total_routing_costEv:      # @_ZN7netlist18total_routing_costEv
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%r14
	.cfi_def_cfa_offset 16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	pushq	%rax
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -24
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	88(%rdi), %rbx
	addq	$72, %r14
	xorpd	%xmm0, %xmm0
	cmpq	%r14, %rbx
	je	.LBB13_6
# %bb.1:                                # %for.body.preheader
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB13_2:                               # %for.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_3 Depth 2
	movq	64(%rbx), %rdi
	.p2align	4, 0x90
.LBB13_3:                               # %do.body.i
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, 80(%rdi)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB13_3
# %bb.4:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	(%rax), %rsi
	callq	_ZN12netlist_elem22routing_cost_given_locE10location_t
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movq	%rbx, %rdi
	callq	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base
	movq	%rax, %rbx
	cmpq	%r14, %rax
	jne	.LBB13_2
# %bb.5:                                # %for.cond.cleanup.loopexit
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI13_0(%rip), %xmm0
.LBB13_6:                               # %for.cond.cleanup
	addq	$8, %rsp
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end13:
	.size	_ZN7netlist18total_routing_costEv, .Lfunc_end13-_ZN7netlist18total_routing_costEv
	.cfi_endproc
                                        # -- End function
	.globl	_ZN7netlist7shuffleEP3Rng # -- Begin function _ZN7netlist7shuffleEP3Rng
	.p2align	4, 0x90
	.type	_ZN7netlist7shuffleEP3Rng,@function
_ZN7netlist7shuffleEP3Rng:              # @_ZN7netlist7shuffleEP3Rng
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movl	4(%rdi), %eax
	imull	8(%rdi), %eax
	imull	$1000, %eax, %eax       # imm = 0x3E8
	testl	%eax, %eax
	je	.LBB14_11
# %bb.1:                                # %for.body.lr.ph
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB14_2:                               # %for.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_3 Depth 2
                                        #     Child Loop BB14_5 Depth 2
                                        #     Child Loop BB14_8 Depth 2
	movl	12(%r15), %esi
	movq	%r14, %rdi
	callq	_ZN3Rng4randEi
	movq	%rax, %rbx
	movq	16(%r15), %rbp
	imulq	$88, %rax, %r12
	leaq	(%r12,%rbp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB14_3:                               # %while.body.i
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	12(%r15), %esi
	movq	%r14, %rdi
	callq	_ZN3Rng4randEi
	cmpq	%rbx, %rax
	je	.LBB14_3
# %bb.4:                                # %while.cond.while.end_crit_edge.i
                                        #   in Loop: Header=BB14_2 Depth=1
	imulq	$88, %rax, %rdx
	addq	16(%r15), %rdx
	movq	(%rsp), %r8             # 8-byte Reload
	leaq	(%r12,%rbp), %rsi
	addq	$80, %rsi
	leaq	80(%rdx), %rbx
	cmpq	%rdx, %r8
	movq	%rbx, %rdi
	cmovbq	%rsi, %rdi
	movq	_ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip), %rbp
	.p2align	4, 0x90
.LBB14_5:                               # %do.body.i.i.i.i.i
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, (%rdi)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB14_5
# %bb.6:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit.i.i.i.i
                                        #   in Loop: Header=BB14_5 Depth=2
	movq	%rax, %rcx
	#APP
	lock
	cmpxchgq	%rbp, (%rdi)
	sete	%al
	movzbq	%al, %rax
.Ltmp87:	# atomic_cmpset_long
	#NO_APP
	testl	%eax, %eax
	je	.LBB14_5
# %bb.7:                                # %_ZN7threads9AtomicPtrI10location_tE8CheckoutEv.exit.i.i
                                        #   in Loop: Header=BB14_2 Depth=1
	cmpq	%rdx, %r8
	cmovbq	%rbx, %rsi
	.p2align	4, 0x90
.LBB14_8:                               # %do.body.i.i10.i.i
                                        #   Parent Loop BB14_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, (%rsi)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB14_8
# %bb.9:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit.i.i.i
                                        #   in Loop: Header=BB14_8 Depth=2
	movq	%rax, %rdx
	#APP
	lock
	cmpxchgq	%rcx, (%rsi)
	sete	%al
	movzbq	%al, %rax
.Ltmp88:	# atomic_cmpset_long
	#NO_APP
	testl	%eax, %eax
	je	.LBB14_8
# %bb.10:                               # %_ZN7netlist14swap_locationsEP12netlist_elemS1_.exit
                                        #   in Loop: Header=BB14_2 Depth=1
	movq	%rdx, (%rdi)
	addl	$1, %r13d
	movl	4(%r15), %eax
	imull	8(%r15), %eax
	imull	$1000, %eax, %eax       # imm = 0x3E8
	cmpl	%eax, %r13d
	jb	.LBB14_2
.LBB14_11:                              # %for.cond.cleanup
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end14:
	.size	_ZN7netlist7shuffleEP3Rng, .Lfunc_end14-_ZN7netlist7shuffleEP3Rng
	.cfi_endproc
                                        # -- End function
	.globl	_ZN7netlist15get_random_pairEPP12netlist_elemS2_P3Rng # -- Begin function _ZN7netlist15get_random_pairEPP12netlist_elemS2_P3Rng
	.p2align	4, 0x90
	.type	_ZN7netlist15get_random_pairEPP12netlist_elemS2_P3Rng,@function
_ZN7netlist15get_random_pairEPP12netlist_elemS2_P3Rng: # @_ZN7netlist15get_random_pairEPP12netlist_elemS2_P3Rng
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	12(%rdi), %esi
	movq	%rcx, %rdi
	callq	_ZN3Rng4randEi
	movq	%rax, %rbx
	imulq	$88, %rax, %rbp
	addq	16(%r13), %rbp
	.p2align	4, 0x90
.LBB15_1:                               # %while.body
                                        # =>This Inner Loop Header: Depth=1
	movl	12(%r13), %esi
	movq	%r12, %rdi
	callq	_ZN3Rng4randEi
	cmpq	%rbx, %rax
	je	.LBB15_1
# %bb.2:                                # %while.cond.while.end_crit_edge
	imulq	$88, %rax, %rax
	addq	16(%r13), %rax
	movq	%rbp, (%r15)
	movq	%rax, (%r14)
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end15:
	.size	_ZN7netlist15get_random_pairEPP12netlist_elemS2_P3Rng, .Lfunc_end15-_ZN7netlist15get_random_pairEPP12netlist_elemS2_P3Rng
	.cfi_endproc
                                        # -- End function
	.globl	_ZN7netlist14swap_locationsEP12netlist_elemS1_ # -- Begin function _ZN7netlist14swap_locationsEP12netlist_elemS1_
	.p2align	4, 0x90
	.type	_ZN7netlist14swap_locationsEP12netlist_elemS1_,@function
_ZN7netlist14swap_locationsEP12netlist_elemS1_: # @_ZN7netlist14swap_locationsEP12netlist_elemS1_
	.cfi_startproc
# %bb.0:                                # %entry
	leaq	80(%rsi), %r8
	leaq	80(%rdx), %r9
	cmpq	%rdx, %rsi
	movq	%r9, %rdi
	cmovbq	%r8, %rdi
	movq	_ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip), %r10
	.p2align	4, 0x90
.LBB16_1:                               # %do.body.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	#APP
	lock
	cmpxchgq	%rax, (%rdi)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB16_1
# %bb.2:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit.i.i.i
                                        #   in Loop: Header=BB16_1 Depth=1
	movq	%rax, %rcx
	#APP
	lock
	cmpxchgq	%r10, (%rdi)
	sete	%al
	movzbq	%al, %rax
.Ltmp89:	# atomic_cmpset_long
	#NO_APP
	testl	%eax, %eax
	je	.LBB16_1
# %bb.3:                                # %_ZN7threads9AtomicPtrI10location_tE8CheckoutEv.exit.i
	cmpq	%rdx, %rsi
	cmovbq	%r9, %r8
	.p2align	4, 0x90
.LBB16_4:                               # %do.body.i.i10.i
                                        # =>This Inner Loop Header: Depth=1
	#APP
	lock
	cmpxchgq	%rax, (%r8)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB16_4
# %bb.5:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit.i.i
                                        #   in Loop: Header=BB16_4 Depth=1
	movq	%rax, %rdx
	#APP
	lock
	cmpxchgq	%rcx, (%r8)
	sete	%al
	movzbq	%al, %rax
.Ltmp90:	# atomic_cmpset_long
	#NO_APP
	testl	%eax, %eax
	je	.LBB16_4
# %bb.6:                                # %_ZN7threads9AtomicPtrI10location_tE4SwapERS2_.exit
	movq	%rdx, (%rdi)
	retq
.Lfunc_end16:
	.size	_ZN7netlist14swap_locationsEP12netlist_elemS1_, .Lfunc_end16-_ZN7netlist14swap_locationsEP12netlist_elemS1_
	.cfi_endproc
                                        # -- End function
	.globl	_ZN7netlist18get_random_elementEPllP3Rng # -- Begin function _ZN7netlist18get_random_elementEPllP3Rng
	.p2align	4, 0x90
	.type	_ZN7netlist18get_random_elementEPllP3Rng,@function
_ZN7netlist18get_random_elementEPllP3Rng: # @_ZN7netlist18get_random_elementEPllP3Rng
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r12
	.cfi_def_cfa_offset 32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	pushq	%rax
	.cfi_def_cfa_offset 48
	.cfi_offset %rbx, -40
	.cfi_offset %r12, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	12(%rdi), %esi
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB17_2:                               # %while.body
                                        # =>This Inner Loop Header: Depth=1
	callq	_ZN3Rng4randEi
	cmpq	%rbx, %rax
	jne	.LBB17_3
# %bb.1:                                # %while.body
                                        #   in Loop: Header=BB17_2 Depth=1
	movl	12(%r15), %esi
	movq	%r12, %rdi
	jmp	.LBB17_2
.LBB17_3:                               # %while.cond.while.end_crit_edge
	imulq	$88, %rax, %rcx
	addq	16(%r15), %rcx
	movq	%rax, (%r14)
	movq	%rcx, %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end17:
	.size	_ZN7netlist18get_random_elementEPllP3Rng, .Lfunc_end17-_ZN7netlist18get_random_elementEPllP3Rng
	.cfi_endproc
                                        # -- End function
	.globl	_ZN7netlist21netlist_elem_from_locER10location_t # -- Begin function _ZN7netlist21netlist_elem_from_locER10location_t
	.p2align	4, 0x90
	.type	_ZN7netlist21netlist_elem_from_locER10location_t,@function
_ZN7netlist21netlist_elem_from_locER10location_t: # @_ZN7netlist21netlist_elem_from_locER10location_t
	.cfi_startproc
# %bb.0:                                # %entry
	xorl	%eax, %eax
	retq
.Lfunc_end18:
	.size	_ZN7netlist21netlist_elem_from_locER10location_t, .Lfunc_end18-_ZN7netlist21netlist_elem_from_locER10location_t
	.cfi_endproc
                                        # -- End function
	.globl	_ZN7netlist22netlist_elem_from_nameERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE # -- Begin function _ZN7netlist22netlist_elem_from_nameERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align	4, 0x90
	.type	_ZN7netlist22netlist_elem_from_nameERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN7netlist22netlist_elem_from_nameERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN7netlist22netlist_elem_from_nameERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rax
	.cfi_def_cfa_offset 16
	addq	$64, %rdi
	callq	_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_
	movq	(%rax), %rax
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end19:
	.size	_ZN7netlist22netlist_elem_from_nameERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end19-_ZN7netlist22netlist_elem_from_nameERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_,"axG",@progbits,_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_,comdat
	.weak	_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_ # -- Begin function _ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_
	.p2align	4, 0x90
	.type	_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_,@function
_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_: # @_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	16(%rdi), %r12
	leaq	8(%rdi), %rbp
	testq	%r12, %r12
	je	.LBB20_17
# %bb.1:                                # %while.body.lr.ph.i.i.i
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	(%rsi), %r13
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	8(%rsi), %rbx
	movl	$-2147483648, %r15d     # imm = 0x80000000
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	jmp	.LBB20_2
	.p2align	4, 0x90
.LBB20_6:                               # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit.i.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	testl	%eax, %eax
	js	.LBB20_8
.LBB20_7:                               # %if.then.i.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	%r12, %rax
	addq	$16, %rax
	movq	%r12, %rbp
.LBB20_9:                               # %if.end.i.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	(%rax), %r12
	testq	%r12, %r12
	je	.LBB20_10
.LBB20_2:                               # %while.body.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rdx
	movq	%rdx, %r14
	subq	%rbx, %r14
	cmovaq	%rbx, %rdx
	testq	%rdx, %rdx
	je	.LBB20_4
# %bb.3:                                # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i.i.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	32(%r12), %rdi
	movq	%r13, %rsi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB20_6
.LBB20_4:                               # %if.then.i.i.i.i.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	cmpq	$2147483647, %r14       # imm = 0x7FFFFFFF
	jg	.LBB20_7
# %bb.5:                                # %if.else.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	cmpq	$-2147483648, %r14      # imm = 0x80000000
	cmovlel	%r15d, %r14d
	movl	%r14d, %eax
	jmp	.LBB20_6
	.p2align	4, 0x90
.LBB20_8:                               # %if.else.i.i.i
                                        #   in Loop: Header=BB20_2 Depth=1
	addq	$24, %r12
	movq	%r12, %rax
	jmp	.LBB20_9
.LBB20_10:                              # %_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEE11lower_boundERSB_.exit
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, %rbp
	je	.LBB20_11
# %bb.12:                               # %lor.rhs
	movq	40(%rbp), %rax
	movq	%rbx, %r14
	subq	%rax, %r14
	cmovaq	%rax, %rbx
	testq	%rbx, %rbx
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB20_14
# %bb.13:                               # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i
	movq	32(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB20_16
.LBB20_14:                              # %if.then.i.i.i18
	cmpq	$2147483647, %r14       # imm = 0x7FFFFFFF
	jg	.LBB20_18
# %bb.15:                               # %if.else.i.i.i.i
	cmpq	$-2147483648, %r14      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%r14d, %eax
.LBB20_16:                              # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit
	testl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	jns	.LBB20_18
	jmp	.LBB20_17
.LBB20_11:
	movq	%rax, %rbp
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB20_17:                              # %if.then
	movq	%rsi, 32(%rsp)
	leaq	32(%rsp), %rcx
	movq	%rsp, %r8
	movl	$_ZStL19piecewise_construct, %edx
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_
	movq	%rax, %rbp
.LBB20_18:                              # %if.end
	addq	$64, %rbp
	movq	%rbp, %rax
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end20:
	.size	_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_, .Lfunc_end20-_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_,comdat
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_ # -- Begin function _ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_,@function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_: # @_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# %bb.0:                                # %invoke.cont
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r13
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	movq	%r15, %r8
	callq	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_
	leaq	32(%r13), %rbp
.Ltmp91:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_
.Ltmp92:
# %bb.1:                                # %invoke.cont11
	movq	%rax, %rbx
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.LBB21_13
# %bb.2:                                # %if.then
	movb	$1, %al
	testq	%rbx, %rbx
	jne	.LBB21_9
# %bb.3:                                # %lor.lhs.false.i
	leaq	8(%r14), %rcx
	cmpq	%rcx, %r15
	je	.LBB21_9
# %bb.4:                                # %lor.rhs.i
	movq	40(%r13), %rdx
	movq	40(%r15), %rax
	movq	%rdx, %rbx
	subq	%rax, %rbx
	cmovaq	%rax, %rdx
	testq	%rdx, %rdx
	je	.LBB21_6
# %bb.5:                                # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i.i
	movq	32(%r15), %rsi
	movq	(%rbp), %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB21_8
.LBB21_6:                               # %if.then.i.i.i.i
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	cmpq	$2147483647, %rbx       # imm = 0x7FFFFFFF
	jg	.LBB21_8
# %bb.7:                                # %if.else.i.i.i.i.i
	cmpq	$-2147483648, %rbx      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%ebx, %eax
.LBB21_8:                               # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit.i
	shrl	$31, %eax
.LBB21_9:                               # %invoke.cont14
	leaq	8(%r14), %rcx
	movzbl	%al, %edi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_
	addq	$1, 40(%r14)
	jmp	.LBB21_16
.LBB21_13:                              # %if.end
	movq	32(%r13), %rdi
	movq	%r13, %rax
	addq	$48, %rax
	cmpq	%rax, %rdi
	je	.LBB21_15
# %bb.14:                               # %if.then.i.i.i.i.i.i.i
	callq	_ZdlPv
.LBB21_15:                              # %_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE12_M_drop_nodeEPSt13_Rb_tree_nodeISA_E.exit
	movq	%r13, %rdi
	callq	_ZdlPv
	movq	%rbx, %r13
.LBB21_16:                              # %cleanup
	movq	%r13, %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.LBB21_10:                              # %lpad
	.cfi_def_cfa_offset 64
.Ltmp93:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	32(%r13), %rdi
	movq	%r13, %rax
	addq	$48, %rax
	cmpq	%rax, %rdi
	je	.LBB21_12
# %bb.11:                               # %if.then.i.i.i.i.i.i.i39
	callq	_ZdlPv
.LBB21_12:                              # %_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE12_M_drop_nodeEPSt13_Rb_tree_nodeISA_E.exit40
	movq	%r13, %rdi
	callq	_ZdlPv
.Ltmp94:
	callq	__cxa_rethrow
.Ltmp95:
# %bb.20:                               # %unreachable
.LBB21_17:                              # %lpad18
.Ltmp96:
	movq	%rax, %rbx
.Ltmp97:
	callq	__cxa_end_catch
.Ltmp98:
# %bb.18:                               # %invoke.cont19
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB21_19:                              # %terminate.lpad
.Ltmp99:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_, .Lfunc_end21-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEESt17_Rb_tree_iteratorISA_ESt23_Rb_tree_const_iteratorISA_EDpOT_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase1-.Lttbaseref1
.Lttbaseref1:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end3-.Lcst_begin3
.Lcst_begin3:
	.uleb128 .Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.uleb128 .Ltmp91-.Lfunc_begin3  #   Call between .Lfunc_begin3 and .Ltmp91
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp91-.Lfunc_begin3  # >> Call Site 2 <<
	.uleb128 .Ltmp92-.Ltmp91        #   Call between .Ltmp91 and .Ltmp92
	.uleb128 .Ltmp93-.Lfunc_begin3  #     jumps to .Ltmp93
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp92-.Lfunc_begin3  # >> Call Site 3 <<
	.uleb128 .Ltmp94-.Ltmp92        #   Call between .Ltmp92 and .Ltmp94
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp94-.Lfunc_begin3  # >> Call Site 4 <<
	.uleb128 .Ltmp95-.Ltmp94        #   Call between .Ltmp94 and .Ltmp95
	.uleb128 .Ltmp96-.Lfunc_begin3  #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp97-.Lfunc_begin3  # >> Call Site 5 <<
	.uleb128 .Ltmp98-.Ltmp97        #   Call between .Ltmp97 and .Ltmp98
	.uleb128 .Ltmp99-.Lfunc_begin3  #     jumps to .Ltmp99
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp98-.Lfunc_begin3  # >> Call Site 6 <<
	.uleb128 .Lfunc_end21-.Ltmp98   #   Call between .Ltmp98 and .Lfunc_end21
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end3:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase1:
	.p2align	2
                                        # -- End function
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_,comdat
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_ # -- Begin function _ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_,@function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_: # @_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r13
	.cfi_def_cfa_offset 32
	pushq	%r12
	.cfi_def_cfa_offset 40
	pushq	%rbx
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -48
	.cfi_offset %r12, -40
	.cfi_offset %r13, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	(%rcx), %rax
	leaq	48(%rsi), %r14
	movq	%r14, 32(%rsi)
	movq	(%rax), %r15
	movq	8(%rax), %rbx
	testq	%r15, %r15
	jne	.LBB22_4
# %bb.1:                                # %entry
	testq	%rbx, %rbx
	jne	.LBB22_2
.LBB22_4:                               # %if.end.i.i.i.i.i.i.i.i
	movq	%rbx, 8(%rsp)
	cmpq	$16, %rbx
	jb	.LBB22_7
# %bb.5:                                # %if.then4.i.i.i.i.i.i.i.i
	leaq	32(%r12), %r13
.Ltmp102:
	leaq	8(%rsp), %rsi
	movq	%r13, %rdi
	xorl	%edx, %edx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
.Ltmp103:
# %bb.6:                                # %call5.i.i.i14.i.i.i.i.i.noexc
	movq	%rax, (%r13)
	movq	8(%rsp), %rcx
	movq	%rcx, (%r14)
	movq	%rax, %r14
.LBB22_7:                               # %if.end6.i.i.i.i.i.i.i.i
	testq	%rbx, %rbx
	je	.LBB22_11
# %bb.8:                                # %if.end6.i.i.i.i.i.i.i.i
	cmpq	$1, %rbx
	jne	.LBB22_10
# %bb.9:                                # %if.then.i.i.i.i.i.i.i.i.i.i
	movb	(%r15), %al
	movb	%al, (%r14)
	jmp	.LBB22_11
.LBB22_10:                              # %if.end.i.i.i.i.i.i.i.i.i.i.i
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB22_11:                              # %try.cont
	movq	8(%rsp), %rax
	movq	%rax, 40(%r12)
	movq	32(%r12), %rcx
	movb	$0, (%rcx,%rax)
	movq	$0, 64(%r12)
	addq	$16, %rsp
	.cfi_def_cfa_offset 48
	popq	%rbx
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.LBB22_2:                               # %if.then.i.i.i.i.i.i.i.i
	.cfi_def_cfa_offset 64
.Ltmp100:
	movl	$.L.str.7.11, %edi
	callq	_ZSt19__throw_logic_errorPKc
.Ltmp101:
# %bb.3:                                # %.noexc
.LBB22_15:                              # %lpad
.Ltmp104:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	movq	%r12, %rdi
	callq	_ZdlPv
.Ltmp105:
	callq	__cxa_rethrow
.Ltmp106:
# %bb.16:                               # %unreachable
.LBB22_12:                              # %lpad11
.Ltmp107:
	movq	%rax, %rbx
.Ltmp108:
	callq	__cxa_end_catch
.Ltmp109:
# %bb.13:                               # %eh.resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB22_14:                              # %terminate.lpad
.Ltmp110:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end22:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_, .Lfunc_end22-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE17_M_construct_nodeIJRKSt21piecewise_construct_tSt5tupleIJRS7_EESL_IJEEEEEvPSt13_Rb_tree_nodeISA_EDpOT_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase2-.Lttbaseref2
.Lttbaseref2:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end4-.Lcst_begin4
.Lcst_begin4:
	.uleb128 .Ltmp102-.Lfunc_begin4 # >> Call Site 1 <<
	.uleb128 .Ltmp103-.Ltmp102      #   Call between .Ltmp102 and .Ltmp103
	.uleb128 .Ltmp104-.Lfunc_begin4 #     jumps to .Ltmp104
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp103-.Lfunc_begin4 # >> Call Site 2 <<
	.uleb128 .Ltmp100-.Ltmp103      #   Call between .Ltmp103 and .Ltmp100
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp100-.Lfunc_begin4 # >> Call Site 3 <<
	.uleb128 .Ltmp101-.Ltmp100      #   Call between .Ltmp100 and .Ltmp101
	.uleb128 .Ltmp104-.Lfunc_begin4 #     jumps to .Ltmp104
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp101-.Lfunc_begin4 # >> Call Site 4 <<
	.uleb128 .Ltmp105-.Ltmp101      #   Call between .Ltmp101 and .Ltmp105
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp105-.Lfunc_begin4 # >> Call Site 5 <<
	.uleb128 .Ltmp106-.Ltmp105      #   Call between .Ltmp105 and .Ltmp106
	.uleb128 .Ltmp107-.Lfunc_begin4 #     jumps to .Ltmp107
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp108-.Lfunc_begin4 # >> Call Site 6 <<
	.uleb128 .Ltmp109-.Ltmp108      #   Call between .Ltmp108 and .Ltmp109
	.uleb128 .Ltmp110-.Lfunc_begin4 #     jumps to .Ltmp110
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp109-.Lfunc_begin4 # >> Call Site 7 <<
	.uleb128 .Lfunc_end22-.Ltmp109  #   Call between .Ltmp109 and .Lfunc_end22
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end4:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase2:
	.p2align	2
                                        # -- End function
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_,comdat
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_ # -- Begin function _ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_,@function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_: # @_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	leaq	8(%rdi), %rax
	cmpq	%rsi, %rax
	je	.LBB23_9
# %bb.1:                                # %if.else12
	movq	%rsi, %r13
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%rbp, %r15
	movq	8(%rbp), %r14
	movq	40(%rsi), %r12
	movq	%r14, %rbp
	subq	%r12, %rbp
	movq	%r14, %rbx
	cmovaq	%r12, %rbx
	testq	%rbx, %rbx
	je	.LBB23_4
# %bb.2:                                # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i177
	movq	32(%r13), %rsi
	movq	(%r15), %rdi
	movq	%rbx, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB23_4
# %bb.3:                                # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit185
	testl	%eax, %eax
	jns	.LBB23_6
	jmp	.LBB23_19
.LBB23_4:                               # %if.then.i.i.i180
	cmpq	$2147483647, %rbp       # imm = 0x7FFFFFFF
	jg	.LBB23_6
# %bb.5:                                # %if.else.i.i.i.i182
	cmpq	$-2147483648, %rbp      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%ebp, %eax
	testl	%eax, %eax
	js	.LBB23_19
.LBB23_6:                               # %if.else44
	testq	%rbx, %rbx
	je	.LBB23_16
# %bb.7:                                # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i105
	movq	(%r15), %rsi
	movq	32(%r13), %rdi
	movq	%rbx, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB23_16
# %bb.8:                                # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit113
	testl	%eax, %eax
	jns	.LBB23_18
	jmp	.LBB23_26
.LBB23_9:                               # %if.then
	cmpq	$0, 40(%rbx)
	je	.LBB23_15
# %bb.10:                               # %land.lhs.true
	movq	32(%rbx), %r12
	movq	40(%r12), %rdx
	movq	8(%rbp), %rax
	movq	%rdx, %r14
	subq	%rax, %r14
	cmovaq	%rax, %rdx
	testq	%rdx, %rdx
	je	.LBB23_12
# %bb.11:                               # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i127
	movq	(%rbp), %rsi
	movq	32(%r12), %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB23_14
.LBB23_12:                              # %if.then.i.i.i130
	cmpq	$2147483647, %r14       # imm = 0x7FFFFFFF
	jg	.LBB23_15
# %bb.13:                               # %if.else.i.i.i.i132
	cmpq	$-2147483648, %r14      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%r14d, %eax
.LBB23_14:                              # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit135
	testl	%eax, %eax
	js	.LBB23_37
.LBB23_15:                              # %if.else
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	jmp	.LBB23_34
.LBB23_16:                              # %if.then.i.i.i108
	subq	%r14, %r12
	cmpq	$2147483647, %r12       # imm = 0x7FFFFFFF
	jg	.LBB23_18
# %bb.17:                               # %if.else.i.i.i.i110
	cmpq	$-2147483648, %r12      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%r12d, %eax
	testl	%eax, %eax
	js	.LBB23_26
.LBB23_18:
	xorl	%r12d, %r12d
	movq	%r13, %rax
	jmp	.LBB23_35
.LBB23_19:                              # %if.then18
	movq	(%rsp), %rax            # 8-byte Reload
	movq	24(%rax), %rax
	movq	%rax, %r12
	cmpq	%r13, %rax
	je	.LBB23_35
# %bb.20:                               # %if.else25
	movq	%r13, %rdi
	callq	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base
	movq	%rax, %r12
	movq	40(%rax), %rdx
	movq	%rdx, %rbx
	subq	%r14, %rbx
	cmovaq	%r14, %rdx
	testq	%rdx, %rdx
	movq	(%rsp), %r14            # 8-byte Reload
	je	.LBB23_22
# %bb.21:                               # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i149
	movq	(%r15), %rsi
	movq	32(%r12), %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB23_24
.LBB23_22:                              # %if.then.i.i.i152
	cmpq	$2147483647, %rbx       # imm = 0x7FFFFFFF
	jg	.LBB23_25
# %bb.23:                               # %if.else.i.i.i.i154
	cmpq	$-2147483648, %rbx      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%ebx, %eax
.LBB23_24:                              # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit157
	testl	%eax, %eax
	js	.LBB23_38
.LBB23_25:                              # %if.else42
	movq	%r14, %rdi
	jmp	.LBB23_33
.LBB23_26:                              # %if.then50
	movq	(%rsp), %rax            # 8-byte Reload
	movq	32(%rax), %r12
	cmpq	%r13, %r12
	je	.LBB23_37
# %bb.27:                               # %if.else57
	movq	%r13, %rdi
	callq	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base
	movq	%rax, %r12
	movq	40(%rax), %rax
	movq	%r14, %rbx
	subq	%rax, %rbx
	cmovaq	%rax, %r14
	testq	%r14, %r14
	je	.LBB23_29
# %bb.28:                               # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i
	movq	32(%r12), %rsi
	movq	(%r15), %rdi
	movq	%r14, %rdx
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB23_31
.LBB23_29:                              # %if.then.i.i.i
	cmpq	$2147483647, %rbx       # imm = 0x7FFFFFFF
	jg	.LBB23_32
# %bb.30:                               # %if.else.i.i.i.i
	cmpq	$-2147483648, %rbx      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%ebx, %eax
.LBB23_31:                              # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit
	testl	%eax, %eax
	js	.LBB23_39
.LBB23_32:                              # %if.else74
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB23_33:                              # %cleanup80
	movq	%r15, %rsi
.LBB23_34:                              # %cleanup80
	callq	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_
	movq	%rdx, %r12
.LBB23_35:                              # %cleanup80
	movq	%r12, %rdx
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.LBB23_37:                              # %if.then54
	.cfi_def_cfa_offset 64
	xorl	%eax, %eax
	jmp	.LBB23_35
.LBB23_38:                              # %if.then32
	movq	24(%r12), %rax
	testq	%rax, %rax
	cmovneq	%r13, %rax
	cmovneq	%r13, %r12
	jmp	.LBB23_35
.LBB23_39:                              # %if.then64
	movq	24(%r13), %rax
	testq	%rax, %rax
	cmovneq	%r12, %rax
	cmoveq	%r13, %r12
	jmp	.LBB23_35
.Lfunc_end23:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_, .Lfunc_end23-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorISA_ERS7_
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_,comdat
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_ # -- Begin function _ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_,@function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_: # @_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB24_19
# %bb.1:                                # %while.body.lr.ph
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	(%rbp), %r12
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	8(%rbp), %r15
	movl	$2147483648, %ebp       # imm = 0x80000000
	movl	$-2147483648, %r14d     # imm = 0x80000000
	jmp	.LBB24_5
	.p2align	4, 0x90
.LBB24_2:                               # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit48
                                        #   in Loop: Header=BB24_5 Depth=1
	testl	%eax, %eax
	js	.LBB24_9
.LBB24_3:                               # %cond.false
                                        #   in Loop: Header=BB24_5 Depth=1
	movq	%r13, %rcx
	addq	$24, %rcx
	xorl	%eax, %eax
.LBB24_4:                               # %cond.end
                                        #   in Loop: Header=BB24_5 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB24_10
.LBB24_5:                               # %while.body
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %r13
	movq	40(%rcx), %rdx
	movq	%r15, %rbx
	subq	%rdx, %rbx
	cmovbeq	%r15, %rdx
	testq	%rdx, %rdx
	je	.LBB24_7
# %bb.6:                                # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i40
                                        #   in Loop: Header=BB24_5 Depth=1
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB24_2
.LBB24_7:                               # %if.then.i.i.i43
                                        #   in Loop: Header=BB24_5 Depth=1
	cmpq	%rbp, %rbx
	jge	.LBB24_3
# %bb.8:                                # %if.else.i.i.i.i45
                                        #   in Loop: Header=BB24_5 Depth=1
	cmpq	$-2147483648, %rbx      # imm = 0x80000000
	cmovlel	%r14d, %ebx
	movl	%ebx, %eax
	jmp	.LBB24_2
	.p2align	4, 0x90
.LBB24_9:                               # %cond.true
                                        #   in Loop: Header=BB24_5 Depth=1
	leaq	16(%r13), %rcx
	movb	$1, %al
	jmp	.LBB24_4
.LBB24_10:                              # %while.end
	movq	%r13, %r12
	testb	%al, %al
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	je	.LBB24_13
# %bb.11:                               # %if.then
	cmpq	%r13, 24(%rdi)
	jne	.LBB24_12
	jmp	.LBB24_21
.LBB24_19:                              # %while.end.thread
	leaq	8(%rdi), %r13
	cmpq	%r13, 24(%rdi)
	je	.LBB24_21
.LBB24_12:                              # %if.else
	movq	%r13, %rdi
	callq	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base
	movq	%rax, %r12
.LBB24_13:                              # %if.end12
	movq	40(%r12), %rdx
	movq	8(%rbp), %rax
	movq	%rdx, %rbx
	subq	%rax, %rbx
	cmovaq	%rax, %rdx
	testq	%rdx, %rdx
	je	.LBB24_16
# %bb.14:                               # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i
	movq	(%rbp), %rsi
	movq	32(%r12), %rdi
	callq	memcmp
	testl	%eax, %eax
	je	.LBB24_16
# %bb.15:                               # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit
	testl	%eax, %eax
	jns	.LBB24_18
	jmp	.LBB24_21
.LBB24_16:                              # %if.then.i.i.i
	cmpq	$2147483647, %rbx       # imm = 0x7FFFFFFF
	jg	.LBB24_18
# %bb.17:                               # %if.else.i.i.i.i
	cmpq	$-2147483648, %rbx      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%ebx, %eax
	testl	%eax, %eax
	js	.LBB24_21
.LBB24_18:                              # %if.end18
	xorl	%r13d, %r13d
	jmp	.LBB24_22
.LBB24_21:
	xorl	%r12d, %r12d
.LBB24_22:                              # %cleanup
	movq	%r12, %rax
	movq	%r13, %rdx
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end24:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_, .Lfunc_end24-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE24_M_get_insert_unique_posERS7_
	.cfi_endproc
                                        # -- End function
	.text
	.globl	_ZN7netlistC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE # -- Begin function _ZN7netlistC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align	4, 0x90
	.type	_ZN7netlistC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN7netlistC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN7netlistC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$712, %rsp              # imm = 0x2C8
	.cfi_def_cfa_offset 768
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	leaq	16(%rdi), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	40(%rdi), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	64(%rdi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	72(%rdi), %rax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 16(%rdi)
	movl	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	%rax, 88(%rdi)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 96(%rdi)
	movq	$0, 104(%rdi)
	movq	(%rsi), %rsi
.Ltmp111:
	leaq	192(%rsp), %rdi
	movl	$8, %edx
	callq	_ZNSt14basic_ifstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode
.Ltmp112:
# %bb.1:                                # %invoke.cont
.Ltmp114:
	leaq	192(%rsp), %rdi
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	%r15, %rsi
	callq	_ZNSi10_M_extractIjEERSiRT_
.Ltmp115:
# %bb.2:                                # %invoke.cont3
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	4(%rcx), %r14
.Ltmp116:
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZNSi10_M_extractIjEERSiRT_
.Ltmp117:
# %bb.3:                                # %invoke.cont5
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	8(%rcx), %r12
.Ltmp118:
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	_ZNSi10_M_extractIjEERSiRT_
.Ltmp119:
# %bb.4:                                # %invoke.cont7
	movq	(%rsp), %rax            # 8-byte Reload
	movl	8(%rax), %esi
	imull	4(%rax), %esi
	movl	%esi, 12(%rax)
	movq	24(%rax), %rbp
	movq	16(%rax), %r15
	movq	%rbp, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	movabsq	$3353953467947191203, %rcx # imm = 0x2E8BA2E8BA2E8BA3
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	jae	.LBB25_6
# %bb.5:                                # %if.then.i
	subq	%rax, %rsi
.Ltmp120:
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	_ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm
.Ltmp121:
	jmp	.LBB25_17
.LBB25_6:                               # %if.else.i
	jbe	.LBB25_17
# %bb.7:                                # %if.then5.i
	imulq	$88, %rsi, %rax
	addq	%rax, %r15
	cmpq	%rbp, %r15
	je	.LBB25_17
# %bb.8:                                # %for.body.i.i.i.i443.preheader
	movq	%r15, %rbx
	jmp	.LBB25_9
	.p2align	4, 0x90
.LBB25_15:                              # %_ZSt8_DestroyI12netlist_elemEvPT_.exit.i.i.i.i456
                                        #   in Loop: Header=BB25_9 Depth=1
	addq	$88, %rbx
	cmpq	%rbx, %rbp
	je	.LBB25_16
.LBB25_9:                               # %for.body.i.i.i.i443
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_11
# %bb.10:                               # %if.then.i.i.i.i.i.i.i.i.i444
                                        #   in Loop: Header=BB25_9 Depth=1
	callq	_ZdlPv
.LBB25_11:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit.i.i.i.i.i.i447
                                        #   in Loop: Header=BB25_9 Depth=1
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_13
# %bb.12:                               # %if.then.i.i.i4.i.i.i.i.i.i448
                                        #   in Loop: Header=BB25_9 Depth=1
	callq	_ZdlPv
.LBB25_13:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit5.i.i.i.i.i.i452
                                        #   in Loop: Header=BB25_9 Depth=1
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rdi, %rax
	je	.LBB25_15
# %bb.14:                               # %if.then.i.i.i.i.i.i.i.i453
                                        #   in Loop: Header=BB25_9 Depth=1
	callq	_ZdlPv
	jmp	.LBB25_15
.LBB25_16:                              # %invoke.cont.i457
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r15, 24(%rax)
.LBB25_17:                              # %invoke.cont13
.Ltmp122:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.12, %esi
	movl	$12, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp123:
# %bb.18:                               # %invoke.cont14
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB25_19
# %bb.21:                               # %call.i.noexc163
	cmpb	$0, 56(%rbx)
	je	.LBB25_23
# %bb.22:                               # %if.then.i166
	movb	67(%rbx), %al
	jmp	.LBB25_25
.LBB25_23:                              # %if.end.i
.Ltmp124:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp125:
# %bb.24:                               # %.noexc168
	movq	(%rbx), %rax
.Ltmp126:
	movq	%rbx, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.Ltmp127:
.LBB25_25:                              # %call.i.noexc
.Ltmp128:
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
.Ltmp129:
# %bb.26:                               # %call1.i.noexc
.Ltmp130:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp131:
# %bb.27:                               # %invoke.cont16
	movl	(%r12), %ebx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 96(%rsp)
	movq	$0, 112(%rsp)
	testq	%rbx, %rbx
	je	.LBB25_28
# %bb.29:                               # %_ZNSt16allocator_traitsISaI10location_tEE8allocateERS1_m.exit.i.i.i.i
	leaq	(,%rbx,8), %rdi
.Ltmp132:
	callq	_Znwm
.Ltmp133:
	jmp	.LBB25_30
.LBB25_28:
	xorl	%eax, %eax
.LBB25_30:                              # %call2.i.i.i.i3.i.i.noexc
	movq	(%rsp), %r15            # 8-byte Reload
	movq	%rax, 96(%rsp)
	movq	%rax, 104(%rsp)
	leaq	(%rax,%rbx,8), %rbp
	movq	%rbp, 112(%rsp)
	testl	%ebx, %ebx
	je	.LBB25_32
# %bb.31:                               # %for.body.i.i.preheader.i.i.i.i.i
	shlq	$3, %rbx
	movq	%rax, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	callq	memset
	movq	%rbp, %rax
.LBB25_32:                              # %invoke.cont21
	movq	%rax, 104(%rsp)
	movl	4(%r15), %edx
	movq	%r15, %rax
	movq	48(%r15), %r15
	movq	40(%rax), %rax
	movq	%r15, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	movabsq	$-6148914691236517205, %r13 # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%r13, %rcx
	cmpq	%rdx, %rcx
	jae	.LBB25_34
# %bb.33:                               # %if.then.i176
	subq	%rcx, %rdx
.Ltmp135:
	leaq	96(%rsp), %rcx
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	%r15, %rsi
	callq	_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_
.Ltmp136:
	jmp	.LBB25_41
.LBB25_34:                              # %if.else.i177
	jbe	.LBB25_41
# %bb.35:                               # %if.then7.i
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%rax,%rcx,8), %rbx
	cmpq	%r15, %rbx
	je	.LBB25_41
# %bb.36:                               # %for.body.i.i.i.i.i.preheader
	movq	%rbx, %rbp
	jmp	.LBB25_37
	.p2align	4, 0x90
.LBB25_39:                              # %_ZSt8_DestroyISt6vectorI10location_tSaIS1_EEEvPT_.exit.i.i.i.i.i
                                        #   in Loop: Header=BB25_37 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %r15
	je	.LBB25_40
.LBB25_37:                              # %for.body.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB25_39
# %bb.38:                               # %if.then.i.i.i.i.i.i.i.i.i181
                                        #   in Loop: Header=BB25_37 Depth=1
	callq	_ZdlPv
	jmp	.LBB25_39
.LBB25_40:                              # %invoke.cont.i.i
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, 48(%rax)
.LBB25_41:                              # %_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE6resizeEmRKS2_.exit
	movl	(%r14), %edx
	testl	%edx, %edx
	movq	(%rsp), %r15            # 8-byte Reload
	je	.LBB25_53
# %bb.42:                               # %for.cond28.preheader.preheader
	movl	(%r12), %eax
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	movabsq	$3353953467947191203, %rcx # imm = 0x2E8BA2E8BA2E8BA3
	testl	%eax, %eax
	jne	.LBB25_56
	.p2align	4, 0x90
.LBB25_44:
	xorl	%eax, %eax
.LBB25_52:                              # %for.cond.cleanup31
	addq	$1, %r8
	movl	%edx, %esi
	cmpq	%rsi, %r8
	jae	.LBB25_53
# %bb.43:                               # %for.cond28.preheader
	testl	%eax, %eax
	je	.LBB25_44
.LBB25_56:                              # %for.body32.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_57:                              # %for.body32
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_67 Depth 2
	movq	40(%r15), %rsi
	movq	48(%r15), %rdx
	subq	%rsi, %rdx
	sarq	$3, %rdx
	imulq	%r13, %rdx
	cmpq	%r8, %rdx
	jbe	.LBB25_58
# %bb.60:                               # %invoke.cont36
                                        #   in Loop: Header=BB25_57 Depth=1
	leaq	(%r8,%r8,2), %rdx
	movq	(%rsi,%rdx,8), %rax
	movq	8(%rsi,%rdx,8), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rbp, %rdx
	jbe	.LBB25_61
# %bb.63:                               # %invoke.cont39
                                        #   in Loop: Header=BB25_57 Depth=1
	movl	%r8d, (%rax,%rbp,8)
	movl	%ebp, 4(%rax,%rbp,8)
	movl	%edi, %esi
	movq	16(%r15), %rbx
	movq	24(%r15), %rdx
	subq	%rbx, %rdx
	sarq	$3, %rdx
	imulq	%rcx, %rdx
	cmpq	%rsi, %rdx
	jbe	.LBB25_64
# %bb.66:                               # %invoke.cont45
                                        #   in Loop: Header=BB25_57 Depth=1
	leaq	(%rax,%rbp,8), %rdx
	imulq	$88, %rsi, %rax
	leaq	(%rbx,%rax), %rsi
	addq	$80, %rsi
	.p2align	4, 0x90
.LBB25_67:                              # %do.body.i.i.i
                                        #   Parent Loop BB25_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, (%rsi)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB25_67
# %bb.68:                               # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit.i.i
                                        #   in Loop: Header=BB25_67 Depth=2
	#APP
	lock
	cmpxchgq	%rdx, (%rsi)
	sete	%al
	movzbq	%al, %rax
.Ltmp227:	# atomic_cmpset_long
	#NO_APP
	testl	%eax, %eax
	je	.LBB25_67
# %bb.69:                               # %invoke.cont47
                                        #   in Loop: Header=BB25_57 Depth=1
	addl	$1, %edi
	addq	$1, %rbp
	movl	(%r12), %eax
	cmpq	%rax, %rbp
	jb	.LBB25_57
# %bb.51:                               # %for.cond.cleanup31.loopexit
	movl	(%r14), %edx
	jmp	.LBB25_52
.LBB25_53:                              # %for.cond.cleanup
.Ltmp145:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.1.13, %esi
	movl	$13, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp146:
# %bb.54:                               # %invoke.cont54
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	_ZSt4cout+240(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB25_55
# %bb.72:                               # %call.i464.noexc
	cmpb	$0, 56(%rbx)
	je	.LBB25_74
# %bb.73:                               # %if.then.i471
	movb	67(%rbx), %al
	jmp	.LBB25_76
.LBB25_74:                              # %if.end.i475
.Ltmp147:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp148:
# %bb.75:                               # %.noexc477
	movq	(%rbx), %rax
.Ltmp149:
	movq	%rbx, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.Ltmp150:
.LBB25_76:                              # %call.i.noexc212
.Ltmp151:
	movsbl	%al, %esi
	movl	$_ZSt4cout, %edi
	callq	_ZNSo3putEc
.Ltmp152:
# %bb.77:                               # %call1.i.noexc214
.Ltmp153:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp154:
# %bb.78:                               # %invoke.cont59.preheader
	movq	192(%rsp), %rax
	movq	-24(%rax), %rax
	xorl	%ebx, %ebx
	testb	$2, 224(%rsp,%rax)
	jne	.LBB25_186
# %bb.79:                               # %while.body.lr.ph
	xorl	%ebx, %ebx
	jmp	.LBB25_80
	.p2align	4, 0x90
.LBB25_185:                             # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit366
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	192(%rsp), %rax
	movq	-24(%rax), %rax
	testb	$2, 224(%rsp,%rax)
	jne	.LBB25_186
.LBB25_80:                              # %while.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_95 Depth 2
                                        #     Child Loop BB25_120 Depth 2
                                        #       Child Loop BB25_125 Depth 3
	addl	$1, %ebx
	imull	$197912093, %ebx, %eax  # imm = 0xBCBE61D
	rorl	$5, %eax
	cmpl	$42949, %eax            # imm = 0xA7C5
	ja	.LBB25_92
# %bb.81:                               # %if.then
                                        #   in Loop: Header=BB25_80 Depth=1
.Ltmp155:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.2.14, %esi
	movl	$18, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp156:
# %bb.82:                               # %invoke.cont63
                                        #   in Loop: Header=BB25_80 Depth=1
.Ltmp157:
	movl	$_ZSt4cout, %edi
	movl	%ebx, %esi
	callq	_ZNSolsEi
.Ltmp158:
# %bb.83:                               # %invoke.cont65
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB25_84
# %bb.86:                               # %call.i481.noexc
                                        #   in Loop: Header=BB25_80 Depth=1
	cmpb	$0, 56(%rbp)
	je	.LBB25_88
# %bb.87:                               # %if.then.i488
                                        #   in Loop: Header=BB25_80 Depth=1
	movb	67(%rbp), %al
	jmp	.LBB25_90
.LBB25_88:                              # %if.end.i492
                                        #   in Loop: Header=BB25_80 Depth=1
.Ltmp159:
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp160:
# %bb.89:                               # %.noexc494
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	(%rbp), %rax
.Ltmp161:
	movq	%rbp, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.Ltmp162:
.LBB25_90:                              # %call.i.noexc235
                                        #   in Loop: Header=BB25_80 Depth=1
.Ltmp163:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
.Ltmp164:
# %bb.91:                               # %call1.i.noexc237
                                        #   in Loop: Header=BB25_80 Depth=1
.Ltmp165:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp166:
.LBB25_92:                              # %if.end
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	%rbx, 152(%rsp)         # 8-byte Spill
	leaq	80(%rsp), %rax
	movq	%rax, 64(%rsp)
	movq	$0, 72(%rsp)
	movb	$0, 80(%rsp)
.Ltmp168:
	leaq	192(%rsp), %rdi
	leaq	64(%rsp), %rsi
	callq	_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE
.Ltmp169:
# %bb.93:                               # %invoke.cont70
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB25_111
# %bb.94:                               # %while.body.lr.ph.i.i
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	64(%rsp), %r14
	movq	72(%rsp), %r12
	movq	8(%rsp), %r15           # 8-byte Reload
	jmp	.LBB25_95
	.p2align	4, 0x90
.LBB25_99:                              # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit.i.i
                                        #   in Loop: Header=BB25_95 Depth=2
	testl	%eax, %eax
	js	.LBB25_103
.LBB25_100:                             # %if.then.i.i260
                                        #   in Loop: Header=BB25_95 Depth=2
	movq	%rbx, %rax
	addq	$16, %rax
	movq	%rbx, %r15
.LBB25_104:                             # %if.end.i.i
                                        #   in Loop: Header=BB25_95 Depth=2
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB25_105
.LBB25_95:                              # %while.body.i.i
                                        #   Parent Loop BB25_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%rbx), %rdx
	movq	%rdx, %rbp
	subq	%r12, %rbp
	cmovaq	%r12, %rdx
	testq	%rdx, %rdx
	je	.LBB25_97
# %bb.96:                               # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i.i.i
                                        #   in Loop: Header=BB25_95 Depth=2
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB25_99
.LBB25_97:                              # %if.then.i.i.i.i.i
                                        #   in Loop: Header=BB25_95 Depth=2
	cmpq	$2147483647, %rbp       # imm = 0x7FFFFFFF
	jg	.LBB25_100
# %bb.98:                               # %if.else.i.i.i.i.i.i
                                        #   in Loop: Header=BB25_95 Depth=2
	cmpq	$-2147483648, %rbp      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovlel	%eax, %ebp
	movl	%ebp, %eax
	jmp	.LBB25_99
	.p2align	4, 0x90
.LBB25_103:                             # %if.else.i.i
                                        #   in Loop: Header=BB25_95 Depth=2
	addq	$24, %rbx
	movq	%rbx, %rax
	jmp	.LBB25_104
	.p2align	4, 0x90
.LBB25_105:                             # %_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE14_M_lower_boundEPSt13_Rb_tree_nodeISA_EPSt18_Rb_tree_node_baseRS7_.exit.i
                                        #   in Loop: Header=BB25_80 Depth=1
	cmpq	8(%rsp), %r15           # 8-byte Folded Reload
	je	.LBB25_111
# %bb.106:                              # %lor.lhs.false.i
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	40(%r15), %rax
	movq	%r12, %rbx
	subq	%rax, %rbx
	cmovaq	%rax, %r12
	testq	%r12, %r12
	je	.LBB25_108
# %bb.107:                              # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i.i
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	32(%r15), %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB25_108
# %bb.110:                              # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit.i
                                        #   in Loop: Header=BB25_80 Depth=1
	testl	%eax, %eax
	jns	.LBB25_116
	jmp	.LBB25_111
	.p2align	4, 0x90
.LBB25_108:                             # %if.then.i.i.i.i
                                        #   in Loop: Header=BB25_80 Depth=1
	cmpq	$2147483647, %rbx       # imm = 0x7FFFFFFF
	jg	.LBB25_116
# %bb.109:                              # %if.else.i.i.i.i.i
                                        #   in Loop: Header=BB25_80 Depth=1
	cmpq	$-2147483648, %rbx      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovlel	%eax, %ebx
	movl	%ebx, %eax
	testl	%eax, %eax
	js	.LBB25_111
.LBB25_116:                             # %if.else.i250
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	64(%r15), %rbp
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB25_117
	.p2align	4, 0x90
.LBB25_111:                             # %if.then.i248
                                        #   in Loop: Header=BB25_80 Depth=1
	movl	_ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem(%rip), %ebx
	movq	(%rsp), %r15            # 8-byte Reload
	movq	16(%r15), %rbp
	movq	24(%r15), %rdx
	subq	%rbp, %rdx
	sarq	$3, %rdx
	movabsq	$3353953467947191203, %rax # imm = 0x2E8BA2E8BA2E8BA3
	imulq	%rax, %rdx
	cmpq	%rbx, %rdx
	jbe	.LBB25_112
# %bb.114:                              # %_ZNSt6vectorI12netlist_elemSaIS0_EE2atEm.exit.i
                                        #   in Loop: Header=BB25_80 Depth=1
.Ltmp174:
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	64(%rsp), %rsi
	callq	_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_
.Ltmp175:
# %bb.115:                              # %call8.i.noexc
                                        #   in Loop: Header=BB25_80 Depth=1
	imulq	$88, %rbx, %rcx
	addq	%rcx, %rbp
	movq	%rbp, (%rax)
	addl	$1, _ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem(%rip)
.LBB25_117:                             # %invoke.cont73
                                        #   in Loop: Header=BB25_80 Depth=1
.Ltmp176:
	movq	%rbp, %rdi
	leaq	64(%rsp), %rsi
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_
.Ltmp177:
# %bb.118:                              # %invoke.cont75
                                        #   in Loop: Header=BB25_80 Depth=1
.Ltmp179:
	leaq	192(%rsp), %rdi
	leaq	188(%rsp), %rsi
	callq	_ZNSirsERi
.Ltmp180:
# %bb.119:                              # %invoke.cont78
                                        #   in Loop: Header=BB25_80 Depth=1
	leaq	48(%rsp), %rax
	movq	%rax, 32(%rsp)
	movq	$0, 40(%rsp)
	movb	$0, 48(%rsp)
	leaq	40(%rbp), %rbx
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	leaq	48(%rbp), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	jmp	.LBB25_120
	.p2align	4, 0x90
.LBB25_166:                             # %if.then.i314
                                        #   in Loop: Header=BB25_120 Depth=2
	addq	$64, %r13
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movq	(%r13), %rax
	addq	$8, %rax
	movq	%rax, (%r13)
.LBB25_120:                             # %while.cond80
                                        #   Parent Loop BB25_80 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_125 Depth 3
.Ltmp182:
	leaq	192(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE
.Ltmp183:
# %bb.121:                              # %invoke.cont88
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	testb	$5, 32(%rax,%rcx)
	jne	.LBB25_181
# %bb.122:                              # %while.body90
                                        #   in Loop: Header=BB25_120 Depth=2
	movl	$.L.str.3.15, %esi
	leaq	32(%rsp), %rdi
	callq	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc
	testl	%eax, %eax
	je	.LBB25_181
# %bb.123:                              # %if.end94
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB25_145
# %bb.124:                              # %while.body.lr.ph.i.i503
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	32(%rsp), %r12
	movq	40(%rsp), %r14
	movq	8(%rsp), %r13           # 8-byte Reload
	jmp	.LBB25_125
	.p2align	4, 0x90
.LBB25_129:                             # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit.i.i523
                                        #   in Loop: Header=BB25_125 Depth=3
	testl	%eax, %eax
	js	.LBB25_136
.LBB25_130:                             # %if.then.i.i525
                                        #   in Loop: Header=BB25_125 Depth=3
	movq	%rbx, %rax
	addq	$16, %rax
	movq	%rbx, %r13
.LBB25_137:                             # %if.end.i.i533
                                        #   in Loop: Header=BB25_125 Depth=3
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB25_138
.LBB25_125:                             # %while.body.i.i510
                                        #   Parent Loop BB25_80 Depth=1
                                        #     Parent Loop BB25_120 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	40(%rbx), %rdx
	movq	%rdx, %rbp
	subq	%r14, %rbp
	cmovaq	%r14, %rdx
	testq	%rdx, %rdx
	je	.LBB25_127
# %bb.126:                              # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i.i.i515
                                        #   in Loop: Header=BB25_125 Depth=3
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB25_129
.LBB25_127:                             # %if.then.i.i.i.i.i518
                                        #   in Loop: Header=BB25_125 Depth=3
	cmpq	$2147483647, %rbp       # imm = 0x7FFFFFFF
	jg	.LBB25_130
# %bb.128:                              # %if.else.i.i.i.i.i.i520
                                        #   in Loop: Header=BB25_125 Depth=3
	cmpq	$-2147483648, %rbp      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovlel	%eax, %ebp
	movl	%ebp, %eax
	jmp	.LBB25_129
	.p2align	4, 0x90
.LBB25_136:                             # %if.else.i.i527
                                        #   in Loop: Header=BB25_125 Depth=3
	addq	$24, %rbx
	movq	%rbx, %rax
	jmp	.LBB25_137
	.p2align	4, 0x90
.LBB25_138:                             # %_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE14_M_lower_boundEPSt13_Rb_tree_nodeISA_EPSt18_Rb_tree_node_baseRS7_.exit.i535
                                        #   in Loop: Header=BB25_120 Depth=2
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	je	.LBB25_145
# %bb.139:                              # %lor.lhs.false.i540
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	40(%r13), %rax
	movq	%r14, %rbx
	subq	%rax, %rbx
	cmovaq	%rax, %r14
	testq	%r14, %r14
	je	.LBB25_141
# %bb.140:                              # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i.i545
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	32(%r13), %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB25_141
# %bb.143:                              # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit.i553
                                        #   in Loop: Header=BB25_120 Depth=2
	testl	%eax, %eax
	jns	.LBB25_144
	jmp	.LBB25_145
	.p2align	4, 0x90
.LBB25_141:                             # %if.then.i.i.i.i548
                                        #   in Loop: Header=BB25_120 Depth=2
	cmpq	$2147483647, %rbx       # imm = 0x7FFFFFFF
	jg	.LBB25_144
# %bb.142:                              # %if.else.i.i.i.i.i550
                                        #   in Loop: Header=BB25_120 Depth=2
	cmpq	$-2147483648, %rbx      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovlel	%eax, %ebx
	movl	%ebx, %eax
	testl	%eax, %eax
	js	.LBB25_145
.LBB25_144:                             # %call.i.i267.noexc
                                        #   in Loop: Header=BB25_120 Depth=2
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	je	.LBB25_145
# %bb.150:                              # %if.else.i283
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	64(%r13), %r13
	jmp	.LBB25_151
	.p2align	4, 0x90
.LBB25_145:                             # %if.then.i277
                                        #   in Loop: Header=BB25_120 Depth=2
	movl	_ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem(%rip), %ebx
	movq	16(%r15), %r13
	movq	24(%r15), %rdx
	subq	%r13, %rdx
	sarq	$3, %rdx
	movabsq	$3353953467947191203, %rax # imm = 0x2E8BA2E8BA2E8BA3
	imulq	%rax, %rdx
	cmpq	%rbx, %rdx
	jbe	.LBB25_146
# %bb.148:                              # %_ZNSt6vectorI12netlist_elemSaIS0_EE2atEm.exit.i281
                                        #   in Loop: Header=BB25_120 Depth=2
.Ltmp187:
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	32(%rsp), %rsi
	callq	_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_
.Ltmp188:
# %bb.149:                              # %call8.i.noexc288
                                        #   in Loop: Header=BB25_120 Depth=2
	imulq	$88, %rbx, %rcx
	addq	%rcx, %r13
	movq	%r13, (%rax)
	addl	$1, _ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem(%rip)
	movq	(%rsp), %r15            # 8-byte Reload
.LBB25_151:                             # %invoke.cont96
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	176(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx), %rbp
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpq	(%rax), %rbp
	je	.LBB25_153
# %bb.152:                              # %if.then.i293
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%r13, (%rbp)
	movq	(%rbx), %rax
	addq	$8, %rax
	movq	%rbx, %rcx
	jmp	.LBB25_165
	.p2align	4, 0x90
.LBB25_153:                             # %if.else.i294
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	subq	%rax, %rbp
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	cmpq	%rax, %rbp
	je	.LBB25_154
# %bb.156:                              # %_ZNKSt6vectorIP12netlist_elemSaIS1_EE12_M_check_lenEmPKc.exit.i
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%rbp, %r14
	sarq	$3, %r14
	testq	%rbp, %rbp
	movq	%r14, %rax
	movl	$1, %ecx
	cmoveq	%rcx, %rax
	leaq	(%rax,%r14), %rbx
	movabsq	$1152921504606846975, %rcx # imm = 0xFFFFFFFFFFFFFFF
	cmpq	%rcx, %rbx
	cmovaq	%rcx, %rbx
	addq	%r14, %rax
	cmovbq	%rcx, %rbx
	testq	%rbx, %rbx
	je	.LBB25_157
# %bb.158:                              # %_ZNSt16allocator_traitsISaIP12netlist_elemEE8allocateERS2_m.exit.i.i
                                        #   in Loop: Header=BB25_120 Depth=2
	leaq	(,%rbx,8), %rdi
.Ltmp189:
	callq	_Znwm
.Ltmp190:
# %bb.159:                              # %call2.i.i.i.i.noexc
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%rax, %r15
	jmp	.LBB25_160
.LBB25_157:                             #   in Loop: Header=BB25_120 Depth=2
	xorl	%r15d, %r15d
	xorl	%eax, %eax
.LBB25_160:                             # %_ZNSt12_Vector_baseIP12netlist_elemSaIS1_EE11_M_allocateEm.exit.i
                                        #   in Loop: Header=BB25_120 Depth=2
	leaq	(%rax,%r14,8), %r12
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	%r13, (%rax,%r14,8)
	testq	%rbp, %rbp
	movq	168(%rsp), %r14         # 8-byte Reload
	jle	.LBB25_162
# %bb.161:                              # %if.then.i.i.i.i44.i
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	memmove
.LBB25_162:                             # %_ZNSt6vectorIP12netlist_elemSaIS1_EE11_S_relocateEPS1_S4_S4_RS2_.exit.i
                                        #   in Loop: Header=BB25_120 Depth=2
	addq	$8, %r12
	testq	%r14, %r14
	je	.LBB25_164
# %bb.163:                              # %if.then.i38.i
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%r14, %rdi
	callq	_ZdlPv
.LBB25_164:                             # %.noexc296
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r15, 32(%rax)
	movq	%r12, 40(%rax)
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	(%rsp), %r15            # 8-byte Reload
.LBB25_165:                             # %invoke.cont98
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%rax, (%rcx)
	movq	64(%r13), %rbp
	cmpq	72(%r13), %rbp
	jne	.LBB25_166
# %bb.167:                              # %if.else.i315
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	56(%r13), %rdx
	subq	%rdx, %rbp
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	cmpq	%rax, %rbp
	je	.LBB25_168
# %bb.170:                              # %_ZNKSt6vectorIP12netlist_elemSaIS1_EE12_M_check_lenEmPKc.exit.i334
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%rbp, %rbx
	sarq	$3, %rbx
	testq	%rbp, %rbp
	movq	%rbx, %rax
	movl	$1, %ecx
	cmoveq	%rcx, %rax
	leaq	(%rax,%rbx), %r15
	movabsq	$1152921504606846975, %rcx # imm = 0xFFFFFFFFFFFFFFF
	cmpq	%rcx, %r15
	cmovaq	%rcx, %r15
	addq	%rbx, %rax
	cmovbq	%rcx, %r15
	testq	%r15, %r15
	je	.LBB25_171
# %bb.172:                              # %_ZNSt16allocator_traitsISaIP12netlist_elemEE8allocateERS2_m.exit.i.i336
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%rdx, %r14
	leaq	(,%r15,8), %rdi
.Ltmp191:
	callq	_Znwm
.Ltmp192:
# %bb.173:                              # %call2.i.i.i.i.noexc355
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%rax, %r12
	movq	%rax, %rcx
	movq	%r14, %rdx
	jmp	.LBB25_174
.LBB25_171:                             #   in Loop: Header=BB25_120 Depth=2
	xorl	%r12d, %r12d
	xorl	%ecx, %ecx
.LBB25_174:                             # %_ZNSt12_Vector_baseIP12netlist_elemSaIS1_EE11_M_allocateEm.exit.i340
                                        #   in Loop: Header=BB25_120 Depth=2
	leaq	(%rcx,%rbx,8), %r14
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rcx,%rbx,8)
	testq	%rbp, %rbp
	jle	.LBB25_176
# %bb.175:                              # %if.then.i.i.i.i44.i341
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%r12, %rdi
	movq	%rdx, %rbx
	movq	%rdx, %rsi
	movq	%rbp, %rdx
	movq	%rcx, %rbp
	callq	memmove
	movq	%rbp, %rcx
	movq	%rbx, %rdx
.LBB25_176:                             # %_ZNSt6vectorIP12netlist_elemSaIS1_EE11_S_relocateEPS1_S4_S4_RS2_.exit.i351
                                        #   in Loop: Header=BB25_120 Depth=2
	leaq	72(%r13), %rbx
	addq	$8, %r14
	testq	%rdx, %rdx
	je	.LBB25_178
# %bb.177:                              # %if.then.i38.i352
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%rdx, %rdi
	movq	%rcx, %rbp
	callq	_ZdlPv
	movq	%rbp, %rcx
.LBB25_178:                             # %.noexc317
                                        #   in Loop: Header=BB25_120 Depth=2
	movq	%r12, 56(%r13)
	movq	%r14, 64(%r13)
	leaq	(%rcx,%r15,8), %rax
	movq	%rax, (%rbx)
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB25_120
	.p2align	4, 0x90
.LBB25_181:                             # %while.end
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	32(%rsp), %rdi
	leaq	48(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB25_183
# %bb.182:                              # %if.then.i.i359
                                        #   in Loop: Header=BB25_80 Depth=1
	callq	_ZdlPv
.LBB25_183:                             # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
                                        #   in Loop: Header=BB25_80 Depth=1
	movq	64(%rsp), %rdi
	leaq	80(%rsp), %rax
	cmpq	%rax, %rdi
	movq	152(%rsp), %rbx         # 8-byte Reload
	je	.LBB25_185
# %bb.184:                              # %if.then.i.i364
                                        #   in Loop: Header=BB25_80 Depth=1
	callq	_ZdlPv
	jmp	.LBB25_185
.LBB25_186:                             # %while.end105
.Ltmp199:
	movl	$_ZSt4cout, %edi
	movl	$.L.str.4.17, %esi
	movl	$17, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp200:
# %bb.187:                              # %invoke.cont106
	addl	$-1, %ebx
.Ltmp201:
	movl	$_ZSt4cout, %edi
	movl	%ebx, %esi
	callq	_ZNSolsEi
.Ltmp202:
# %bb.188:                              # %invoke.cont108
	movq	%rax, %rbx
.Ltmp203:
	movl	$.L.str.5.18, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp204:
# %bb.189:                              # %invoke.cont110
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB25_190
# %bb.197:                              # %call.i558.noexc
	cmpb	$0, 56(%rbp)
	je	.LBB25_199
# %bb.198:                              # %if.then.i565
	movb	67(%rbp), %al
	jmp	.LBB25_201
.LBB25_199:                             # %if.end.i569
.Ltmp205:
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp206:
# %bb.200:                              # %.noexc571
	movq	(%rbp), %rax
.Ltmp207:
	movq	%rbp, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.Ltmp208:
.LBB25_201:                             # %call.i398.noexc
.Ltmp209:
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZNSo3putEc
.Ltmp210:
# %bb.202:                              # %call1.i.noexc400
.Ltmp211:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp212:
# %bb.203:                              # %invoke.cont112
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB25_205
# %bb.204:                              # %if.then.i.i.i409
	callq	_ZdlPv
.LBB25_205:                             # %_ZNSt6vectorI10location_tSaIS0_EED2Ev.exit
	movq	_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 192(%rsp)
	movq	_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 192(%rsp,%rax)
	leaq	208(%rsp), %rdi
	callq	_ZNSt13basic_filebufIcSt11char_traitsIcEED2Ev
	movq	_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE+8(%rip), %rax
	movq	%rax, 192(%rsp)
	movq	_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE+16(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 192(%rsp,%rax)
	movq	$0, 200(%rsp)
	leaq	448(%rsp), %rdi
	callq	_ZNSt8ios_baseD2Ev
	addq	$712, %rsp              # imm = 0x2C8
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.LBB25_58:                              # %if.then.i.i
	.cfi_def_cfa_offset 768
	movl	%r8d, %esi
.Ltmp138:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	_ZSt24__throw_out_of_range_fmtPKcz
.Ltmp139:
# %bb.59:                               # %.noexc190
.LBB25_61:                              # %if.then.i.i195
	movl	%ebp, %esi
.Ltmp140:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	_ZSt24__throw_out_of_range_fmtPKcz
.Ltmp141:
# %bb.62:                               # %.noexc197
.LBB25_64:                              # %if.then.i.i202
.Ltmp142:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	_ZSt24__throw_out_of_range_fmtPKcz
.Ltmp143:
# %bb.65:                               # %.noexc204
.LBB25_146:                             # %if.then.i.i.i278
.Ltmp185:
	movl	$.L.str.11, %edi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	callq	_ZSt24__throw_out_of_range_fmtPKcz
.Ltmp186:
# %bb.147:                              # %.noexc287
.LBB25_154:                             # %if.then.i.i298
.Ltmp196:
	movl	$.L.str.12.16, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp197:
# %bb.155:                              # %.noexc308
.LBB25_168:                             # %if.then.i.i323
.Ltmp194:
	movl	$.L.str.12.16, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp195:
# %bb.169:                              # %.noexc354
.LBB25_112:                             # %if.then.i.i.i249
.Ltmp171:
	movl	$.L.str.11, %edi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	callq	_ZSt24__throw_out_of_range_fmtPKcz
.Ltmp172:
# %bb.113:                              # %.noexc253
.LBB25_84:                              # %if.then.i580
.Ltmp215:
	callq	_ZSt16__throw_bad_castv
.Ltmp216:
# %bb.85:                               # %.noexc582
.LBB25_19:                              # %if.then.i460
.Ltmp221:
	callq	_ZSt16__throw_bad_castv
.Ltmp222:
# %bb.20:                               # %.noexc462
.LBB25_55:                              # %if.then.i575
.Ltmp218:
	callq	_ZSt16__throw_bad_castv
.Ltmp219:
# %bb.71:                               # %.noexc577
.LBB25_190:                             # %if.then.i585
.Ltmp213:
	callq	_ZSt16__throw_bad_castv
.Ltmp214:
# %bb.196:                              # %.noexc587
.LBB25_133:                             # %lpad72.loopexit.split-lp
.Ltmp173:
	movq	%rax, %r14
	jmp	.LBB25_194
.LBB25_180:                             # %lpad95.loopexit.split-lp
.Ltmp198:
	jmp	.LBB25_192
.LBB25_70:                              # %lpad35
.Ltmp144:
	jmp	.LBB25_50
.LBB25_49:                              # %lpad25
.Ltmp137:
	jmp	.LBB25_50
.LBB25_48:                              # %lpad20
.Ltmp134:
	jmp	.LBB25_47
.LBB25_45:                              # %lpad
.Ltmp113:
	movq	%rax, %r14
	jmp	.LBB25_210
.LBB25_206:                             # %lpad53
.Ltmp220:
	movq	%rax, %r14
	jmp	.LBB25_207
.LBB25_102:                             # %lpad58.loopexit.split-lp
.Ltmp217:
	jmp	.LBB25_50
.LBB25_46:                              # %lpad2
.Ltmp223:
.LBB25_47:                              # %ehcleanup118
	movq	%rax, %r14
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB25_209
.LBB25_131:                             # %lpad69
.Ltmp170:
	movq	%rax, %r14
	jmp	.LBB25_194
.LBB25_135:                             # %lpad77
.Ltmp181:
	movq	%rax, %r14
	jmp	.LBB25_194
.LBB25_132:                             # %lpad72.loopexit
.Ltmp178:
	movq	%rax, %r14
	jmp	.LBB25_194
.LBB25_101:                             # %lpad58.loopexit
.Ltmp167:
.LBB25_50:                              # %ehcleanup116
	movq	%rax, %r14
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB25_207
.LBB25_191:                             # %lpad81
.Ltmp184:
	jmp	.LBB25_192
.LBB25_179:                             # %lpad95.loopexit
.Ltmp193:
.LBB25_192:                             # %ehcleanup
	movq	%rax, %r14
	movq	32(%rsp), %rdi
	leaq	48(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB25_194
# %bb.193:                              # %if.then.i.i370
	callq	_ZdlPv
.LBB25_194:                             # %ehcleanup101
	movq	(%rsp), %r15            # 8-byte Reload
	movq	64(%rsp), %rdi
	leaq	80(%rsp), %rax
	cmpq	%rax, %rdi
	je	.LBB25_207
# %bb.195:                              # %if.then.i.i376
	callq	_ZdlPv
.LBB25_207:                             # %ehcleanup116
	movq	96(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB25_209
# %bb.208:                              # %if.then.i.i.i415
	callq	_ZdlPv
.LBB25_209:                             # %ehcleanup118
	movq	_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 192(%rsp)
	movq	_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 192(%rsp,%rax)
	leaq	208(%rsp), %rdi
	callq	_ZNSt13basic_filebufIcSt11char_traitsIcEED2Ev
	movq	_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE+8(%rip), %rax
	movq	%rax, 192(%rsp)
	movq	_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE+16(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, 192(%rsp,%rax)
	movq	$0, 200(%rsp)
	leaq	448(%rsp), %rdi
	callq	_ZNSt8ios_baseD2Ev
.LBB25_210:                             # %ehcleanup119
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	16(%rdi), %rsi
.Ltmp224:
	callq	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE8_M_eraseEPSt13_Rb_tree_nodeISA_E
.Ltmp225:
# %bb.211:                              # %_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEED2Ev.exit
	movq	40(%r15), %rbx
	movq	48(%r15), %rbp
	cmpq	%rbp, %rbx
	jne	.LBB25_212
	jmp	.LBB25_216
.LBB25_214:                             # %_ZSt8_DestroyISt6vectorI10location_tSaIS1_EEEvPT_.exit.i.i.i.i
                                        #   in Loop: Header=BB25_212 Depth=1
	addq	$24, %rbx
	cmpq	%rbx, %rbp
	je	.LBB25_215
.LBB25_212:                             # %for.body.i.i.i.i430
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_214
# %bb.213:                              # %if.then.i.i.i.i.i.i.i.i431
                                        #   in Loop: Header=BB25_212 Depth=1
	callq	_ZdlPv
	jmp	.LBB25_214
.LBB25_215:                             # %invoke.cont.loopexit.i435
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rbx
.LBB25_216:                             # %invoke.cont.i437
	testq	%rbx, %rbx
	je	.LBB25_218
# %bb.217:                              # %if.then.i.i.i438
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB25_218:                             # %_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EED2Ev.exit
	movq	16(%r15), %rbx
	movq	24(%r15), %rbp
	cmpq	%rbp, %rbx
	jne	.LBB25_219
	jmp	.LBB25_227
	.p2align	4, 0x90
.LBB25_225:                             # %_ZSt8_DestroyI12netlist_elemEvPT_.exit.i.i.i.i
                                        #   in Loop: Header=BB25_219 Depth=1
	addq	$88, %rbx
	cmpq	%rbx, %rbp
	je	.LBB25_226
.LBB25_219:                             # %for.body.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_221
# %bb.220:                              # %if.then.i.i.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB25_219 Depth=1
	callq	_ZdlPv
.LBB25_221:                             # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit.i.i.i.i.i.i
                                        #   in Loop: Header=BB25_219 Depth=1
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_223
# %bb.222:                              # %if.then.i.i.i4.i.i.i.i.i.i
                                        #   in Loop: Header=BB25_219 Depth=1
	callq	_ZdlPv
.LBB25_223:                             # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit5.i.i.i.i.i.i
                                        #   in Loop: Header=BB25_219 Depth=1
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rdi, %rax
	je	.LBB25_225
# %bb.224:                              # %if.then.i.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB25_219 Depth=1
	callq	_ZdlPv
	jmp	.LBB25_225
.LBB25_226:                             # %invoke.cont.loopexit.i
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rbx
.LBB25_227:                             # %invoke.cont.i
	testq	%rbx, %rbx
	je	.LBB25_229
# %bb.228:                              # %if.then.i.i.i
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB25_229:                             # %_ZNSt6vectorI12netlist_elemSaIS0_EED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_230:                             # %lpad.i.i
.Ltmp226:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN7netlistC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end25-_ZN7netlistC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase3-.Lttbaseref3
.Lttbaseref3:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end5-.Lcst_begin5
.Lcst_begin5:
	.uleb128 .Ltmp111-.Lfunc_begin5 # >> Call Site 1 <<
	.uleb128 .Ltmp112-.Ltmp111      #   Call between .Ltmp111 and .Ltmp112
	.uleb128 .Ltmp113-.Lfunc_begin5 #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp114-.Lfunc_begin5 # >> Call Site 2 <<
	.uleb128 .Ltmp131-.Ltmp114      #   Call between .Ltmp114 and .Ltmp131
	.uleb128 .Ltmp223-.Lfunc_begin5 #     jumps to .Ltmp223
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp132-.Lfunc_begin5 # >> Call Site 3 <<
	.uleb128 .Ltmp133-.Ltmp132      #   Call between .Ltmp132 and .Ltmp133
	.uleb128 .Ltmp134-.Lfunc_begin5 #     jumps to .Ltmp134
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp133-.Lfunc_begin5 # >> Call Site 4 <<
	.uleb128 .Ltmp135-.Ltmp133      #   Call between .Ltmp133 and .Ltmp135
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp135-.Lfunc_begin5 # >> Call Site 5 <<
	.uleb128 .Ltmp136-.Ltmp135      #   Call between .Ltmp135 and .Ltmp136
	.uleb128 .Ltmp137-.Lfunc_begin5 #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp145-.Lfunc_begin5 # >> Call Site 6 <<
	.uleb128 .Ltmp154-.Ltmp145      #   Call between .Ltmp145 and .Ltmp154
	.uleb128 .Ltmp220-.Lfunc_begin5 #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp155-.Lfunc_begin5 # >> Call Site 7 <<
	.uleb128 .Ltmp166-.Ltmp155      #   Call between .Ltmp155 and .Ltmp166
	.uleb128 .Ltmp167-.Lfunc_begin5 #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp168-.Lfunc_begin5 # >> Call Site 8 <<
	.uleb128 .Ltmp169-.Ltmp168      #   Call between .Ltmp168 and .Ltmp169
	.uleb128 .Ltmp170-.Lfunc_begin5 #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp174-.Lfunc_begin5 # >> Call Site 9 <<
	.uleb128 .Ltmp177-.Ltmp174      #   Call between .Ltmp174 and .Ltmp177
	.uleb128 .Ltmp178-.Lfunc_begin5 #     jumps to .Ltmp178
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp179-.Lfunc_begin5 # >> Call Site 10 <<
	.uleb128 .Ltmp180-.Ltmp179      #   Call between .Ltmp179 and .Ltmp180
	.uleb128 .Ltmp181-.Lfunc_begin5 #     jumps to .Ltmp181
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp182-.Lfunc_begin5 # >> Call Site 11 <<
	.uleb128 .Ltmp183-.Ltmp182      #   Call between .Ltmp182 and .Ltmp183
	.uleb128 .Ltmp184-.Lfunc_begin5 #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp187-.Lfunc_begin5 # >> Call Site 12 <<
	.uleb128 .Ltmp190-.Ltmp187      #   Call between .Ltmp187 and .Ltmp190
	.uleb128 .Ltmp193-.Lfunc_begin5 #     jumps to .Ltmp193
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp190-.Lfunc_begin5 # >> Call Site 13 <<
	.uleb128 .Ltmp191-.Ltmp190      #   Call between .Ltmp190 and .Ltmp191
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp191-.Lfunc_begin5 # >> Call Site 14 <<
	.uleb128 .Ltmp192-.Ltmp191      #   Call between .Ltmp191 and .Ltmp192
	.uleb128 .Ltmp193-.Lfunc_begin5 #     jumps to .Ltmp193
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp192-.Lfunc_begin5 # >> Call Site 15 <<
	.uleb128 .Ltmp199-.Ltmp192      #   Call between .Ltmp192 and .Ltmp199
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp199-.Lfunc_begin5 # >> Call Site 16 <<
	.uleb128 .Ltmp212-.Ltmp199      #   Call between .Ltmp199 and .Ltmp212
	.uleb128 .Ltmp217-.Lfunc_begin5 #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp138-.Lfunc_begin5 # >> Call Site 17 <<
	.uleb128 .Ltmp143-.Ltmp138      #   Call between .Ltmp138 and .Ltmp143
	.uleb128 .Ltmp144-.Lfunc_begin5 #     jumps to .Ltmp144
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp185-.Lfunc_begin5 # >> Call Site 18 <<
	.uleb128 .Ltmp195-.Ltmp185      #   Call between .Ltmp185 and .Ltmp195
	.uleb128 .Ltmp198-.Lfunc_begin5 #     jumps to .Ltmp198
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp171-.Lfunc_begin5 # >> Call Site 19 <<
	.uleb128 .Ltmp172-.Ltmp171      #   Call between .Ltmp171 and .Ltmp172
	.uleb128 .Ltmp173-.Lfunc_begin5 #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp215-.Lfunc_begin5 # >> Call Site 20 <<
	.uleb128 .Ltmp216-.Ltmp215      #   Call between .Ltmp215 and .Ltmp216
	.uleb128 .Ltmp217-.Lfunc_begin5 #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp221-.Lfunc_begin5 # >> Call Site 21 <<
	.uleb128 .Ltmp222-.Ltmp221      #   Call between .Ltmp221 and .Ltmp222
	.uleb128 .Ltmp223-.Lfunc_begin5 #     jumps to .Ltmp223
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp218-.Lfunc_begin5 # >> Call Site 22 <<
	.uleb128 .Ltmp219-.Ltmp218      #   Call between .Ltmp218 and .Ltmp219
	.uleb128 .Ltmp220-.Lfunc_begin5 #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp213-.Lfunc_begin5 # >> Call Site 23 <<
	.uleb128 .Ltmp214-.Ltmp213      #   Call between .Ltmp213 and .Ltmp214
	.uleb128 .Ltmp217-.Lfunc_begin5 #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp224-.Lfunc_begin5 # >> Call Site 24 <<
	.uleb128 .Ltmp225-.Ltmp224      #   Call between .Ltmp224 and .Ltmp225
	.uleb128 .Ltmp226-.Lfunc_begin5 #     jumps to .Ltmp226
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp225-.Lfunc_begin5 # >> Call Site 25 <<
	.uleb128 .Lfunc_end25-.Ltmp225  #   Call between .Ltmp225 and .Lfunc_end25
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end5:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase3:
	.p2align	2
                                        # -- End function
	.section	.text._ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm,comdat
	.weak	_ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm # -- Begin function _ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm
	.p2align	4, 0x90
	.type	_ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm,@function
_ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm: # @_ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	testq	%rsi, %rsi
	je	.LBB26_3
# %bb.1:                                # %if.then
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	8(%rdi), %rbx
	movq	16(%rdi), %rax
	movabsq	$3353953467947191203, %rcx # imm = 0x2E8BA2E8BA2E8BA3
	subq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	jae	.LBB26_2
# %bb.4:                                # %if.else
	movabsq	$104811045873349725, %rax # imm = 0x1745D1745D1745D
	subq	(%r13), %rbx
	sarq	$3, %rbx
	imulq	%rcx, %rbx
	movq	%rax, %rcx
	subq	%rbx, %rcx
	cmpq	%r15, %rcx
	jb	.LBB26_38
# %bb.5:                                # %_ZNKSt6vectorI12netlist_elemSaIS0_EE12_M_check_lenEmPKc.exit
	cmpq	%r15, %rbx
	movq	%rbx, %rcx
	cmovbq	%r15, %rcx
	leaq	(%rcx,%rbx), %rdx
	cmpq	%rax, %rdx
	cmovaq	%rax, %rdx
	addq	%rbx, %rcx
	cmovbq	%rax, %rdx
	testq	%rdx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	je	.LBB26_6
# %bb.7:                                # %_ZNSt16allocator_traitsISaI12netlist_elemEE8allocateERS1_m.exit.i
	imulq	$88, %rdx, %rdi
	callq	_Znwm
	movq	%rax, %rbp
	jmp	.LBB26_8
.LBB26_2:                               # %if.then9
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_
	movq	%rax, 8(%r13)
	jmp	.LBB26_3
.LBB26_6:
	xorl	%ebp, %ebp
.LBB26_8:                               # %_ZNSt12_Vector_baseI12netlist_elemSaIS0_EE11_M_allocateEm.exit
	imulq	$88, %rbx, %r12
	addq	%rbp, %r12
	xorl	%ebx, %ebx
.Ltmp228:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_
.Ltmp229:
# %bb.9:                                # %invoke.cont
	movq	%r12, %rbx
	movq	(%r13), %rdi
	movq	8(%r13), %rsi
.Ltmp230:
	movq	%rbp, %rdx
	callq	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_
.Ltmp231:
# %bb.10:                               # %try.cont
	movq	(%r13), %rbx
	movq	8(%r13), %r14
	cmpq	%r14, %rbx
	jne	.LBB26_11
# %bb.19:                               # %_ZSt8_DestroyIP12netlist_elemS0_EvT_S2_RSaIT0_E.exit
	testq	%rbx, %rbx
	je	.LBB26_21
.LBB26_20:                              # %if.then.i
	movq	%rbx, %rdi
	callq	_ZdlPv
.LBB26_21:                              # %_ZNSt12_Vector_baseI12netlist_elemSaIS0_EE13_M_deallocateEPS0_m.exit
	movq	%rbp, (%r13)
	imulq	$88, %r15, %rax
	addq	%rax, %r12
	movq	%r12, 8(%r13)
	imulq	$88, (%rsp), %rax       # 8-byte Folded Reload
	addq	%rax, %rbp
	movq	%rbp, 16(%r13)
.LBB26_3:                               # %if.end60
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
	.p2align	4, 0x90
.LBB26_17:                              # %_ZSt8_DestroyI12netlist_elemEvPT_.exit.i.i.i
                                        #   in Loop: Header=BB26_11 Depth=1
	.cfi_def_cfa_offset 64
	addq	$88, %rbx
	cmpq	%rbx, %r14
	je	.LBB26_18
.LBB26_11:                              # %for.body.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_13
# %bb.12:                               # %if.then.i.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB26_11 Depth=1
	callq	_ZdlPv
.LBB26_13:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit.i.i.i.i.i
                                        #   in Loop: Header=BB26_11 Depth=1
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_15
# %bb.14:                               # %if.then.i.i.i4.i.i.i.i.i
                                        #   in Loop: Header=BB26_11 Depth=1
	callq	_ZdlPv
.LBB26_15:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit5.i.i.i.i.i
                                        #   in Loop: Header=BB26_11 Depth=1
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rdi, %rax
	je	.LBB26_17
# %bb.16:                               # %if.then.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB26_11 Depth=1
	callq	_ZdlPv
	jmp	.LBB26_17
.LBB26_18:                              # %_ZSt8_DestroyIP12netlist_elemS0_EvT_S2_RSaIT0_E.exitthread-pre-split
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	jne	.LBB26_20
	jmp	.LBB26_21
.LBB26_38:                              # %if.then.i94
	movl	$.L.str.8.20, %edi
	callq	_ZSt20__throw_length_errorPKc
.LBB26_22:                              # %lpad
.Ltmp232:
	movq	%rbp, %r12
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%rbx, %rbx
	je	.LBB26_31
# %bb.23:                               # %for.body.i.i.i108.preheader
	imulq	$88, %r15, %r14
	leaq	16(%rbx), %r15
	xorl	%ebp, %ebp
	jmp	.LBB26_24
	.p2align	4, 0x90
.LBB26_30:                              # %_ZSt8_DestroyI12netlist_elemEvPT_.exit.i.i.i121
                                        #   in Loop: Header=BB26_24 Depth=1
	addq	$88, %rbp
	cmpq	%rbp, %r14
	je	.LBB26_31
.LBB26_24:                              # %for.body.i.i.i108
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%rbx,%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB26_26
# %bb.25:                               # %if.then.i.i.i.i.i.i.i.i109
                                        #   in Loop: Header=BB26_24 Depth=1
	callq	_ZdlPv
.LBB26_26:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit.i.i.i.i.i112
                                        #   in Loop: Header=BB26_24 Depth=1
	movq	32(%rbx,%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB26_28
# %bb.27:                               # %if.then.i.i.i4.i.i.i.i.i113
                                        #   in Loop: Header=BB26_24 Depth=1
	callq	_ZdlPv
.LBB26_28:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit5.i.i.i.i.i117
                                        #   in Loop: Header=BB26_24 Depth=1
	movq	(%rbx,%rbp), %rdi
	leaq	(%r15,%rbp), %rax
	cmpq	%rdi, %rax
	je	.LBB26_30
# %bb.29:                               # %if.then.i.i.i.i.i.i.i118
                                        #   in Loop: Header=BB26_24 Depth=1
	callq	_ZdlPv
	jmp	.LBB26_30
.LBB26_31:                              # %if.end32
	testq	%r12, %r12
	je	.LBB26_33
# %bb.32:                               # %if.then.i102
	movq	%r12, %rdi
	callq	_ZdlPv
.LBB26_33:                              # %invoke.cont33
.Ltmp233:
	callq	__cxa_rethrow
.Ltmp234:
# %bb.37:                               # %unreachable
.LBB26_34:                              # %lpad30
.Ltmp235:
	movq	%rax, %rbx
.Ltmp236:
	callq	__cxa_end_catch
.Ltmp237:
# %bb.35:                               # %invoke.cont34
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB26_36:                              # %terminate.lpad
.Ltmp238:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm, .Lfunc_end26-_ZNSt6vectorI12netlist_elemSaIS0_EE17_M_default_appendEm
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase4-.Lttbaseref4
.Lttbaseref4:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end6-.Lcst_begin6
.Lcst_begin6:
	.uleb128 .Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.uleb128 .Ltmp228-.Lfunc_begin6 #   Call between .Lfunc_begin6 and .Ltmp228
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp228-.Lfunc_begin6 # >> Call Site 2 <<
	.uleb128 .Ltmp231-.Ltmp228      #   Call between .Ltmp228 and .Ltmp231
	.uleb128 .Ltmp232-.Lfunc_begin6 #     jumps to .Ltmp232
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp231-.Lfunc_begin6 # >> Call Site 3 <<
	.uleb128 .Ltmp233-.Ltmp231      #   Call between .Ltmp231 and .Ltmp233
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp233-.Lfunc_begin6 # >> Call Site 4 <<
	.uleb128 .Ltmp234-.Ltmp233      #   Call between .Ltmp233 and .Ltmp234
	.uleb128 .Ltmp235-.Lfunc_begin6 #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp236-.Lfunc_begin6 # >> Call Site 5 <<
	.uleb128 .Ltmp237-.Ltmp236      #   Call between .Ltmp236 and .Ltmp237
	.uleb128 .Ltmp238-.Lfunc_begin6 #     jumps to .Ltmp238
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp237-.Lfunc_begin6 # >> Call Site 6 <<
	.uleb128 .Lfunc_end26-.Ltmp237  #   Call between .Ltmp237 and .Lfunc_end26
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end6:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase4:
	.p2align	2
                                        # -- End function
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4               # -- Begin function _ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_
.LCPI27_0:
	.zero	16
	.section	.text._ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_,"axG",@progbits,_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_,comdat
	.weak	_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_
	.p2align	4, 0x90
	.type	_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_,@function
_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_: # @_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
	.cfi_def_cfa_offset 112
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	testq	%rdx, %rdx
	je	.LBB27_54
# %bb.1:                                # %if.then
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	16(%rdi), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	movabsq	$-6148914691236517205, %r9 # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%r9, %rcx
	cmpq	%rdx, %rcx
	jae	.LBB27_2
# %bb.33:                               # %if.else54
	movabsq	$384307168202282325, %rdx # imm = 0x555555555555555
	movq	(%rbx), %rcx
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%r9, %rax
	movq	%rdx, %rsi
	subq	%rax, %rsi
	cmpq	%r15, %rsi
	jb	.LBB27_68
# %bb.34:                               # %_ZNKSt6vectorIS_I10location_tSaIS0_EESaIS2_EE12_M_check_lenEmPKc.exit
	cmpq	%r15, %rax
	movq	%rax, %rsi
	cmovbq	%r15, %rsi
	leaq	(%rsi,%rax), %rdi
	cmpq	%rdx, %rdi
	cmovaq	%rdx, %rdi
	addq	%rax, %rsi
	cmovbq	%rdx, %rdi
	movq	%r12, %rbp
	subq	%rcx, %rbp
	sarq	$3, %rbp
	imulq	%r9, %rbp
	testq	%rdi, %rdi
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	je	.LBB27_35
# %bb.36:                               # %_ZNSt16allocator_traitsISaISt6vectorI10location_tSaIS1_EEEE8allocateERS4_m.exit.i
	leaq	(,%rdi,8), %rax
	leaq	(%rax,%rax,2), %rdi
	callq	_Znwm
	jmp	.LBB27_37
.LBB27_2:                               # %if.then4
	movq	%rbx, 8(%rsp)
	movq	(%r13), %rsi
	movq	8(%r13), %rdx
	movq	%rdx, %rdi
	subq	%rsi, %rdi
	movq	%rdi, %r14
	sarq	$3, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rsp)
	movq	$0, 32(%rsp)
	testq	%rdi, %rdi
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	je	.LBB27_3
# %bb.4:                                # %cond.true.i.i.i.i.i.i.i
	movq	%r14, %rax
	shrq	$60, %rax
	jne	.LBB27_66
# %bb.5:                                # %_ZNSt16allocator_traitsISaI10location_tEE8allocateERS1_m.exit.i.i.i.i.i.i.i
	callq	_Znwm
	movq	%rax, %rbp
	movq	(%r13), %rsi
	movq	8(%r13), %rdx
	movabsq	$-6148914691236517205, %r9 # imm = 0xAAAAAAAAAAAAAAAB
	jmp	.LBB27_6
.LBB27_3:
	xorl	%ebp, %ebp
.LBB27_6:                               # %invoke.cont.i.i.i.i
	movq	%rbp, 16(%rsp)
	movq	%rbp, 24(%rsp)
	leaq	(,%r14,8), %rax
	addq	%rbp, %rax
	movq	%rax, 32(%rsp)
	subq	%rsi, %rdx
	movq	%rdx, %rbx
	sarq	$3, %rbx
	testq	%rdx, %rdx
	je	.LBB27_8
# %bb.7:                                # %if.then.i.i.i.i.i.i.i.i.i.i.i
	movq	%rbp, %rdi
	movq	%r9, %r14
	callq	memmove
	movq	%r14, %r9
.LBB27_8:                               # %_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE16_Temporary_valueC2IJRKS2_EEEPS4_DpOT_.exit
	leaq	16(%rsp), %rcx
	leaq	(,%rbx,8), %rax
	addq	%rbp, %rax
	movq	%rax, 24(%rsp)
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	8(%r14), %r13
	movq	%r13, %rbp
	subq	%r12, %rbp
	sarq	$3, %rbp
	imulq	%r9, %rbp
	cmpq	%r15, %rbp
	jbe	.LBB27_24
# %bb.9:                                # %for.body.i.i.i.i.preheader
	movq	%r14, %r8
	movq	%r15, %rax
	negq	%rax
	leaq	(%rax,%rax,2), %rax
	leaq	(,%rax,8), %rax
	addq	%r13, %rax
	leaq	(,%r15,8), %rcx
	leaq	(%rcx,%rcx,2), %r14
	movq	%r14, %rbx
	negq	%rbx
	movq	%r13, %rsi
	subq	%r14, %rsi
	xorl	%edi, %edi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB27_10:                              # %for.body.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi,%rdi), %xmm1
	movups	%xmm1, (%r13,%rdi)
	movq	16(%rsi,%rdi), %rbp
	movq	%rbp, 16(%r13,%rdi)
	movups	%xmm0, (%rsi,%rdi)
	movq	$0, 16(%rsi,%rdi)
	addq	$24, %rdi
	movq	%rbx, %rdx
	addq	%rdi, %rdx
	jne	.LBB27_10
# %bb.11:                               # %invoke.cont20
	leaq	(%r15,%r15,2), %rdx
	shlq	$3, %rdx
	addq	%rdx, 8(%r8)
	subq	%r12, %rax
	testq	%rax, %rax
	jle	.LBB27_16
# %bb.12:                               # %for.body.preheader.i.i.i.i
	mulq	%r9
	movq	%rdx, %rbp
	shrq	$4, %rbp
	addq	$-24, %r13
	leaq	(%rcx,%rcx,2), %r15
	negq	%r15
	jmp	.LBB27_13
	.p2align	4, 0x90
.LBB27_15:                              # %_ZNSt6vectorI10location_tSaIS0_EEaSEOS2_.exit.i.i.i.i
                                        #   in Loop: Header=BB27_13 Depth=1
	addq	$-24, %r13
	addq	$-1, %rbp
	jle	.LBB27_16
.LBB27_13:                              # %for.body.i.i.i.i185
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rdi
	movups	(%r13,%r15), %xmm0
	movups	%xmm0, (%r13)
	movq	16(%r13,%r15), %rax
	movq	%rax, 16(%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13,%r15)
	movq	$0, 16(%r13,%r15)
	testq	%rdi, %rdi
	je	.LBB27_15
# %bb.14:                               # %if.then.i.i.i.i.i.i.i.i.i186
                                        #   in Loop: Header=BB27_13 Depth=1
	callq	_ZdlPv
	jmp	.LBB27_15
.LBB27_24:                              # %if.else
	subq	%rbp, %r15
.Ltmp239:
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rcx, %rbx
	movq	%rcx, %rdx
	callq	_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_
.Ltmp240:
# %bb.25:                               # %invoke.cont37
	movq	%rax, 8(%r14)
	cmpq	%r12, %r13
	je	.LBB27_67
# %bb.26:                               # %for.body.i.i.i.i250.preheader
	xorl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB27_27:                              # %for.body.i.i.i.i250
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	movq	16(%r12,%rcx), %rdx
	movq	%rdx, 16(%rax,%rcx)
	movups	%xmm0, (%r12,%rcx)
	movq	$0, 16(%r12,%rcx)
	leaq	(%r12,%rcx), %rdx
	addq	$24, %rdx
	addq	$24, %rcx
	cmpq	%r13, %rdx
	jne	.LBB27_27
# %bb.28:                               # %for.body.i.i263.preheader
	leaq	(,%rbp,2), %rax
	addq	%rbp, %rax
	shlq	$3, %rax
	addq	%rax, 8(%r14)
	.p2align	4, 0x90
.LBB27_29:                              # %for.body.i.i263
                                        # =>This Inner Loop Header: Depth=1
.Ltmp242:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_
.Ltmp243:
# %bb.30:                               # %call.i.i.noexc264
                                        #   in Loop: Header=BB27_29 Depth=1
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.LBB27_29
	jmp	.LBB27_31
.LBB27_16:                              # %for.body.i.i.preheader
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB27_17:                              # %for.body.i.i
                                        # =>This Inner Loop Header: Depth=1
.Ltmp245:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_
.Ltmp246:
# %bb.18:                               # %call.i.i.noexc
                                        #   in Loop: Header=BB27_17 Depth=1
	addq	$24, %r12
	addq	$-24, %r14
	jne	.LBB27_17
	jmp	.LBB27_31
.LBB27_35:
	xorl	%eax, %eax
.LBB27_37:                              # %_ZNSt12_Vector_baseISt6vectorI10location_tSaIS1_EESaIS3_EE11_M_allocateEm.exit
	leaq	(,%rbp,2), %rcx
	addq	%rbp, %rcx
	movq	%rax, %rbp
	leaq	(%rax,%rcx,8), %r14
.Ltmp248:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_
.Ltmp249:
# %bb.38:                               # %invoke.cont64
	movq	(%rbx), %rcx
	movq	%rbp, %rax
	cmpq	%r12, %rcx
	movq	%rbp, %rsi
	je	.LBB27_41
# %bb.39:                               # %for.body.i.i.i.i222.preheader
	xorps	%xmm0, %xmm0
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB27_40:                              # %for.body.i.i.i.i222
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx), %xmm1
	movups	%xmm1, (%rax)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movups	%xmm0, (%rcx)
	movq	$0, 16(%rcx)
	addq	$24, %rax
	addq	$24, %rcx
	cmpq	%r12, %rcx
	jne	.LBB27_40
.LBB27_41:                              # %invoke.cont69
	leaq	(%r15,%r15,2), %rcx
	leaq	(%rax,%rcx,8), %rbp
	movq	8(%rbx), %rax
	cmpq	%r12, %rax
	je	.LBB27_42
# %bb.43:                               # %for.body.i.i.i.i209.preheader
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB27_44:                              # %for.body.i.i.i.i209
                                        # =>This Inner Loop Header: Depth=1
	movups	(%r12), %xmm1
	movups	%xmm1, (%rbp)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rbp)
	movups	%xmm0, (%r12)
	movq	$0, 16(%r12)
	addq	$24, %rbp
	addq	$24, %r12
	cmpq	%rax, %r12
	jne	.LBB27_44
# %bb.45:                               # %invoke.cont76.loopexit
	movq	%rsi, %r15
	movq	8(%rbx), %r12
	jmp	.LBB27_46
.LBB27_42:
	movq	%rsi, %r15
.LBB27_46:                              # %invoke.cont76
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB27_47
# %bb.51:                               # %_ZSt8_DestroyIPSt6vectorI10location_tSaIS1_EES3_EvT_S5_RSaIT0_E.exit197
	testq	%r12, %r12
	je	.LBB27_53
.LBB27_52:                              # %if.then.i182
	movq	%r12, %rdi
	callq	_ZdlPv
.LBB27_53:                              # %_ZNSt12_Vector_baseISt6vectorI10location_tSaIS1_EESaIS3_EE13_M_deallocateEPS3_m.exit183
	movq	%r15, (%r14)
	movq	%rbp, 8(%r14)
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %rax
	movq	%rax, 16(%r14)
	jmp	.LBB27_54
	.p2align	4, 0x90
.LBB27_49:                              # %_ZSt8_DestroyISt6vectorI10location_tSaIS1_EEEvPT_.exit.i.i.i196
                                        #   in Loop: Header=BB27_47 Depth=1
	addq	$24, %rbx
	cmpq	%rbx, %r12
	je	.LBB27_50
.LBB27_47:                              # %for.body.i.i.i192
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB27_49
# %bb.48:                               # %if.then.i.i.i.i.i.i.i193
                                        #   in Loop: Header=BB27_47 Depth=1
	callq	_ZdlPv
	jmp	.LBB27_49
.LBB27_50:                              # %_ZSt8_DestroyIPSt6vectorI10location_tSaIS1_EES3_EvT_S5_RSaIT0_E.exit197thread-pre-split
	movq	(%r14), %r12
	testq	%r12, %r12
	jne	.LBB27_52
	jmp	.LBB27_53
.LBB27_67:                              # %invoke.cont45.thread
	leaq	(,%rbp,2), %rcx
	addq	%rbp, %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 8(%r14)
.LBB27_31:                              # %if.end
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_54
# %bb.32:                               # %if.then.i.i.i.i.i.i256
	callq	_ZdlPv
.LBB27_54:                              # %if.end119
	addq	$56, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.LBB27_68:                              # %if.then.i237
	.cfi_def_cfa_offset 112
	movl	$.L.str.10.19, %edi
	callq	_ZSt20__throw_length_errorPKc
.LBB27_66:                              # %if.then.i.i.i.i.i.i.i.i.i
	callq	_ZSt17__throw_bad_allocv
.LBB27_21:                              # %lpad19.loopexit.split-lp.loopexit.split-lp
.Ltmp241:
	jmp	.LBB27_22
.LBB27_55:                              # %lpad63
.Ltmp250:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%rbp, %rbp
	jne	.LBB27_60
# %bb.56:                               # %for.body.i.i.i175.preheader
	shlq	$3, %r15
	leaq	(%r15,%r15,2), %rbx
	xorl	%ebp, %ebp
	jmp	.LBB27_57
	.p2align	4, 0x90
.LBB27_59:                              # %_ZSt8_DestroyISt6vectorI10location_tSaIS1_EEEvPT_.exit.i.i.i179
                                        #   in Loop: Header=BB27_57 Depth=1
	addq	$24, %rbp
	cmpq	%rbp, %rbx
	je	.LBB27_61
.LBB27_57:                              # %for.body.i.i.i175
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_59
# %bb.58:                               # %if.then.i.i.i.i.i.i.i176
                                        #   in Loop: Header=BB27_57 Depth=1
	callq	_ZdlPv
	jmp	.LBB27_59
.LBB27_60:                              # %if.then.i
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB27_61:                              # %invoke.cont89
.Ltmp251:
	callq	__cxa_rethrow
.Ltmp252:
# %bb.65:                               # %unreachable
.LBB27_62:                              # %lpad83
.Ltmp253:
	movq	%rax, %rbx
.Ltmp254:
	callq	__cxa_end_catch
.Ltmp255:
	jmp	.LBB27_63
.LBB27_64:                              # %terminate.lpad
.Ltmp256:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB27_20:                              # %lpad19.loopexit.split-lp.loopexit
.Ltmp244:
	jmp	.LBB27_22
.LBB27_19:                              # %lpad19.loopexit
.Ltmp247:
.LBB27_22:                              # %lpad19
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB27_23
.LBB27_63:                              # %eh.resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB27_23:                              # %if.then.i.i.i.i.i.i
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end27:
	.size	_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_, .Lfunc_end27-_ZNSt6vectorIS_I10location_tSaIS0_EESaIS2_EE14_M_fill_insertEN9__gnu_cxx17__normal_iteratorIPS2_S4_EEmRKS2_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase5-.Lttbaseref5
.Lttbaseref5:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end7-.Lcst_begin7
.Lcst_begin7:
	.uleb128 .Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.uleb128 .Ltmp239-.Lfunc_begin7 #   Call between .Lfunc_begin7 and .Ltmp239
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp239-.Lfunc_begin7 # >> Call Site 2 <<
	.uleb128 .Ltmp240-.Ltmp239      #   Call between .Ltmp239 and .Ltmp240
	.uleb128 .Ltmp241-.Lfunc_begin7 #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp242-.Lfunc_begin7 # >> Call Site 3 <<
	.uleb128 .Ltmp243-.Ltmp242      #   Call between .Ltmp242 and .Ltmp243
	.uleb128 .Ltmp244-.Lfunc_begin7 #     jumps to .Ltmp244
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp245-.Lfunc_begin7 # >> Call Site 4 <<
	.uleb128 .Ltmp246-.Ltmp245      #   Call between .Ltmp245 and .Ltmp246
	.uleb128 .Ltmp247-.Lfunc_begin7 #     jumps to .Ltmp247
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp248-.Lfunc_begin7 # >> Call Site 5 <<
	.uleb128 .Ltmp249-.Ltmp248      #   Call between .Ltmp248 and .Ltmp249
	.uleb128 .Ltmp250-.Lfunc_begin7 #     jumps to .Ltmp250
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp249-.Lfunc_begin7 # >> Call Site 6 <<
	.uleb128 .Ltmp251-.Ltmp249      #   Call between .Ltmp249 and .Ltmp251
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp251-.Lfunc_begin7 # >> Call Site 7 <<
	.uleb128 .Ltmp252-.Ltmp251      #   Call between .Ltmp251 and .Ltmp252
	.uleb128 .Ltmp253-.Lfunc_begin7 #     jumps to .Ltmp253
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp254-.Lfunc_begin7 # >> Call Site 8 <<
	.uleb128 .Ltmp255-.Ltmp254      #   Call between .Ltmp254 and .Ltmp255
	.uleb128 .Ltmp256-.Lfunc_begin7 #     jumps to .Ltmp256
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp255-.Lfunc_begin7 # >> Call Site 9 <<
	.uleb128 .Lfunc_end27-.Ltmp255  #   Call between .Ltmp255 and .Lfunc_end27
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end7:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase5:
	.p2align	2
                                        # -- End function
	.section	.text._ZNSt6vectorI10location_tSaIS0_EEaSERKS2_,"axG",@progbits,_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_,comdat
	.weak	_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_ # -- Begin function _ZNSt6vectorI10location_tSaIS0_EEaSERKS2_
	.p2align	4, 0x90
	.type	_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_,@function
_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_: # @_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r13
	.cfi_def_cfa_offset 32
	pushq	%r12
	.cfi_def_cfa_offset 40
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset %rbx, -48
	.cfi_offset %r12, -40
	.cfi_offset %r13, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpq	%rdi, %rsi
	je	.LBB28_18
# %bb.1:                                # %if.then
	movq	%rsi, %rbx
	movq	(%rsi), %r14
	movq	8(%rsi), %rdx
	movq	%rdx, %r12
	subq	%r14, %r12
	movq	%r12, %r13
	sarq	$3, %r13
	movq	(%r15), %rax
	movq	16(%r15), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	cmpq	%rcx, %r13
	jbe	.LBB28_9
# %bb.2:                                # %if.then4
	testq	%r12, %r12
	je	.LBB28_3
# %bb.4:                                # %cond.true.i.i
	movq	%r13, %rax
	shrq	$60, %rax
	jne	.LBB28_19
# %bb.5:                                # %if.then.i.i.i.i.i.i.i.i
	movq	%r12, %rdi
	callq	_Znwm
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcpy
	jmp	.LBB28_6
.LBB28_9:                               # %if.else
	movq	8(%r15), %rdi
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rsi
	sarq	$3, %rsi
	cmpq	%r13, %rsi
	jae	.LBB28_10
# %bb.12:                               # %if.else49
	testq	%rcx, %rcx
	je	.LBB28_13
# %bb.14:                               # %if.then.i.i.i.i
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rcx, %rdx
	callq	memmove
	movq	(%rbx), %r14
	movq	8(%rbx), %rdx
	movq	8(%r15), %rdi
	movq	%rdi, %rax
	subq	(%r15), %rax
	sarq	$3, %rax
	jmp	.LBB28_15
.LBB28_10:                              # %if.then27
	testq	%r12, %r12
	je	.LBB28_17
# %bb.11:                               # %if.then.i.i.i.i102
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	jmp	.LBB28_16
.LBB28_3:
	xorl	%ebx, %ebx
.LBB28_6:                               # %_ZNSt6vectorI10location_tSaIS0_EE20_M_allocate_and_copyIN9__gnu_cxx17__normal_iteratorIPKS0_S2_EEEEPS0_mT_SA_.exit
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB28_8
# %bb.7:                                # %if.then.i
	callq	_ZdlPv
.LBB28_8:                               # %_ZNSt12_Vector_baseI10location_tSaIS0_EE13_M_deallocateEPS0_m.exit
	movq	%rbx, (%r15)
	leaq	(%rbx,%r13,8), %rax
	movq	%rax, 16(%r15)
	jmp	.LBB28_17
.LBB28_13:
	xorl	%eax, %eax
.LBB28_15:                              # %_ZSt4copyIP10location_tS1_ET0_T_S3_S2_.exit
	leaq	(%r14,%rax,8), %rsi
	subq	%rsi, %rdx
	je	.LBB28_17
.LBB28_16:                              # %if.then.i.i.i.i.i.i.i
	callq	memmove
.LBB28_17:                              # %if.end69
	shlq	$3, %r13
	addq	(%r15), %r13
	movq	%r13, 8(%r15)
.LBB28_18:                              # %if.end75
	movq	%r15, %rax
	popq	%rbx
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.LBB28_19:                              # %if.then.i.i.i.i110
	.cfi_def_cfa_offset 48
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end28:
	.size	_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_, .Lfunc_end28-_ZNSt6vectorI10location_tSaIS0_EEaSERKS2_
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4               # -- Begin function _ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_
.LCPI29_0:
	.zero	16
	.section	.text._ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_,"axG",@progbits,_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_,comdat
	.weak	_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_
	.p2align	4, 0x90
	.type	_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_,@function
_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_: # @_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.LBB29_13
# %bb.1:                                # %for.body.lr.ph
	movq	%rdx, %r15
	movq	%rsi, %rbp
	movq	(%rdx), %r12
	xorl	%r13d, %r13d
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	jmp	.LBB29_2
	.p2align	4, 0x90
.LBB29_11:                              # %for.inc
                                        #   in Loop: Header=BB29_2 Depth=1
	leaq	(,%r15,8), %rax
	addq	%rbp, %rax
	movq	%rax, 8(%r14)
	addq	$24, %r13
	movq	16(%rsp), %rbp          # 8-byte Reload
	addq	$-1, %rbp
	movq	8(%rsp), %r15           # 8-byte Reload
	je	.LBB29_12
.LBB29_2:                               # %for.body
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rdi
	subq	%r12, %rdi
	movq	%rdi, %r12
	sarq	$3, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx,%r13)
	movq	$0, 16(%rbx,%r13)
	testq	%rdi, %rdi
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	je	.LBB29_3
# %bb.4:                                # %cond.true.i.i.i.i.i
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%r12, %rax
	shrq	$60, %rax
	jne	.LBB29_5
# %bb.7:                                # %_ZNSt16allocator_traitsISaI10location_tEE8allocateERS1_m.exit.i.i.i.i.i
                                        #   in Loop: Header=BB29_2 Depth=1
.Ltmp257:
	callq	_Znwm
.Ltmp258:
# %bb.8:                                # %call2.i.i.i.i3.i19.i.i.noexc
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rbp
	jmp	.LBB29_9
	.p2align	4, 0x90
.LBB29_3:                               #   in Loop: Header=BB29_2 Depth=1
	xorl	%eax, %eax
	xorl	%ebp, %ebp
.LBB29_9:                               # %invoke.cont.i.i
                                        #   in Loop: Header=BB29_2 Depth=1
	leaq	(%rbx,%r13), %r14
	movq	%rbp, (%r14)
	movq	%rax, 8(%r14)
	leaq	(,%r12,8), %rcx
	addq	%rbp, %rcx
	movq	%rcx, 16(%r14)
	movq	(%r15), %r12
	movq	8(%r15), %rdx
	subq	%r12, %rdx
	movq	%rdx, %r15
	sarq	$3, %r15
	testq	%rdx, %rdx
	je	.LBB29_11
# %bb.10:                               # %if.then.i.i.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	memmove
	jmp	.LBB29_11
.LBB29_12:                              # %for.end.loopexit
	addq	%r13, %rbx
.LBB29_13:                              # %for.end
	movq	%rbx, %rax
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.LBB29_5:                               # %if.then.i.i.i.i.i.i.i
	.cfi_def_cfa_offset 80
.Ltmp260:
	callq	_ZSt17__throw_bad_allocv
.Ltmp261:
# %bb.6:                                # %.noexc
.LBB29_15:                              # %lpad.loopexit.split-lp
.Ltmp262:
	jmp	.LBB29_16
.LBB29_14:                              # %lpad.loopexit
.Ltmp259:
.LBB29_16:                              # %lpad
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%r13, %r13
	jne	.LBB29_17
	jmp	.LBB29_20
	.p2align	4, 0x90
.LBB29_19:                              # %_ZSt8_DestroyISt6vectorI10location_tSaIS1_EEEvPT_.exit.i.i
                                        #   in Loop: Header=BB29_17 Depth=1
	addq	$24, %rbx
	addq	$-24, %r13
	je	.LBB29_20
.LBB29_17:                              # %for.body.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_19
# %bb.18:                               # %if.then.i.i.i.i.i.i
                                        #   in Loop: Header=BB29_17 Depth=1
	callq	_ZdlPv
	jmp	.LBB29_19
.LBB29_20:                              # %invoke.cont2
.Ltmp263:
	callq	__cxa_rethrow
.Ltmp264:
# %bb.24:                               # %unreachable
.LBB29_21:                              # %lpad1
.Ltmp265:
	movq	%rax, %rbx
.Ltmp266:
	callq	__cxa_end_catch
.Ltmp267:
# %bb.22:                               # %invoke.cont3
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB29_23:                              # %terminate.lpad
.Ltmp268:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_, .Lfunc_end29-_ZNSt22__uninitialized_fill_nILb0EE15__uninit_fill_nIPSt6vectorI10location_tSaIS3_EEmS5_EET_S7_T0_RKT1_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase6-.Lttbaseref6
.Lttbaseref6:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end8-.Lcst_begin8
.Lcst_begin8:
	.uleb128 .Ltmp257-.Lfunc_begin8 # >> Call Site 1 <<
	.uleb128 .Ltmp258-.Ltmp257      #   Call between .Ltmp257 and .Ltmp258
	.uleb128 .Ltmp259-.Lfunc_begin8 #     jumps to .Ltmp259
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp258-.Lfunc_begin8 # >> Call Site 2 <<
	.uleb128 .Ltmp260-.Ltmp258      #   Call between .Ltmp258 and .Ltmp260
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp260-.Lfunc_begin8 # >> Call Site 3 <<
	.uleb128 .Ltmp261-.Ltmp260      #   Call between .Ltmp260 and .Ltmp261
	.uleb128 .Ltmp262-.Lfunc_begin8 #     jumps to .Ltmp262
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp261-.Lfunc_begin8 # >> Call Site 4 <<
	.uleb128 .Ltmp263-.Ltmp261      #   Call between .Ltmp261 and .Ltmp263
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp263-.Lfunc_begin8 # >> Call Site 5 <<
	.uleb128 .Ltmp264-.Ltmp263      #   Call between .Ltmp263 and .Ltmp264
	.uleb128 .Ltmp265-.Lfunc_begin8 #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp266-.Lfunc_begin8 # >> Call Site 6 <<
	.uleb128 .Ltmp267-.Ltmp266      #   Call between .Ltmp266 and .Ltmp267
	.uleb128 .Ltmp268-.Lfunc_begin8 #     jumps to .Ltmp268
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp267-.Lfunc_begin8 # >> Call Site 7 <<
	.uleb128 .Lfunc_end29-.Ltmp267  #   Call between .Ltmp267 and .Lfunc_end29
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end8:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase6:
	.p2align	2
                                        # -- End function
	.section	.text._ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_,"axG",@progbits,_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_,comdat
	.weak	_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_ # -- Begin function _ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_
	.p2align	4, 0x90
	.type	_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_,@function
_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_: # @_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.LBB30_5
# %bb.1:                                # %for.body.preheader
	movq	%rsi, %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB30_2:                               # %for.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%r15), %rdi
.Ltmp269:
	callq	_ZN12netlist_elemC1Ev
.Ltmp270:
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB30_2 Depth=1
	addq	$88, %r15
	addq	$-1, %r14
	jne	.LBB30_2
# %bb.4:                                # %for.end.loopexit
	addq	%r15, %rbx
.LBB30_5:                               # %for.end
	movq	%rbx, %rax
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.LBB30_6:                               # %lpad
	.cfi_def_cfa_offset 32
.Ltmp271:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%r15, %r15
	je	.LBB30_15
# %bb.7:                                # %for.body.i.i.preheader
	addq	$56, %rbx
	jmp	.LBB30_8
	.p2align	4, 0x90
.LBB30_14:                              # %_ZSt8_DestroyI12netlist_elemEvPT_.exit.i.i
                                        #   in Loop: Header=BB30_8 Depth=1
	addq	$88, %rbx
	addq	$-88, %r15
	je	.LBB30_15
.LBB30_8:                               # %for.body.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_10
# %bb.9:                                # %if.then.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB30_8 Depth=1
	callq	_ZdlPv
.LBB30_10:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit.i.i.i.i
                                        #   in Loop: Header=BB30_8 Depth=1
	movq	-24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB30_12
# %bb.11:                               # %if.then.i.i.i4.i.i.i.i
                                        #   in Loop: Header=BB30_8 Depth=1
	callq	_ZdlPv
.LBB30_12:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit5.i.i.i.i
                                        #   in Loop: Header=BB30_8 Depth=1
	movq	-56(%rbx), %rdi
	leaq	-40(%rbx), %rax
	cmpq	%rdi, %rax
	je	.LBB30_14
# %bb.13:                               # %if.then.i.i.i.i.i.i
                                        #   in Loop: Header=BB30_8 Depth=1
	callq	_ZdlPv
	jmp	.LBB30_14
.LBB30_15:                              # %invoke.cont2
.Ltmp272:
	callq	__cxa_rethrow
.Ltmp273:
# %bb.19:                               # %unreachable
.LBB30_16:                              # %lpad1
.Ltmp274:
	movq	%rax, %rbx
.Ltmp275:
	callq	__cxa_end_catch
.Ltmp276:
# %bb.17:                               # %invoke.cont3
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB30_18:                              # %terminate.lpad
.Ltmp277:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_, .Lfunc_end30-_ZNSt27__uninitialized_default_n_1ILb0EE18__uninit_default_nIP12netlist_elemmEET_S4_T0_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase7-.Lttbaseref7
.Lttbaseref7:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end9-.Lcst_begin9
.Lcst_begin9:
	.uleb128 .Ltmp269-.Lfunc_begin9 # >> Call Site 1 <<
	.uleb128 .Ltmp270-.Ltmp269      #   Call between .Ltmp269 and .Ltmp270
	.uleb128 .Ltmp271-.Lfunc_begin9 #     jumps to .Ltmp271
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp270-.Lfunc_begin9 # >> Call Site 2 <<
	.uleb128 .Ltmp272-.Ltmp270      #   Call between .Ltmp270 and .Ltmp272
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp272-.Lfunc_begin9 # >> Call Site 3 <<
	.uleb128 .Ltmp273-.Ltmp272      #   Call between .Ltmp272 and .Ltmp273
	.uleb128 .Ltmp274-.Lfunc_begin9 #     jumps to .Ltmp274
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp275-.Lfunc_begin9 # >> Call Site 4 <<
	.uleb128 .Ltmp276-.Ltmp275      #   Call between .Ltmp275 and .Ltmp276
	.uleb128 .Ltmp277-.Lfunc_begin9 #     jumps to .Ltmp277
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp276-.Lfunc_begin9 # >> Call Site 5 <<
	.uleb128 .Lfunc_end30-.Ltmp276  #   Call between .Ltmp276 and .Lfunc_end30
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end9:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase7:
	.p2align	2
                                        # -- End function
	.section	.text._ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_,"axG",@progbits,_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_,comdat
	.weak	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_ # -- Begin function _ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_
	.p2align	4, 0x90
	.type	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_,@function
_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_: # @_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r12
	.cfi_def_cfa_offset 32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	pushq	%rax
	.cfi_def_cfa_offset 48
	.cfi_offset %rbx, -40
	.cfi_offset %r12, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	cmpq	%rsi, %rdi
	je	.LBB31_5
# %bb.1:                                # %for.body.preheader
	movq	%rsi, %r14
	movq	%rdi, %r15
	subq	%rdi, %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB31_2:                               # %for.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%r12), %rdi
	leaq	(%r15,%r12), %rsi
.Ltmp278:
	callq	_ZN12netlist_elemC2ERKS_
.Ltmp279:
# %bb.3:                                # %for.inc
                                        #   in Loop: Header=BB31_2 Depth=1
	addq	$88, %r12
	cmpq	%r12, %r14
	jne	.LBB31_2
# %bb.4:                                # %for.end.loopexit
	addq	%r12, %rbx
.LBB31_5:                               # %for.end
	movq	%rbx, %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.LBB31_6:                               # %lpad
	.cfi_def_cfa_offset 48
.Ltmp280:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	testq	%r12, %r12
	je	.LBB31_15
# %bb.7:                                # %for.body.i.i.preheader
	addq	$56, %rbx
	jmp	.LBB31_8
	.p2align	4, 0x90
.LBB31_14:                              # %_ZSt8_DestroyI12netlist_elemEvPT_.exit.i.i
                                        #   in Loop: Header=BB31_8 Depth=1
	addq	$88, %rbx
	addq	$-88, %r12
	je	.LBB31_15
.LBB31_8:                               # %for.body.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB31_10
# %bb.9:                                # %if.then.i.i.i.i.i.i.i
                                        #   in Loop: Header=BB31_8 Depth=1
	callq	_ZdlPv
.LBB31_10:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit.i.i.i.i
                                        #   in Loop: Header=BB31_8 Depth=1
	movq	-24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB31_12
# %bb.11:                               # %if.then.i.i.i4.i.i.i.i
                                        #   in Loop: Header=BB31_8 Depth=1
	callq	_ZdlPv
.LBB31_12:                              # %_ZNSt6vectorIP12netlist_elemSaIS1_EED2Ev.exit5.i.i.i.i
                                        #   in Loop: Header=BB31_8 Depth=1
	movq	-56(%rbx), %rdi
	leaq	-40(%rbx), %rax
	cmpq	%rdi, %rax
	je	.LBB31_14
# %bb.13:                               # %if.then.i.i.i.i.i.i
                                        #   in Loop: Header=BB31_8 Depth=1
	callq	_ZdlPv
	jmp	.LBB31_14
.LBB31_15:                              # %invoke.cont3
.Ltmp281:
	callq	__cxa_rethrow
.Ltmp282:
# %bb.19:                               # %unreachable
.LBB31_16:                              # %lpad2
.Ltmp283:
	movq	%rax, %rbx
.Ltmp284:
	callq	__cxa_end_catch
.Ltmp285:
# %bb.17:                               # %invoke.cont4
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB31_18:                              # %terminate.lpad
.Ltmp286:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end31:
	.size	_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_, .Lfunc_end31-_ZNSt20__uninitialized_copyILb0EE13__uninit_copyIPK12netlist_elemPS2_EET0_T_S7_S6_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.uleb128 .Lttbase8-.Lttbaseref8
.Lttbaseref8:
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end10-.Lcst_begin10
.Lcst_begin10:
	.uleb128 .Ltmp278-.Lfunc_begin10 # >> Call Site 1 <<
	.uleb128 .Ltmp279-.Ltmp278      #   Call between .Ltmp278 and .Ltmp279
	.uleb128 .Ltmp280-.Lfunc_begin10 #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp279-.Lfunc_begin10 # >> Call Site 2 <<
	.uleb128 .Ltmp281-.Ltmp279      #   Call between .Ltmp279 and .Ltmp281
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp281-.Lfunc_begin10 # >> Call Site 3 <<
	.uleb128 .Ltmp282-.Ltmp281      #   Call between .Ltmp281 and .Ltmp282
	.uleb128 .Ltmp283-.Lfunc_begin10 #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp284-.Lfunc_begin10 # >> Call Site 4 <<
	.uleb128 .Ltmp285-.Ltmp284      #   Call between .Ltmp284 and .Ltmp285
	.uleb128 .Ltmp286-.Lfunc_begin10 #     jumps to .Ltmp286
	.byte	1                       #   On action: 1
	.uleb128 .Ltmp285-.Lfunc_begin10 # >> Call Site 5 <<
	.uleb128 .Lfunc_end31-.Ltmp285  #   Call between .Ltmp285 and .Lfunc_end31
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end10:
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.p2align	2
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
.Lttbase8:
	.p2align	2
                                        # -- End function
	.section	.text._ZN12netlist_elemC2ERKS_,"axG",@progbits,_ZN12netlist_elemC2ERKS_,comdat
	.weak	_ZN12netlist_elemC2ERKS_ # -- Begin function _ZN12netlist_elemC2ERKS_
	.p2align	4, 0x90
	.type	_ZN12netlist_elemC2ERKS_,@function
_ZN12netlist_elemC2ERKS_:               # @_ZN12netlist_elemC2ERKS_
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	16(%rdi), %r13
	movq	%r13, (%rdi)
	movq	(%rsi), %r15
	movq	8(%rsi), %r12
	testq	%r15, %r15
	jne	.LBB32_2
# %bb.1:                                # %entry
	testq	%r12, %r12
	jne	.LBB32_35
.LBB32_2:                               # %if.end.i.i.i.i
	movq	%r12, (%rsp)
	movq	%r13, %rax
	cmpq	$16, %r12
	jb	.LBB32_4
# %bb.3:                                # %if.then4.i.i.i.i
	movq	%rsp, %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	callq	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm
	movq	%rax, (%r14)
	movq	(%rsp), %rcx
	movq	%rcx, 16(%r14)
.LBB32_4:                               # %if.end6.i.i.i.i
	testq	%r12, %r12
	je	.LBB32_8
# %bb.5:                                # %if.end6.i.i.i.i
	cmpq	$1, %r12
	jne	.LBB32_7
# %bb.6:                                # %if.then.i.i.i.i.i.i
	movb	(%r15), %cl
	movb	%cl, (%rax)
	jmp	.LBB32_8
.LBB32_7:                               # %if.end.i.i.i.i.i.i.i
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	memcpy
.LBB32_8:                               # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2ERKS4_.exit
	movabsq	$1152921504606846975, %r12 # imm = 0xFFFFFFFFFFFFFFF
	movq	(%rsp), %rax
	movq	%rax, 8(%r14)
	movq	(%r14), %rcx
	movb	$0, (%rcx,%rax)
	movq	40(%rbx), %rdi
	subq	32(%rbx), %rdi
	movq	%rdi, %rbp
	sarq	$3, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r14)
	movq	$0, 48(%r14)
	testq	%rdi, %rdi
	je	.LBB32_9
# %bb.10:                               # %cond.true.i.i.i.i
	cmpq	%r12, %rbp
	ja	.LBB32_11
# %bb.13:                               # %_ZNSt16allocator_traitsISaIP12netlist_elemEE8allocateERS2_m.exit.i.i.i.i
.Ltmp287:
	callq	_Znwm
.Ltmp288:
# %bb.14:                               # %call2.i.i.i.i3.i19.i.noexc
	movq	%rax, %r15
	jmp	.LBB32_15
.LBB32_9:
	xorl	%r15d, %r15d
.LBB32_15:                              # %invoke.cont.i
	movq	%r15, 32(%r14)
	movq	%r15, 40(%r14)
	leaq	(%r15,%rbp,8), %rax
	movq	%rax, 48(%r14)
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	testq	%rdx, %rdx
	je	.LBB32_17
# %bb.16:                               # %if.then.i.i.i.i.i.i.i.i
	movq	%r15, %rdi
	callq	memmove
.LBB32_17:                              # %invoke.cont
	leaq	(%r15,%rbp,8), %rax
	movq	%rax, 40(%r14)
	movq	64(%rbx), %rdi
	subq	56(%rbx), %rdi
	movq	%rdi, %rbp
	sarq	$3, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%r14)
	movq	$0, 72(%r14)
	testq	%rdi, %rdi
	je	.LBB32_18
# %bb.19:                               # %cond.true.i.i.i.i23
	movq	%r12, %rax
	leaq	32(%r14), %r12
	cmpq	%rax, %rbp
	ja	.LBB32_20
# %bb.22:                               # %_ZNSt16allocator_traitsISaIP12netlist_elemEE8allocateERS2_m.exit.i.i.i.i25
.Ltmp292:
	callq	_Znwm
.Ltmp293:
# %bb.23:                               # %call2.i.i.i.i3.i19.i.noexc39
	movq	%rax, %r15
	jmp	.LBB32_24
.LBB32_18:
	xorl	%r15d, %r15d
.LBB32_24:                              # %invoke.cont.i35
	movq	%r15, 56(%r14)
	movq	%r15, 64(%r14)
	leaq	(%r15,%rbp,8), %rax
	movq	%rax, 72(%r14)
	movq	56(%rbx), %rsi
	movq	64(%rbx), %rdx
	subq	%rsi, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	testq	%rdx, %rdx
	je	.LBB32_26
# %bb.25:                               # %if.then.i.i.i.i.i.i.i.i36
	movq	%r15, %rdi
	callq	memmove
.LBB32_26:                              # %invoke.cont6
	leaq	(%r15,%rbp,8), %rax
	movq	%rax, 64(%r14)
	.p2align	4, 0x90
.LBB32_27:                              # %do.body.i.i
                                        # =>This Inner Loop Header: Depth=1
	#APP
	lock
	cmpxchgq	%rax, 80(%rbx)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB32_27
# %bb.28:                               # %invoke.cont9
	movq	%rax, 80(%r14)
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.LBB32_35:                              # %if.then.i.i.i.i
	.cfi_def_cfa_offset 64
	movl	$.L.str.7.11, %edi
	callq	_ZSt19__throw_logic_errorPKc
.LBB32_11:                              # %if.then.i.i.i.i.i.i16
.Ltmp289:
	callq	_ZSt17__throw_bad_allocv
.Ltmp290:
# %bb.12:                               # %.noexc
.LBB32_20:                              # %if.then.i.i.i.i.i.i24
.Ltmp294:
	callq	_ZSt17__throw_bad_allocv
.Ltmp295:
# %bb.21:                               # %.noexc38
.LBB32_30:                              # %lpad5
.Ltmp296:
	movq	%rax, %rbx
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB32_32
# %bb.31:                               # %if.then.i.i.i46
	callq	_ZdlPv
	jmp	.LBB32_32
.LBB32_29:                              # %lpad
.Ltmp291:
	movq	%rax, %rbx
.LBB32_32:                              # %ehcleanup10
	movq	(%r14), %rdi
	cmpq	%r13, %rdi
	je	.LBB32_34
# %bb.33:                               # %if.then.i.i
	callq	_ZdlPv
.LBB32_34:                              # %_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN12netlist_elemC2ERKS_, .Lfunc_end32-_ZN12netlist_elemC2ERKS_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	255                     # @TType Encoding = omit
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end11-.Lcst_begin11
.Lcst_begin11:
	.uleb128 .Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.uleb128 .Ltmp287-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp287
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp287-.Lfunc_begin11 # >> Call Site 2 <<
	.uleb128 .Ltmp288-.Ltmp287      #   Call between .Ltmp287 and .Ltmp288
	.uleb128 .Ltmp291-.Lfunc_begin11 #     jumps to .Ltmp291
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp288-.Lfunc_begin11 # >> Call Site 3 <<
	.uleb128 .Ltmp292-.Ltmp288      #   Call between .Ltmp288 and .Ltmp292
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp292-.Lfunc_begin11 # >> Call Site 4 <<
	.uleb128 .Ltmp293-.Ltmp292      #   Call between .Ltmp292 and .Ltmp293
	.uleb128 .Ltmp296-.Lfunc_begin11 #     jumps to .Ltmp296
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp293-.Lfunc_begin11 # >> Call Site 5 <<
	.uleb128 .Ltmp289-.Ltmp293      #   Call between .Ltmp293 and .Ltmp289
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp289-.Lfunc_begin11 # >> Call Site 6 <<
	.uleb128 .Ltmp290-.Ltmp289      #   Call between .Ltmp289 and .Ltmp290
	.uleb128 .Ltmp291-.Lfunc_begin11 #     jumps to .Ltmp291
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp294-.Lfunc_begin11 # >> Call Site 7 <<
	.uleb128 .Ltmp295-.Ltmp294      #   Call between .Ltmp294 and .Ltmp295
	.uleb128 .Ltmp296-.Lfunc_begin11 #     jumps to .Ltmp296
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp295-.Lfunc_begin11 # >> Call Site 8 <<
	.uleb128 .Lfunc_end32-.Ltmp295  #   Call between .Ltmp295 and .Lfunc_end32
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end11:
	.p2align	2
                                        # -- End function
	.text
	.globl	_ZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE # -- Begin function _ZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align	4, 0x90
	.type	_ZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r12
	.cfi_def_cfa_offset 32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	pushq	%rax
	.cfi_def_cfa_offset 48
	.cfi_offset %rbx, -40
	.cfi_offset %r12, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	64(%rdi), %r15
	movq	%r15, %rdi
	callq	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_
	leaq	72(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.LBB33_1
# %bb.3:                                # %if.else
	movq	64(%rax), %r12
	jmp	.LBB33_4
.LBB33_1:                               # %if.then
	movl	_ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem(%rip), %esi
	movq	16(%rbx), %r12
	movq	24(%rbx), %rax
	subq	%r12, %rax
	sarq	$3, %rax
	movabsq	$3353953467947191203, %rdx # imm = 0x2E8BA2E8BA2E8BA3
	imulq	%rax, %rdx
	cmpq	%rsi, %rdx
	jbe	.LBB33_5
# %bb.2:                                # %_ZNSt6vectorI12netlist_elemSaIS0_EE2atEm.exit
	imulq	$88, %rsi, %rax
	addq	%rax, %r12
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	_ZNSt3mapINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEP12netlist_elemSt4lessIS5_ESaISt4pairIKS5_S7_EEEixERSB_
	movq	%r12, (%rax)
	addl	$1, _ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem(%rip)
.LBB33_4:                               # %if.end
	movq	%r12, %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.LBB33_5:                               # %if.then.i.i
	.cfi_def_cfa_offset 48
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	_ZSt24__throw_out_of_range_fmtPKcz
.Lfunc_end33:
	.size	_ZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end33-_ZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
                                        # -- End function
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_,comdat
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_ # -- Begin function _ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_,@function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_: # @_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%rdi), %rbx
	addq	$8, %r14
	testq	%rbx, %rbx
	je	.LBB34_16
# %bb.1:                                # %while.body.lr.ph.i
	movq	(%rsi), %r12
	movq	8(%rsi), %r13
	movq	%r14, %r15
	jmp	.LBB34_2
	.p2align	4, 0x90
.LBB34_6:                               # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit.i
                                        #   in Loop: Header=BB34_2 Depth=1
	testl	%eax, %eax
	js	.LBB34_8
.LBB34_7:                               # %if.then.i
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	%rbx, %rax
	addq	$16, %rax
	movq	%rbx, %r15
.LBB34_9:                               # %if.end.i
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB34_10
.LBB34_2:                               # %while.body.i
                                        # =>This Inner Loop Header: Depth=1
	movq	40(%rbx), %rdx
	movq	%rdx, %rbp
	subq	%r13, %rbp
	cmovaq	%r13, %rdx
	testq	%rdx, %rdx
	je	.LBB34_4
# %bb.3:                                # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i.i
                                        #   in Loop: Header=BB34_2 Depth=1
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	callq	memcmp
	testl	%eax, %eax
	jne	.LBB34_6
.LBB34_4:                               # %if.then.i.i.i.i
                                        #   in Loop: Header=BB34_2 Depth=1
	cmpq	$2147483647, %rbp       # imm = 0x7FFFFFFF
	jg	.LBB34_7
# %bb.5:                                # %if.else.i.i.i.i.i
                                        #   in Loop: Header=BB34_2 Depth=1
	cmpq	$-2147483648, %rbp      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovlel	%eax, %ebp
	movl	%ebp, %eax
	jmp	.LBB34_6
	.p2align	4, 0x90
.LBB34_8:                               # %if.else.i
                                        #   in Loop: Header=BB34_2 Depth=1
	addq	$24, %rbx
	movq	%rbx, %rax
	jmp	.LBB34_9
.LBB34_10:                              # %_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE14_M_lower_boundEPSt13_Rb_tree_nodeISA_EPSt18_Rb_tree_node_baseRS7_.exit
	cmpq	%r14, %r15
	je	.LBB34_16
# %bb.11:                               # %lor.lhs.false
	movq	40(%r15), %rax
	movq	%r13, %rbx
	subq	%rax, %rbx
	cmovaq	%rax, %r13
	testq	%r13, %r13
	je	.LBB34_13
# %bb.12:                               # %_ZNSt11char_traitsIcE7compareEPKcS2_m.exit.i.i.i
	movq	32(%r15), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	callq	memcmp
	testl	%eax, %eax
	je	.LBB34_13
# %bb.15:                               # %_ZNKSt4lessINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEclERKS5_S8_.exit
	testl	%eax, %eax
	jns	.LBB34_17
	jmp	.LBB34_16
.LBB34_13:                              # %if.then.i.i.i
	cmpq	$2147483647, %rbx       # imm = 0x7FFFFFFF
	jg	.LBB34_17
# %bb.14:                               # %if.else.i.i.i.i
	cmpq	$-2147483648, %rbx      # imm = 0x80000000
	movl	$-2147483648, %eax      # imm = 0x80000000
	cmovgl	%ebx, %eax
	testl	%eax, %eax
	jns	.LBB34_17
.LBB34_16:
	movq	%r14, %r15
.LBB34_17:                              # %cond.end
	movq	%r15, %rax
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end34:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_, .Lfunc_end34-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_P12netlist_elemESt10_Select1stISA_ESt4lessIS5_ESaISA_EE4findERS7_
	.cfi_endproc
                                        # -- End function
	.text
	.globl	_ZN7netlist15print_locationsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE # -- Begin function _ZN7netlist15print_locationsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align	4, 0x90
	.type	_ZN7netlist15print_locationsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
_ZN7netlist15print_locationsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE: # @_ZN7netlist15print_locationsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# %bb.0:                                # %entry
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r13
	.cfi_def_cfa_offset 32
	pushq	%r12
	.cfi_def_cfa_offset 40
	pushq	%rbx
	.cfi_def_cfa_offset 48
	subq	$512, %rsp              # imm = 0x200
	.cfi_def_cfa_offset 560
	.cfi_offset %rbx, -48
	.cfi_offset %r12, -40
	.cfi_offset %r13, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%rsi), %rsi
	movq	%rsp, %rdi
	movl	$16, %edx
	callq	_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode
	movq	88(%r14), %r12
	addq	$72, %r14
	cmpq	%r14, %r12
	je	.LBB35_19
# %bb.1:
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB35_2:                               # %for.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_4 Depth 2
                                        #     Child Loop BB35_7 Depth 2
	movq	64(%r12), %rbx
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
.Ltmp297:
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp298:
# %bb.3:                                # %invoke.cont
                                        #   in Loop: Header=BB35_2 Depth=1
	movq	%rax, %r13
.Ltmp299:
	movl	$.L.str.6.21, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp300:
	.p2align	4, 0x90
.LBB35_4:                               # %do.body.i
                                        #   Parent Loop BB35_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, 80(%rbx)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB35_4
# %bb.5:                                # %invoke.cont11
                                        #   in Loop: Header=BB35_2 Depth=1
	movl	(%rax), %esi
.Ltmp301:
	movq	%r13, %rdi
	callq	_ZNSolsEi
.Ltmp302:
# %bb.6:                                # %invoke.cont13
                                        #   in Loop: Header=BB35_2 Depth=1
	movq	%rax, %r13
.Ltmp303:
	movl	$.L.str.6.21, %esi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
.Ltmp304:
	.p2align	4, 0x90
.LBB35_7:                               # %do.body.i52
                                        #   Parent Loop BB35_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, 80(%rbx)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB35_7
# %bb.8:                                # %invoke.cont18
                                        #   in Loop: Header=BB35_2 Depth=1
	movl	4(%rax), %esi
.Ltmp305:
	movq	%r13, %rdi
	callq	_ZNSolsEi
.Ltmp306:
# %bb.9:                                # %invoke.cont20
                                        #   in Loop: Header=BB35_2 Depth=1
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB35_10
# %bb.12:                               # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit.i.i
                                        #   in Loop: Header=BB35_2 Depth=1
	cmpb	$0, 56(%rbx)
	je	.LBB35_14
# %bb.13:                               # %if.then.i4.i.i
                                        #   in Loop: Header=BB35_2 Depth=1
	movb	67(%rbx), %al
	jmp	.LBB35_16
	.p2align	4, 0x90
.LBB35_14:                              # %if.end.i.i.i
                                        #   in Loop: Header=BB35_2 Depth=1
.Ltmp307:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp308:
# %bb.15:                               # %.noexc34
                                        #   in Loop: Header=BB35_2 Depth=1
	movq	(%rbx), %rax
.Ltmp309:
	movq	%rbx, %rdi
	movl	$10, %esi
	callq	*48(%rax)
.Ltmp310:
.LBB35_16:                              # %_ZNKSt9basic_iosIcSt11char_traitsIcEE5widenEc.exit.i
                                        #   in Loop: Header=BB35_2 Depth=1
.Ltmp311:
	movsbl	%al, %esi
	movq	%r13, %rdi
	callq	_ZNSo3putEc
.Ltmp312:
# %bb.17:                               # %call1.i.noexc
                                        #   in Loop: Header=BB35_2 Depth=1
.Ltmp313:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp314:
# %bb.18:                               # %invoke.cont22
                                        #   in Loop: Header=BB35_2 Depth=1
	movq	%r12, %rdi
	callq	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base
	movq	%rax, %r12
	cmpq	%r14, %rax
	jne	.LBB35_2
.LBB35_19:                              # %for.cond.cleanup
	movq	_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, (%rsp)
	movq	_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, (%rsp,%rax)
	leaq	8(%rsp), %rdi
	callq	_ZNSt13basic_filebufIcSt11char_traitsIcEED2Ev
	leaq	248(%rsp), %rdi
	callq	_ZNSt8ios_baseD2Ev
	addq	$512, %rsp              # imm = 0x200
	.cfi_def_cfa_offset 48
	popq	%rbx
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.LBB35_10:                              # %if.then.i.i.i
	.cfi_def_cfa_offset 560
.Ltmp316:
	callq	_ZSt16__throw_bad_castv
.Ltmp317:
# %bb.11:                               # %.noexc
.LBB35_21:                              # %lpad.loopexit.split-lp
.Ltmp318:
	jmp	.LBB35_22
.LBB35_20:                              # %lpad.loopexit
.Ltmp315:
.LBB35_22:                              # %lpad
	movq	%rax, %rbx
	movq	_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, (%rsp)
	movq	_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE+24(%rip), %rcx
	movq	-24(%rax), %rax
	movq	%rcx, (%rsp,%rax)
	leaq	8(%rsp), %rdi
	callq	_ZNSt13basic_filebufIcSt11char_traitsIcEED2Ev
	leaq	248(%rsp), %rdi
	callq	_ZNSt8ios_baseD2Ev
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end35:
	.size	_ZN7netlist15print_locationsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .Lfunc_end35-_ZN7netlist15print_locationsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	255                     # @TType Encoding = omit
	.byte	1                       # Call site Encoding = uleb128
	.uleb128 .Lcst_end12-.Lcst_begin12
.Lcst_begin12:
	.uleb128 .Lfunc_begin12-.Lfunc_begin12 # >> Call Site 1 <<
	.uleb128 .Ltmp297-.Lfunc_begin12 #   Call between .Lfunc_begin12 and .Ltmp297
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp297-.Lfunc_begin12 # >> Call Site 2 <<
	.uleb128 .Ltmp314-.Ltmp297      #   Call between .Ltmp297 and .Ltmp314
	.uleb128 .Ltmp315-.Lfunc_begin12 #     jumps to .Ltmp315
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp316-.Lfunc_begin12 # >> Call Site 3 <<
	.uleb128 .Ltmp317-.Ltmp316      #   Call between .Ltmp316 and .Ltmp317
	.uleb128 .Ltmp318-.Lfunc_begin12 #     jumps to .Ltmp318
	.byte	0                       #   On action: cleanup
	.uleb128 .Ltmp317-.Lfunc_begin12 # >> Call Site 4 <<
	.uleb128 .Lfunc_end35-.Ltmp317  #   Call between .Ltmp317 and .Lfunc_end35
	.byte	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
.Lcst_end12:
	.p2align	2
                                        # -- End function
	.text
	.globl	_ZN12netlist_elemC2Ev   # -- Begin function _ZN12netlist_elemC2Ev
	.p2align	4, 0x90
	.type	_ZN12netlist_elemC2Ev,@function
_ZN12netlist_elemC2Ev:                  # @_ZN12netlist_elemC2Ev
	.cfi_startproc
# %bb.0:                                # %entry
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movq	$0, 80(%rdi)
	retq
.Lfunc_end36:
	.size	_ZN12netlist_elemC2Ev, .Lfunc_end36-_ZN12netlist_elemC2Ev
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4               # -- Begin function _ZN12netlist_elem22routing_cost_given_locE10location_t
.LCPI37_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	_ZN12netlist_elem22routing_cost_given_locE10location_t
	.p2align	4, 0x90
	.type	_ZN12netlist_elem22routing_cost_given_locE10location_t,@function
_ZN12netlist_elem22routing_cost_given_locE10location_t: # @_ZN12netlist_elem22routing_cost_given_locE10location_t
	.cfi_startproc
# %bb.0:                                # %entry
	movq	%rsi, %r8
	shrq	$32, %r8
	movq	32(%rdi), %rax
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	cmpq	%rax, 40(%rdi)
	je	.LBB37_5
# %bb.1:                                # %for.body.preheader
	xorpd	%xmm3, %xmm3
	xorl	%edx, %edx
	movapd	.LCPI37_0(%rip), %xmm2  # xmm2 = [NaN,NaN]
	.p2align	4, 0x90
.LBB37_2:                               # %for.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_3 Depth 2
	movq	(%rax,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB37_3:                               # %do.body.i66
                                        #   Parent Loop BB37_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, 80(%rcx)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB37_3
# %bb.4:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit67
                                        #   in Loop: Header=BB37_2 Depth=1
	movl	%esi, %ecx
	subl	(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sd	%ecx, %xmm0
	andpd	%xmm2, %xmm0
	addsd	%xmm0, %xmm3
	movl	%r8d, %ecx
	subl	4(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sd	%ecx, %xmm0
	andpd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	addq	$1, %rdx
	movq	32(%rdi), %rax
	movq	40(%rdi), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	movapd	%xmm0, %xmm3
	cmpq	%rdx, %rcx
	ja	.LBB37_2
.LBB37_5:                               # %for.cond13.preheader
	movq	56(%rdi), %rax
	cmpq	%rax, 64(%rdi)
	je	.LBB37_10
# %bb.6:                                # %for.body18.preheader
	xorpd	%xmm3, %xmm3
	xorl	%edx, %edx
	movapd	.LCPI37_0(%rip), %xmm2  # xmm2 = [NaN,NaN]
	.p2align	4, 0x90
.LBB37_7:                               # %for.body18
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB37_8 Depth 2
	movq	(%rax,%rdx,8), %rcx
	.p2align	4, 0x90
.LBB37_8:                               # %do.body.i
                                        #   Parent Loop BB37_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, 80(%rcx)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB37_8
# %bb.9:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit
                                        #   in Loop: Header=BB37_7 Depth=1
	movl	%esi, %ecx
	subl	(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sd	%ecx, %xmm1
	andpd	%xmm2, %xmm1
	addsd	%xmm1, %xmm3
	movl	%r8d, %ecx
	subl	4(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sd	%ecx, %xmm1
	andpd	%xmm2, %xmm1
	addsd	%xmm3, %xmm1
	addq	$1, %rdx
	movq	56(%rdi), %rax
	movq	64(%rdi), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	movapd	%xmm1, %xmm3
	cmpq	%rdx, %rcx
	ja	.LBB37_7
.LBB37_10:                              # %for.cond.cleanup17
	addsd	%xmm1, %xmm0
	retq
.Lfunc_end37:
	.size	_ZN12netlist_elem22routing_cost_given_locE10location_t, .Lfunc_end37-_ZN12netlist_elem22routing_cost_given_locE10location_t
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4               # -- Begin function _ZN12netlist_elem9swap_costEP10location_tS1_
.LCPI38_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	_ZN12netlist_elem9swap_costEP10location_tS1_
	.p2align	4, 0x90
	.type	_ZN12netlist_elem9swap_costEP10location_tS1_,@function
_ZN12netlist_elem9swap_costEP10location_tS1_: # @_ZN12netlist_elem9swap_costEP10location_tS1_
	.cfi_startproc
# %bb.0:                                # %entry
	movq	32(%rdi), %rax
	xorpd	%xmm1, %xmm1
	xorpd	%xmm2, %xmm2
	cmpq	%rax, 40(%rdi)
	je	.LBB38_5
# %bb.1:                                # %for.body.lr.ph
	xorpd	%xmm4, %xmm4
	xorl	%r8d, %r8d
	movapd	.LCPI38_0(%rip), %xmm0  # xmm0 = [NaN,NaN]
	xorpd	%xmm3, %xmm3
	.p2align	4, 0x90
.LBB38_2:                               # %for.body
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_3 Depth 2
	movq	(%rax,%r8,8), %rcx
	.p2align	4, 0x90
.LBB38_3:                               # %do.body.i101
                                        #   Parent Loop BB38_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, 80(%rcx)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB38_3
# %bb.4:                                # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit102
                                        #   in Loop: Header=BB38_2 Depth=1
	movl	(%rsi), %ecx
	movl	4(%rsi), %r9d
	movl	(%rax), %r10d
	movl	4(%rax), %r11d
	subl	%r10d, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sd	%ecx, %xmm1
	andpd	%xmm0, %xmm1
	addsd	%xmm1, %xmm4
	subl	%r11d, %r9d
	xorps	%xmm2, %xmm2
	cvtsi2sd	%r9d, %xmm2
	andpd	%xmm0, %xmm2
	addsd	%xmm4, %xmm2
	movl	(%rdx), %ecx
	movl	4(%rdx), %eax
	subl	%r10d, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sd	%ecx, %xmm1
	andpd	%xmm0, %xmm1
	addsd	%xmm1, %xmm3
	subl	%r11d, %eax
	xorps	%xmm1, %xmm1
	cvtsi2sd	%eax, %xmm1
	andpd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	addq	$1, %r8
	movq	32(%rdi), %rax
	movq	40(%rdi), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	movapd	%xmm2, %xmm4
	movapd	%xmm1, %xmm3
	cmpq	%r8, %rcx
	ja	.LBB38_2
.LBB38_5:                               # %for.cond23.preheader
	movq	56(%rdi), %rax
	cmpq	%rax, 64(%rdi)
	jne	.LBB38_8
# %bb.6:
	movapd	%xmm1, %xmm0
	movapd	%xmm2, %xmm4
	subsd	%xmm4, %xmm0
	retq
.LBB38_8:                               # %for.body28.lr.ph
	xorl	%r8d, %r8d
	movapd	.LCPI38_0(%rip), %xmm3  # xmm3 = [NaN,NaN]
	.p2align	4, 0x90
.LBB38_9:                               # %for.body28
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB38_10 Depth 2
	movq	(%rax,%r8,8), %rcx
	.p2align	4, 0x90
.LBB38_10:                              # %do.body.i
                                        #   Parent Loop BB38_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	#APP
	lock
	cmpxchgq	%rax, 80(%rcx)
	#NO_APP
	cmpq	%rax, _ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE(%rip)
	je	.LBB38_10
# %bb.11:                               # %_ZNK7threads9AtomicPtrI10location_tE3GetEv.exit
                                        #   in Loop: Header=BB38_9 Depth=1
	movl	(%rsi), %ecx
	movl	4(%rsi), %r9d
	movl	(%rax), %r10d
	movl	4(%rax), %r11d
	subl	%r10d, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sd	%ecx, %xmm0
	andpd	%xmm3, %xmm0
	addsd	%xmm0, %xmm2
	subl	%r11d, %r9d
	xorps	%xmm4, %xmm4
	cvtsi2sd	%r9d, %xmm4
	andpd	%xmm3, %xmm4
	addsd	%xmm2, %xmm4
	movl	(%rdx), %ecx
	movl	4(%rdx), %eax
	subl	%r10d, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sd	%ecx, %xmm0
	andpd	%xmm3, %xmm0
	addsd	%xmm0, %xmm1
	subl	%r11d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sd	%eax, %xmm0
	andpd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	addq	$1, %r8
	movq	56(%rdi), %rax
	movq	64(%rdi), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	movapd	%xmm4, %xmm2
	movapd	%xmm0, %xmm1
	cmpq	%r8, %rcx
	ja	.LBB38_9
# %bb.7:                                # %for.cond.cleanup27
	subsd	%xmm4, %xmm0
	retq
.Lfunc_end38:
	.size	_ZN12netlist_elem9swap_costEP10location_tS1_, .Lfunc_end38-_ZN12netlist_elem9swap_costEP10location_tS1_
	.cfi_endproc
                                        # -- End function
	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90         # -- Begin function _GLOBAL__sub_I_rng.cpp
	.type	_GLOBAL__sub_I_rng.cpp,@function
_GLOBAL__sub_I_rng.cpp:                 # @_GLOBAL__sub_I_rng.cpp
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rax
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit.35, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit.35, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end39:
	.size	_GLOBAL__sub_I_rng.cpp, .Lfunc_end39-_GLOBAL__sub_I_rng.cpp
	.cfi_endproc
                                        # -- End function
	.text
	.globl	_ZN3Rng4randEi          # -- Begin function _ZN3Rng4randEi
	.p2align	4, 0x90
	.type	_ZN3Rng4randEi,@function
_ZN3Rng4randEi:                         # @_ZN3Rng4randEi
	.cfi_startproc
# %bb.0:                                # %entry
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset %rbx, -16
	movq	(%rdi), %r11
	addl	$-1, %esi
	movslq	%esi, %r9
	movq	%r9, %rax
	shrq	%rax
	orq	%r9, %rax
	movq	%rax, %rcx
	shrq	$2, %rcx
	orq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$4, %rax
	orq	%rcx, %rax
	movq	%rax, %rcx
	shrq	$8, %rcx
	orq	%rax, %rcx
	movq	%rcx, %r10
	shrq	$16, %r10
	orq	%rcx, %r10
	movl	5000(%r11), %edx
	movq	4992(%r11), %rsi
	leaq	24(%r11), %r8
	testl	%edx, %edx
	je	.LBB40_3
	.p2align	4, 0x90
.LBB40_2:                               # %entry.if.end_crit_edge.i
	addl	$-1, %edx
.LBB40_8:                               # %_ZN6MTRand7randIntEv.exit
	movq	(%rsi), %rax
	addq	$8, %rsi
	movq	%rax, %rcx
	shrq	$11, %rcx
	xorq	%rax, %rcx
	movl	%ecx, %eax
	andl	$20601005, %eax         # imm = 0x13A58AD
	shlq	$7, %rax
	xorq	%rcx, %rax
	movl	%eax, %ecx
	andl	$122764, %ecx           # imm = 0x1DF8C
	shlq	$15, %rcx
	xorq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$18, %rax
	xorq	%rcx, %rax
	andq	%r10, %rax
	cmpq	%r9, %rax
	jbe	.LBB40_9
# %bb.1:                                # %do.body.i
	testl	%edx, %edx
	jne	.LBB40_2
.LBB40_3:                               # %if.then.i
	movq	(%r11), %rcx
	movq	%r8, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB40_4:                               # %for.body.i.i
                                        # =>This Inner Loop Header: Depth=1
	andl	$-2147483648, %ecx      # imm = 0x80000000
	movq	8(%r11,%rdx,8), %rdi
	movl	%edi, %ebx
	andl	$2147483646, %ebx       # imm = 0x7FFFFFFE
	orq	%rcx, %rbx
	movl	%edi, %ecx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorq	3176(%r11,%rdx,8), %rcx
	shrq	%rbx
	xorq	%rbx, %rcx
	movq	%rcx, (%r11,%rdx,8)
	cmpl	$-1808, %esi            # imm = 0xF8F0
	je	.LBB40_5
# %bb.10:                               # %for.body.i.i.1
                                        #   in Loop: Header=BB40_4 Depth=1
	andl	$-2147483648, %edi      # imm = 0x80000000
	movq	16(%r11,%rdx,8), %rcx
	movl	%ecx, %ebx
	andl	$2147483646, %ebx       # imm = 0x7FFFFFFE
	orq	%rdi, %rbx
	shrq	%rbx
	movl	%ecx, %edi
	andl	$1, %edi
	negl	%edi
	andl	$-1727483681, %edi      # imm = 0x9908B0DF
	xorq	3184(%r11,%rdx,8), %rdi
	xorq	%rbx, %rdi
	movq	%rdi, 8(%r11,%rdx,8)
	addq	$-16, %rsi
	addq	$2, %rdx
	addq	$16, %rax
	jmp	.LBB40_4
	.p2align	4, 0x90
.LBB40_5:                               # %for.cond4.preheader.i.i
	movl	$-396, %edx             # imm = 0xFE74
	.p2align	4, 0x90
.LBB40_6:                               # %for.body7.i.i
                                        # =>This Inner Loop Header: Depth=1
                                        # kill: def $edi killed $edi killed $rdi def $rdi
	andl	$-2147483648, %edi      # imm = 0x80000000
	movq	-8(%rax), %rcx
	movl	%ecx, %esi
	andl	$2147483646, %esi       # imm = 0x7FFFFFFE
	orq	%rdi, %rsi
	movq	(%rax), %rdi
	movl	%ecx, %ebx
	andl	$1, %ebx
	negl	%ebx
	andl	$-1727483681, %ebx      # imm = 0x9908B0DF
	xorq	-1832(%rax), %rbx
	shrq	%rsi
	xorq	%rsi, %rbx
	movq	%rbx, -16(%rax)
	andl	$-2147483648, %ecx      # imm = 0x80000000
	movl	%edi, %esi
	andl	$2147483646, %esi       # imm = 0x7FFFFFFE
	orq	%rcx, %rsi
	shrq	%rsi
	movl	%edi, %ecx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorq	-1824(%rax), %rcx
	xorq	%rsi, %rcx
	movq	%rcx, -8(%rax)
	addq	$16, %rax
	addl	$2, %edx
	jne	.LBB40_6
# %bb.7:                                # %_ZN6MTRand6reloadEv.exit.i
	andl	$-2147483648, %edi      # imm = 0x80000000
	movq	(%r11), %rcx
	movl	%ecx, %edx
	andl	$2147483646, %edx       # imm = 0x7FFFFFFE
	orq	%rdi, %rdx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorq	-1832(%rax), %rcx
	shrq	%rdx
	xorq	%rdx, %rcx
	movq	%rcx, -16(%rax)
	movl	$623, %edx              # imm = 0x26F
	movq	%r11, %rsi
	jmp	.LBB40_8
.LBB40_9:                               # %_ZN6MTRand7randIntERKm.exit
	movl	%edx, 5000(%r11)
	movq	%rsi, 4992(%r11)
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end40:
	.size	_ZN3Rng4randEi, .Lfunc_end40-_ZN3Rng4randEi
	.cfi_endproc
                                        # -- End function
	.globl	_ZN3Rng4randEv          # -- Begin function _ZN3Rng4randEv
	.p2align	4, 0x90
	.type	_ZN3Rng4randEv,@function
_ZN3Rng4randEv:                         # @_ZN3Rng4randEv
	.cfi_startproc
# %bb.0:                                # %entry
	movq	(%rdi), %r8
	movl	5000(%r8), %ecx
	testl	%ecx, %ecx
	je	.LBB41_2
# %bb.1:                                # %entry.if.end_crit_edge.i
	movq	4992(%r8), %rdx
	addl	$-1, %ecx
	jmp	.LBB41_7
.LBB41_2:                               # %if.then.i
	movq	(%r8), %rsi
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB41_3:                               # %for.body.i.i
                                        # =>This Inner Loop Header: Depth=1
	andl	$-2147483648, %esi      # imm = 0x80000000
	movq	8(%r8,%r9), %rdx
	movl	%edx, %edi
	andl	$2147483646, %edi       # imm = 0x7FFFFFFE
	orq	%rsi, %rdi
	movl	%edx, %esi
	andl	$1, %esi
	negl	%esi
	andl	$-1727483681, %esi      # imm = 0x9908B0DF
	xorq	3176(%r8,%r9), %rsi
	shrq	%rdi
	xorq	%rdi, %rsi
	movq	%rsi, (%r8,%r9)
	cmpl	$1808, %r9d             # imm = 0x710
	je	.LBB41_4
# %bb.8:                                # %for.body.i.i.1
                                        #   in Loop: Header=BB41_3 Depth=1
	andl	$-2147483648, %edx      # imm = 0x80000000
	movq	16(%r8,%r9), %rsi
	movl	%esi, %eax
	andl	$2147483646, %eax       # imm = 0x7FFFFFFE
	orq	%rdx, %rax
	movl	%esi, %ecx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorq	3184(%r8,%r9), %rcx
	shrq	%rax
	xorq	%rax, %rcx
	movq	%rcx, 8(%r8,%r9)
	addq	$16, %r9
	jmp	.LBB41_3
.LBB41_4:                               # %for.cond4.preheader.i.i
	addq	%r8, %r9
	movl	$3, %edi
	.p2align	4, 0x90
.LBB41_5:                               # %for.body7.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rsi
                                        # kill: def $edx killed $edx killed $rdx def $rdx
	andl	$-2147483648, %edx      # imm = 0x80000000
	movq	-8(%r9,%rdi,8), %rcx
	movl	%ecx, %edi
	andl	$2147483646, %edi       # imm = 0x7FFFFFFE
	orq	%rdx, %rdi
	movq	(%r9,%rsi,8), %rdx
	movl	%ecx, %eax
	andl	$1, %eax
	negl	%eax
	andl	$-1727483681, %eax      # imm = 0x9908B0DF
	xorq	-1832(%r9,%rsi,8), %rax
	shrq	%rdi
	xorq	%rdi, %rax
	movq	%rax, -16(%r9,%rsi,8)
	andl	$-2147483648, %ecx      # imm = 0x80000000
	movl	%edx, %eax
	andl	$2147483646, %eax       # imm = 0x7FFFFFFE
	orq	%rcx, %rax
	shrq	%rax
	movl	%edx, %ecx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorq	-1824(%r9,%rsi,8), %rcx
	xorq	%rax, %rcx
	movq	%rcx, -8(%r9,%rsi,8)
	leaq	2(%rsi), %rdi
	cmpl	$399, %edi              # imm = 0x18F
	jne	.LBB41_5
# %bb.6:                                # %_ZN6MTRand6reloadEv.exit.i
	andl	$-2147483648, %edx      # imm = 0x80000000
	movq	(%r8), %rax
	movl	%eax, %ecx
	andl	$2147483646, %ecx       # imm = 0x7FFFFFFE
	orq	%rdx, %rcx
	shrq	%rcx
	andl	$1, %eax
	negl	%eax
	andl	$-1727483681, %eax      # imm = 0x9908B0DF
	xorq	-1816(%r9,%rsi,8), %rax
	xorq	%rcx, %rax
	movq	%rax, (%r9,%rsi,8)
	movl	$624, 5000(%r8)         # imm = 0x270
	movq	%r8, 4992(%r8)
	movl	$623, %ecx              # imm = 0x26F
	movq	%r8, %rdx
.LBB41_7:                               # %_ZN6MTRand7randIntEv.exit
	movl	%ecx, 5000(%r8)
	leaq	8(%rdx), %rax
	movq	%rax, 4992(%r8)
	movq	(%rdx), %rax
	movq	%rax, %rcx
	shrq	$11, %rcx
	xorq	%rax, %rcx
	movl	%ecx, %eax
	andl	$20601005, %eax         # imm = 0x13A58AD
	shlq	$7, %rax
	xorq	%rcx, %rax
	movl	%eax, %ecx
	andl	$122764, %ecx           # imm = 0x1DF8C
	shlq	$15, %rcx
	xorq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$18, %rax
	xorq	%rcx, %rax
	retq
.Lfunc_end41:
	.size	_ZN3Rng4randEv, .Lfunc_end41-_ZN3Rng4randEv
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4               # -- Begin function _ZN3Rng5drandEv
.LCPI42_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI42_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI42_2:
	.quad	4463067230725210112     # double 2.3283064370807974E-10
	.text
	.globl	_ZN3Rng5drandEv
	.p2align	4, 0x90
	.type	_ZN3Rng5drandEv,@function
_ZN3Rng5drandEv:                        # @_ZN3Rng5drandEv
	.cfi_startproc
# %bb.0:                                # %entry
	movq	(%rdi), %r8
	movl	5000(%r8), %ecx
	testl	%ecx, %ecx
	je	.LBB42_2
# %bb.1:                                # %entry.if.end_crit_edge.i.i
	movq	4992(%r8), %rdx
	addl	$-1, %ecx
	jmp	.LBB42_7
.LBB42_2:                               # %if.then.i.i
	movq	(%r8), %rsi
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB42_3:                               # %for.body.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	andl	$-2147483648, %esi      # imm = 0x80000000
	movq	8(%r8,%r9), %rdx
	movl	%edx, %edi
	andl	$2147483646, %edi       # imm = 0x7FFFFFFE
	orq	%rsi, %rdi
	movl	%edx, %esi
	andl	$1, %esi
	negl	%esi
	andl	$-1727483681, %esi      # imm = 0x9908B0DF
	xorq	3176(%r8,%r9), %rsi
	shrq	%rdi
	xorq	%rdi, %rsi
	movq	%rsi, (%r8,%r9)
	cmpl	$1808, %r9d             # imm = 0x710
	je	.LBB42_4
# %bb.8:                                # %for.body.i.i.i.1
                                        #   in Loop: Header=BB42_3 Depth=1
	andl	$-2147483648, %edx      # imm = 0x80000000
	movq	16(%r8,%r9), %rsi
	movl	%esi, %eax
	andl	$2147483646, %eax       # imm = 0x7FFFFFFE
	orq	%rdx, %rax
	movl	%esi, %ecx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorq	3184(%r8,%r9), %rcx
	shrq	%rax
	xorq	%rax, %rcx
	movq	%rcx, 8(%r8,%r9)
	addq	$16, %r9
	jmp	.LBB42_3
.LBB42_4:                               # %for.cond4.preheader.i.i.i
	addq	%r8, %r9
	movl	$3, %edi
	.p2align	4, 0x90
.LBB42_5:                               # %for.body7.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rsi
                                        # kill: def $edx killed $edx killed $rdx def $rdx
	andl	$-2147483648, %edx      # imm = 0x80000000
	movq	-8(%r9,%rdi,8), %rcx
	movl	%ecx, %edi
	andl	$2147483646, %edi       # imm = 0x7FFFFFFE
	orq	%rdx, %rdi
	movq	(%r9,%rsi,8), %rdx
	movl	%ecx, %eax
	andl	$1, %eax
	negl	%eax
	andl	$-1727483681, %eax      # imm = 0x9908B0DF
	xorq	-1832(%r9,%rsi,8), %rax
	shrq	%rdi
	xorq	%rdi, %rax
	movq	%rax, -16(%r9,%rsi,8)
	andl	$-2147483648, %ecx      # imm = 0x80000000
	movl	%edx, %eax
	andl	$2147483646, %eax       # imm = 0x7FFFFFFE
	orq	%rcx, %rax
	shrq	%rax
	movl	%edx, %ecx
	andl	$1, %ecx
	negl	%ecx
	andl	$-1727483681, %ecx      # imm = 0x9908B0DF
	xorq	-1824(%r9,%rsi,8), %rcx
	xorq	%rax, %rcx
	movq	%rcx, -8(%r9,%rsi,8)
	leaq	2(%rsi), %rdi
	cmpl	$399, %edi              # imm = 0x18F
	jne	.LBB42_5
# %bb.6:                                # %_ZN6MTRand6reloadEv.exit.i.i
	andl	$-2147483648, %edx      # imm = 0x80000000
	movq	(%r8), %rax
	movl	%eax, %ecx
	andl	$2147483646, %ecx       # imm = 0x7FFFFFFE
	orq	%rdx, %rcx
	shrq	%rcx
	andl	$1, %eax
	negl	%eax
	andl	$-1727483681, %eax      # imm = 0x9908B0DF
	xorq	-1816(%r9,%rsi,8), %rax
	xorq	%rcx, %rax
	movq	%rax, (%r9,%rsi,8)
	movl	$624, 5000(%r8)         # imm = 0x270
	movq	%r8, 4992(%r8)
	movl	$623, %ecx              # imm = 0x26F
	movq	%r8, %rdx
.LBB42_7:                               # %_ZN6MTRand4randEv.exit
	movl	%ecx, 5000(%r8)
	leaq	8(%rdx), %rax
	movq	%rax, 4992(%r8)
	movq	(%rdx), %rax
	movq	%rax, %rcx
	shrq	$11, %rcx
	xorq	%rax, %rcx
	movl	%ecx, %eax
	andl	$20601005, %eax         # imm = 0x13A58AD
	shlq	$7, %rax
	xorq	%rcx, %rax
	movl	%eax, %ecx
	andl	$122764, %ecx           # imm = 0x1DF8C
	shlq	$15, %rcx
	xorq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$18, %rax
	xorq	%rcx, %rax
	movq	%rax, %xmm1
	punpckldq	.LCPI42_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI42_1(%rip), %xmm1
	movapd	%xmm1, %xmm0
	unpckhpd	%xmm1, %xmm0    # xmm0 = xmm0[1],xmm1[1]
	addsd	%xmm1, %xmm0
	mulsd	.LCPI42_2(%rip), %xmm0
	retq
.Lfunc_end42:
	.size	_ZN3Rng5drandEv, .Lfunc_end42-_ZN3Rng5drandEv
	.cfi_endproc
                                        # -- End function
	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_annealer_thread.cpp
	.quad	_GLOBAL__sub_I_main.cpp
	.quad	_GLOBAL__sub_I_netlist.cpp
	.quad	_GLOBAL__sub_I_rng.cpp
	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	_ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE,@object # @_ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE
	.section	.data._ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE,"aGw",@progbits,_ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE,comdat
	.weak	_ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE
	.p2align	3
_ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE:
	.quad	1
	.size	_ZN7threads9AtomicPtrI10location_tE11ATOMIC_NULLE, 8

	.type	_ZStL8__ioinit.2,@object # @_ZStL8__ioinit.2
	.local	_ZStL8__ioinit.2
	.comm	_ZStL8__ioinit.2,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"PARSEC Benchmark Suite"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Usage: "
	.size	.L.str.1, 8

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	" NTHREADS NSWAPS TEMP NETLIST [NSTEPS]"
	.size	.L.str.2, 39

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Threadcount: "
	.size	.L.str.3, 14

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" swaps per temperature step"
	.size	.L.str.4, 28

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"start temperature: "
	.size	.L.str.5, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"basic_string::_M_construct null not valid"
	.size	.L.str.9, 42

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"netlist filename: "
	.size	.L.str.6, 19

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"number of temperature steps: "
	.size	.L.str.7, 30

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"cannot create std::vector larger than max_size()"
	.size	.L.str.10, 49

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Final routing is: "
	.size	.L.str.8, 19

	.type	_ZStL8__ioinit.4,@object # @_ZStL8__ioinit.4
	.local	_ZStL8__ioinit.4
	.comm	_ZStL8__ioinit.4,1,1
	.type	_ZStL19piecewise_construct,@object # @_ZStL19piecewise_construct
	.section	.rodata,"a",@progbits
_ZStL19piecewise_construct:
	.zero	1
	.size	_ZStL19piecewise_construct, 1

	.type	.L.str.7.11,@object     # @.str.7.11
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.7.11:
	.asciz	"basic_string::_M_construct null not valid"
	.size	.L.str.7.11, 42

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"locs created"
	.size	.L.str.12, 13

	.type	.L.str.1.13,@object     # @.str.1.13
.L.str.1.13:
	.asciz	"locs assigned"
	.size	.L.str.1.13, 14

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.size	.L.str.11, 74

	.type	.L.str.2.14,@object     # @.str.2.14
.L.str.2.14:
	.asciz	"Just saw element: "
	.size	.L.str.2.14, 19

	.type	_ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem,@object # @_ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem
	.local	_ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem
	.comm	_ZZN7netlist24create_elem_if_necessaryERNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11unused_elem,4,4
	.type	.L.str.3.15,@object     # @.str.3.15
.L.str.3.15:
	.asciz	"END"
	.size	.L.str.3.15, 4

	.type	.L.str.12.16,@object    # @.str.12.16
.L.str.12.16:
	.asciz	"vector::_M_realloc_insert"
	.size	.L.str.12.16, 26

	.type	.L.str.4.17,@object     # @.str.4.17
.L.str.4.17:
	.asciz	"netlist created. "
	.size	.L.str.4.17, 18

	.type	.L.str.5.18,@object     # @.str.5.18
.L.str.5.18:
	.asciz	" elements."
	.size	.L.str.5.18, 11

	.type	.L.str.10.19,@object    # @.str.10.19
.L.str.10.19:
	.asciz	"vector::_M_fill_insert"
	.size	.L.str.10.19, 23

	.type	.L.str.8.20,@object     # @.str.8.20
.L.str.8.20:
	.asciz	"vector::_M_default_append"
	.size	.L.str.8.20, 26

	.type	.L.str.6.21,@object     # @.str.6.21
.L.str.6.21:
	.asciz	"\t"
	.size	.L.str.6.21, 2

	.type	_ZN3Rng4seedE,@object   # @_ZN3Rng4seedE
	.bss
	.globl	_ZN3Rng4seedE
	.p2align	2
_ZN3Rng4seedE:
	.long	0                       # 0x0
	.size	_ZN3Rng4seedE, 4

	.type	_ZN3Rng9seed_lockE,@object # @_ZN3Rng9seed_lockE
	.globl	_ZN3Rng9seed_lockE
	.p2align	3
_ZN3Rng9seed_lockE:
	.zero	40
	.size	_ZN3Rng9seed_lockE, 40

	.type	_ZStL8__ioinit.35,@object # @_ZStL8__ioinit.35
	.local	_ZStL8__ioinit.35
	.comm	_ZStL8__ioinit.35,1,1

	.globl	_ZN7netlistC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN7netlistC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,@function
.set _ZN7netlistC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, _ZN7netlistC2ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.globl	_ZN12netlist_elemC1Ev
	.type	_ZN12netlist_elemC1Ev,@function
.set _ZN12netlist_elemC1Ev, _ZN12netlist_elemC2Ev
	.ident	"clang version 9.0.1 "
	.ident	"clang version 9.0.1 "
	.ident	"clang version 9.0.1 "
	.ident	"clang version 9.0.1 "
	.ident	"clang version 9.0.1 "
	.section	".note.GNU-stack","",@progbits
